#ifndef LIBBASE_ALL_HPP_
#define LIBBASE_ALL_HPP_
#define LIBBASE_ON_MODE_MAINTAIN 1

#ifdef LIBBASE_EXCLUDE
#undef LIBBASE_EXCLUDE
#define LIBBASE_EXCLUDE 0
#endif

#include "tie.hpp"
#include "auto.hpp"
#include "line.hpp"
#include "table.hpp"
#include "tuple.hpp"
#include "utils.hpp"
#include "logcat.hpp"
#include "thread.hpp"

#if LIBBASE_EXCLUDE || LIBBASE_FULL
#include "unittest.hpp"
#endif

#if LIBBASE_ON_PLANING
#include "trace.hpp"
#include "hash.hpp"
#endif

#if !DISABLE_LIBGRAMMAR
#include "pipe.hpp"
#endif

#if !DISABLE_LIBDISPERSE
#include "disperse.hpp"
#endif

#if !DISABLE_LIBSTREAM
#include "print.hpp"
#include "stream.hpp"
#endif
#endif // LIBBASE_ALL_HPP_