#ifndef LIBBASE_HASH_HPP_
#define LIBBASE_HASH_HPP_

#if defined(__clang__)
#if !defined(uint128_t)
#define uint128_t __uint128_t
#endif
#endif

#if __cplusplus
extern "C"{
#endif
uint32_t  hashArray32b(const char* s, size_t len);
uint64_t  hashArray64b(const char* s, size_t len);
uint128_t hashArray128b(const char* s, size_t len);

uint32_t  hashArray32bWithSeed(const char* s, size_t len, size_t seed);
uint64_t  hashArray64bWithSeed(const char* s, size_t len, size_t seed);
uint128_t hashArray128bWithSeed(const char* s, size_t len, size_t seed);

uint32_t  hashArray32bWithSeeds(const char* s, size_t len,
                               size_t seed1, size_t seed2);
uint64_t  hashArray64bWithSeeds(const char* s, size_t len,
                               size_t seed1, size_t seed2);
uint128_t hashArray128bWithSeeds(const char* s, size_t len,
                                size_t seed1, size_t seed2);

uint32_t  fingerprintArray32b(const char* s, size_t len);
uint64_t  fingerprintArray64b(const char* s, size_t len);
uint128_t fingerprintArray128b(const char* s, size_t len);

uint32_t  fingerprintArray32bWithSeed(const char* s, size_t len, size_t seed);
uint64_t  fingerprintArray64bWithSeed(const char* s, size_t len, size_t seed);
uint128_t fingerprintArray128bWithSeed(const char* s, size_t len, size_t seed);

uint32_t  fingerprintArray32bWithSeeds(const char* s, size_t len,
                                       size_t seed1, size_t seed2);
uint64_t  fingerprintArray64bWithSeeds(const char* s, size_t len,
                                       size_t seed1, size_t seed2);
uint128_t fingerprintArray128bWithSeeds(const char* s, size_t len,
                                        size_t seed1, size_t seed2);
#if __cplusplus
}
#endif

#if __cplusplus
namespace base{
struct Hash{
 public:
  explicit Hash(std::size_t seed1 = std::string::npos,
                std::size_t seed2 = std::string::npos){
    _seed[0] = seed1;
    _seed[1] = seed2;
  }

 public:
  template <typename InputT>
  inline base::Error hash(InputT input, std::size_t& output){
    return NoSupport;
  }

  template <>
  inline base::Error hash<std::string>(std::string s, std::size_t& output){
    if (_seed[0] != std::string::npos && _seed[1] != std::string::npos) {
      if (_byte == 32){
        output = hashArray32bWithSeeds(s.c_str(), s.size(), _seed[0], _seed[1]);
      } else if (_byte == 64) {
        output = hashArray32bWithSeeds(s.c_str(), s.size(), _seed[0], _seed[1]);
      } else if (_byte == 128){
        output = hashArray128bWithSeeds(s.c_str(), s.size(), _seed[0], _seed[1]);
      } else return NoSupport;
    } else if (_seed[1] != std::string::npos) {
      return NoSupport;
    } else if (_seed[0] != std::string::npos) {
      if (_byte == 32) {
        output = hashArray32bWithSeed(s.c_str(), s.size(), _seed[0]);
      } else if (_byte == 64) {
        output = hashArray64bWithSeed(s.c_str(), s.size(), _seed[0]);
      } else if (_byte == 128) {
        output = hashArray128bWithSeed(s.c_str(), s.size(), _seed[0]);
      } else returen NoSupport;
    } else {
      if (_byte == 32) {
        output = hashArray32b(s.c_str(), s.size());
      } else if (_byte == 64) {
        output = hashArray64b(s.c_str(), s.size());
      } else if (_byte == 128) {
        output = hashArray128b(s.c_str(), s.size());
      } return NoSupport;
    }

    /*@NOTE: seed may need to be selected again after performing hasing */
    if (_seed[0]) { }
    if (_seed[1]) { }

    return NoError;
  }

 public:
  template <typename InputT>
  inline base::Error fingerprint(InputT input, std::size_t& output){
    return NoSupport;
  }

  template <>
  inline base::Error fingerprint<std::string>(std::string input,
                                              std::size_t& output){
    if (_seed[0] != std::string::npos && _seed[1] != std::string::npos) {
      if (_byte == 32){
        output = fingerprintArray32bWithSeeds(s.c_str(), s.size(),
                                              _seed[0], _seed[1]);
      } else if (_byte == 64) {
        output = fingerprintArray32bWithSeeds(s.c_str(), s.size(),
                                              _seed[0], _seed[1]);
      } else if (_byte == 128){
        output = fingerprintArray128bWithSeeds(s.c_str(), s.size(),
                                               _seed[0], _seed[1]);
      } else return NoSupport;
    } else if (_seed[1] != std::string::npos) {
      return NoSupport;
    } else if (_seed[0] != std::string::npos) {
      if (_byte == 32) {
        output = fingerprintArray32bWithSeed(s.c_str(), s.size(), _seed[0]);
      } else if (_byte == 64) {
        output = fingerprintArray64bWithSeed(s.c_str(), s.size(), _seed[0]);
      } else if (_byte == 128) {
        output = fingerprintArray128bWithSeed(s.c_str(), s.size(), _seed[0]);
      } else returen NoSupport;
    } else {
      if (_byte == 32) {
        output = fingerprintArray32b(s.c_str(), s.size());
      } else if (_byte == 64) {
        output = fingerprintArray64b(s.c_str(), s.size());
      } else if (_byte == 128) {
        output = fingerprintArray128b(s.c_str(), s.size());
      } return NoSupport;
    }

    /*@NOTE: seed may need to be selected again after performing hasing */
    if (_seed[0]) { }
    if (_seed[1]) { }

    return NoError;
  }

 public:
  static std::size_t dad();

 private:
  std::size_t _seed[2], _byte;
}
};
#endif
#endif  // LIBBASE_HASH_HPP_