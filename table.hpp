#if !defined(LIBBASE_TABLE_HPP_) && __cplusplus
#define LIBBASE_TABLE_HPP_
#define USE_ARRAY_HASH_TABLE 1

#include <memory>
#include <functional>

#include "logcat.hpp"
#include "utils.hpp"

namespace base{
template <typename Key, typename Entry> struct Table{
 public:
  explicit Table(std::function<std::size_t()>&& count, std::function<int(Key)> &&convert): 
      _count{count}, _max{count()}, _none{nullptr}, _convert{convert}, _size{0} {
    if (_max > 0){
     #if USE_ARRAY_HASH_TABLE
      _index = reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), _max));
     #else
      _lfindex = reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), _max));
      _rhindex = reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), _max));
     #endif
      _entries = new Entry[_max];
      _keys    = new Key[_max];

      for (auto i = _max; i >= 0; --i){
       #if USE_ARRAY_HASH_TABLE
        _index[i - 1]   = _max;
       #else
        _lfindex[i - 1] = _max;
        _rhindex[i - 1] = _max;
       #endif
        _keys[i - 1]    = Key{};
        _entries[i - 1] = Entry{};
      }
    } else throw NoSupport;
  }

  explicit Table(std::size_t count, std::function<int(Key)> &&convert)
   #if USE_ARRAY_HASH_TABLE
    : _index{reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), count))},
   #else
    : _lfindex{reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), count))},
      _rhindex{reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), count))},
   #endif
      _entries{new Entry[count]},
      _none{nullptr},
      _keys{new Key[count]},
      _convert{convert},
      _max{count},
      _size{0} {
    for (auto i = count; i > 0; --i){
      #if USE_ARRAY_HASH_TABLE
        _index[i - 1]   = _max;
      #else
        _lfindex[i - 1] = _max;
        _rhindex[i - 1] = _max;
      #endif
      _keys[i - 1]    = Key{};
      _entries[i - 1] = Entry{};
    }
  }

  explicit Table(std::function<std::size_t()>&& count, std::function<int(Key)> &&convert, Entry& none): 
      _count{count}, _max{count()}, _none{&none}, _convert{convert}, _size{0} {
    if (_max > 0){
     #if USE_ARRAY_HASH_TABLE
      _index = reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), _max));
     #else
      _lfindex = reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), _max));
      _rhindex = reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), _max));
     #endif
      _entries = new Entry[_max];
      _keys    = new Key[_max];

      for (auto i = _max; i >= 0; --i){
       #if USE_ARRAY_HASH_TABLE
        _index[i - 1]   = _max;
       #else
        _lfindex[i - 1] = _max;
        _rhindex[i - 1] = _max;
       #endif
        _keys[i - 1]    = Key{};
        _entries[i - 1] = Entry{};
      }
    } else throw NoSupport;
  }

  explicit Table(std::size_t count, std::function<int(Key)> &&convert, Entry& none)
   #if USE_ARRAY_HASH_TABLE
    : _index{reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), count))},
   #else
    : _lfindex{reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), count))},
      _rhindex{reinterpret_cast<std::size_t*>(calloc(sizeof(std::size_t), count))},
   #endif
      _entries{new Entry[count]},
      _none{&none},
      _keys{new Key[count]},
      _convert{convert},
      _max{count},
      _size{0} {
    for (auto i = count; i > 0; --i){
      #if USE_ARRAY_HASH_TABLE
        _index[i - 1]   = _max;
      #else
        _lfindex[i - 1] = _max;
        _rhindex[i - 1] = _max;
      #endif
      _keys[i - 1]    = Key{};
      _entries[i - 1] = Entry{};
    }
  }

  Table(Table &) = delete;
  Table() = delete;

  ~Table(){
    if (_clear){
      for (auto i = 0; i < cast_(i, _max); ++i)
        if (_index[i] != _max)
          clr(_keys[i]);
    }
   #if USE_ARRAY_HASH_TABLE
    free(_index);
   #else
    free(_lfindex);
    free(_rhindex);
   #endif

    delete [] _entries;
    delete [] _keys;
  }

 #if DEBUG
 public:
 #if USE_ARRAY_HASH_TABLE
  std::size_t index_(std::size_t i){ return _index[i]; }
 #elif USE_TREE_HASH_TABLE
  std::size_t lfindex_(std::size_t i){ return _lfindex[i]; }
  std::size_t rhindex_(std::size_t i){ return _rhindex[i]; }
 #else
 #endif
 #endif

 public:
 #if USE_ARRAY_HASH_TABLE || USE_TREE_HASH_TABLE
  int index(const Key &key){
    auto pos = _convert(key) % _max;

    if (_index[pos] == _max) return -1;
    do
      if (_index[pos] == pos) break;
      else if (_keys[pos] == key) return pos;
      else pos = _index[pos];
    while (pos != std::string::npos && pos != _max);
    return -1;
  }

  int index(Key &&key){
    auto pos = _convert(key) % _max;

    if (_index[pos] == _max) return -1;
    do
      if (_index[pos] == pos) break;
      else if (_keys[pos] == key) return pos;
      else pos = _index[pos];
    while (pos != std::string::npos && pos != _max);
    return -1;
  }

  Entry& get(const Key &key, Entry& none){
    auto pos = _convert(key) % _max;

    do
      if (_keys[pos] == key) return _entries[pos];
     #if USE_ARRAY_HASH_TABLE
      else pos = _index[pos];
     #else
      else pos = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif
    while (pos != std::string::npos && pos != _max);
    return none;
  }

  Entry& get(Key &&key, Entry& none){
    auto pos = _convert(key) % _max;

    do
      if (_keys[pos] == key) 
        return _entries[pos];
     #if USE_ARRAY_HASH_TABLE
      else pos = _index[pos];
     #else
      else pos = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif
    while (pos != std::string::npos && pos != _max);
    return none;
  }

  Entry& get(const Key &key){
    auto pos = _convert(key) % _max;

    do
      if (_keys[pos] == key) return _entries[pos];
     #if USE_ARRAY_HASH_TABLE
      else pos = _index[pos];
     #else
      else pos = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif
    while (pos != std::string::npos && pos != _max);

    if (!_none)
      throw NotFound;
    return *_none;
  }

  Entry& get(Key &&key){
    auto pos = _convert(key) % _max;

    do
      if (_keys[pos] == key) 
        return _entries[pos];
     #if USE_ARRAY_HASH_TABLE
      else pos = _index[pos];
     #else
      else pos = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif
    while (pos != std::string::npos && pos != _max);

    if (!_none)
      throw NotFound;
    return *_none;
  }

  int put(const Key &key, Entry &value){
    auto cur = _convert(key);
    auto pos = cur % _max;

    if (_index[pos] == _max){
     #if USE_ARRAY_HASH_TABLE
      _index[pos] = std::string::npos;
     #else
      _lfindex[pos] = std::string::npos;
      _rhindex[pos] = std::string::npos;
     #endif
      _entries[pos] = value;
      _keys[pos] = key;
      _size++;
      return pos;
    }
    
    for (auto i = 0; i < cast_(i, _max); ++i){
      auto tmp = pos;
  
      if (_keys[pos] == key){
        _entries[pos] = value;
        return pos;
      }
     #if USE_ARRAY_HASH_TABLE
      else tmp = _index[pos];
     #else
      else tmp = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif

     if (tmp != std::string::npos && tmp != _max) 
      pos = tmp;
     else break;
    }

    if (_keys[pos] == key){
      _entries[pos] = value;
      return pos;
    } else if (_size == _max) return -1;

    for (auto i = static_cast<int>(pos) - 1; i >= 0; --i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i;
    }

    for (auto i = static_cast<int>(pos) + 1; i < cast_(i, _max); ++i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i;
    }
    return -1;
  }

  int put(Key &&key, Entry &value){
    auto cur = _convert(key);
    auto pos = cur % _max;

    if (_index[pos] == _max){
     #if USE_ARRAY_HASH_TABLE
      _index[pos] = std::string::npos;
     #else
      _lfindex[pos] = std::string::npos;
      _rhindex[pos] = std::string::npos;
     #endif
      _entries[pos] = value;
      _keys[pos] = key;
      _size++;
      return pos;
    }

    for (auto i = 0; i < cast_(i, _max); ++i){
      auto tmp = pos;
  
      if (_keys[pos] == key){
        _entries[pos] = value;
        return pos;
      }
     #if USE_ARRAY_HASH_TABLE
      else tmp = _index[pos];
     #else
      else tmp = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif

     if (tmp != std::string::npos && tmp != _max) 
      pos = tmp;
     else break;
    }

    if (_keys[pos] == key){
      _entries[pos] = value;
      return pos;
    } else if (_size == _max) return -1;

    for (auto i = static_cast<int>(pos) - 1; i >= 0; --i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i - 1;
    }

    for (auto i = static_cast<int>(pos) + 1; i < cast_(i, _max); ++i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i;
    }
    return -1;
  } 

  int put(const Key &key, Entry &&value){
    auto cur = _convert(key);
    auto pos = cur % _max;

    if (_index[pos] == _max){
     #if USE_ARRAY_HASH_TABLE
      _index[pos] = std::string::npos;
     #else
      _lfindex[pos] = std::string::npos;
      _rhindex[pos] = std::string::npos;
     #endif
      _entries[pos] = value;
      _keys[pos] = key;
      _size++;
      return pos;
    }
  
    for (auto i = 0; i < cast_(i, _max); ++i){
      auto tmp = pos;
  
      if (_keys[pos] == key){
        _entries[pos] = value;
        return pos;
      }
     #if USE_ARRAY_HASH_TABLE
      else tmp = _index[pos];
     #else
      else tmp = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif

     if (tmp != std::string::npos && tmp != _max) 
      pos = tmp;
     else break;
    }
  
    if (_keys[pos] == key){
      _entries[pos] = value;
      return pos;
    } else if (_size == _max) return -1;

    for (auto i = static_cast<int>(pos) - 1; i >= 0; --i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i - 1;
    }

    for (auto i = static_cast<int>(pos) + 1; i < cast_(i, _max); ++i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i;
    }
    return -1;
  }

  int put(Key &&key, Entry &&value){
    auto cur = _convert(key);
    auto pos = cur % _max;

    if (_index[pos] == _max){
     #if USE_ARRAY_HASH_TABLE
      _index[pos] = std::string::npos;
     #else
      _lfindex[pos] = std::string::npos;
      _rhindex[pos] = std::string::npos;
     #endif
      _entries[pos] = value;
      _keys[pos] = key;
      _size++;
      return pos;
    }
  
    for (auto i = 0; i < cast_(i, _max); ++i){
      auto tmp = pos;
  
      if (_keys[pos] == key){
        _entries[pos] = value;
        return pos;
      }
     #if USE_ARRAY_HASH_TABLE
      else tmp = _index[pos];
     #else
      else tmp = (cur < _convert(_keys[pos]))? _lfindex[pos]: _rhindex[pos];
     #endif

     if (tmp != std::string::npos && tmp != _max) 
      pos = tmp;
     else break;
    }
  
    if (_keys[pos] == key){
      _entries[pos] = value;
      return pos;
    } else if (_size == _max) return -1;

    for (auto i = static_cast<int>(pos) - 1; i >= 0; --i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i - 1;
    }

    for (auto i = static_cast<int>(pos) + 1; i < cast_(i, _max); ++i){
     #if USE_ARRAY_HASH_TABLE
      if (_index[i] != _max) continue;
      _index[pos] = i;
      _index[i]   = std::string::npos;
     #else
      if (_lfindex[i] != _max || _rhindex[i] != _max) continue;
      if (cur < _convert(_keys[pos]))
        _lfindex[pos] = i;
      else
        _rhindex[pos] = i; 
      _lfindex[i] = std::string::npos;
      _rhindex[i] = std::string::npos;
      _optimize(key);
     #endif
      _entries[i] = value;
      _keys[i]    = key;
      _size++;
      return i;
    }
    return -1;
  }

  base::Error clr(const Key &key){ 
    auto id = index(key);

    if (id < 0) return NotFound;
    if (_clear) _clear(_entries[id]);

   #if USE_ARRAY_HASH_TABLE
    if (_index[id] != std::string::npos && _index[id] != _max){
      _entries[id] = _entries[_index[id]];
      _keys[id]    = _keys[_index[id]];
      _index[id]   = _index[_index[id]];
    } else {
      _index[id]   = _max;
    }
   #else
    _lfindex[id] = _max;
    _rhindex[id] = _max;
   #endif
    return NoError;
  }

  base::Error clr(const Key &key, Entry&& none){ 
    auto id = index(key);

    if (id < 0) return NotFound;
    if (_clear) _clear(_entries[id]);

   #if USE_ARRAY_HASH_TABLE
    if (_index[id] != std::string::npos && _index[id] != _max){
      _entries[id] = _entries[_index[id]];
      _keys[id]    = _keys[_index[id]];
      _index[id]   = _index[_index[id]];
    } else {
      _index[id]   = _max;
      _entries[id] = none;
    }
   #else
    _lfindex[id] = _max;
    _rhindex[id] = _max;
   #endif
    return NoError;
  }

  base::Error clr(Key &&key){ 
    auto id = index(key);

    if (id < 0) return NotFound;
    if (_clear) _clear(_entries[id]);

   #if USE_ARRAY_HASH_TABLE
    if (_index[id] != std::string::npos && _index[id] != _max){
      _entries[id] = _entries[_index[id]];
      _keys[id]    = _keys[_index[id]];
      _index[id]   = _index[_index[id]];
    } else {
      _index[id]   = _max;
    }
   #else
    _lfindex[id] = _max;
    _rhindex[id] = _max;
   #endif
    return NoError;
  }

  base::Error clr(Key &&key, Entry& none){ 
    auto id = index(key);

    if (id < 0) return NotFound;
    if (_clear) _clear(_entries[id]);

   #if USE_ARRAY_HASH_TABLE
    if (_index[id] != std::string::npos && _index[id] != _max){
      _entries[id] = _entries[_index[id]];
      _keys[id]    = _keys[_index[id]];
      _index[id]   = _index[_index[id]];
    } else {
      _index[id]   = _max;
      _entries[id] = none;
    }
   #else
    _lfindex[id] = _max;
    _rhindex[id] = _max;
   #endif
    return NoError;
  }

  base::Error clr(Key &&key, Entry&& none){ 
    auto id = index(key);

    if (id < 0) return NotFound;
    if (_clear) _clear(_entries[id]);

   #if USE_ARRAY_HASH_TABLE
    if (_index[id] != std::string::npos && _index[id] != _max){
      _entries[id] = _entries[_index[id]];
      _keys[id]    = _keys[_index[id]];
      _index[id]   = _index[_index[id]];
    } else {
      _index[id]   = _max;
      _entries[id] = none;
    }
   #else
    _lfindex[id] = _max;
    _rhindex[id] = _max;
   #endif
    return NoError;
  }
 #else
 #error "can't select hashtable automatically, it need config manually"
 #endif
 public:
  std::function<int()>& counter(){ return _count; }
  
  Key& key(std::size_t index){ return _keys[index]; }

  Entry& value(std::size_t index){ return _entries[index]; }

  Entry* &none(){ return _none; }

  std::size_t size(){ return _size; }

  std::size_t max(){ return _max; }

  std::function<void(Entry&)>& clrcb(){ return _clear; }

 #if USE_TREE_HASH_TABLE 
 private:
  /* @TODO: fill these functions */
  void _optimize(Key& key){
  }

  void _optimize(Key&& key){}
 #endif

 private:
  std::function<int()> _count;
  std::function<void(Entry&)> _clear;

 private:
#if USE_ARRAY_HASH_TABLE
  std::size_t *_index;
#else
  std::size_t *_lfindex, *_rhindex;
#endif

 private:
  Entry *_entries, *_none;
  Key   *_keys;

 private:
  std::function<int(Key)> _convert;
  std::size_t _max, _size;
};
} // namespace base

#endif // LIBBASE_TABLE_HPP_