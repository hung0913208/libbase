#include "thread.hpp"
#include "trace.hpp"

#include <signal.h>

namespace base{
namespace thread{
enum Status{ Initing = 0, Running = 1, Waiting = 2 };
} // namespace thread

namespace internal{
extern std::map<std::thread::id, int> threadmanager;

template<typename... Args> 
pthread_t pthread(void*(*callback)(void*), Args... args) {
  pthread_t result;

  if (pthread_create(&result, NULL, callback, new std::tuple<Args...>(args...)) != 0)
    throw BadAccess.reason(strerror(errno));
  return result;
}

void safe(std::mutex& mutex, std::function<void()> callback){
  mutex.lock();
  callback();
  mutex.unlock();
}

void safe(Thread::PMutex& mutex, std::function<void()> callback){
  pthread_mutex_lock(&mutex);
  callback();
  pthread_mutex_unlock(&mutex);
}

void wait(Thread::PMutex& mutex){
  pthread_mutex_lock(&mutex);
  pthread_mutex_unlock(&mutex);
}
} // namespace internal

Thread::Thread(Thread::Callback callback, bool start_immediately): 
    _callback{callback}, _status{Pause}, _inputs{Tuple::make()}
{
  pthread_mutex_init(&_lock, nullptr);
  pthread_mutex_lock(&_lock);
  _pthread = internal::pthread([](void* pointer) -> void*{
    using Argument = std::tuple<Thread*, Tuple*, Thread::PMutex*,
                                Thread::Callback*, Thread::Yield*>;
    auto args   = reinterpret_cast<Argument*>(pointer);
    auto thiz   = std::get<0>(*args);
    auto inputs = std::get<1>(*args);
    auto mutex  = std::get<2>(*args);
    auto runner = std::get<3>(*args);
    auto yield  = std::get<4>(*args);

    internal::safe(trace::glmutex, [](){ 
      internal::threadmanager[std::this_thread::get_id()] = thread::Initing;
    });

    if (thiz){
      internal::wait(*mutex);

      internal::safe(trace::glmutex, [](){ 
        internal::threadmanager[std::this_thread::get_id()] = thread::Initing;
      });

      if (*yield)
        (*yield)((*runner)(*thiz, *inputs));
      else
        (*runner)(*thiz, *inputs);
    }

    internal::safe(trace::glmutex, [](){ 
      internal::threadmanager.erase(std::this_thread::get_id());
    });

    delete args;
    return nullptr;
  }, this, &_inputs, &_lock, &_callback, &_yield);

  if (start_immediately && callback){
    start();
  }
}

Thread::~Thread(){
  if (_status == Pause)
    pthread_mutex_unlock(&_lock);

  if (_status != Stop)
    pthread_join(_pthread, nullptr);
  pthread_mutex_destroy(&_lock);
}

base::Error Thread::yield(Yield callback){
  if (!_yield && _status == Pause)
    _yield = callback;
  else
    return BadAccess;
  return NoError;
}

bool Thread::kill(int signal){
  if (_status == Stop)
    return false;

  pthread_kill(_pthread, signal);
  _status = Stop;
  return true;
}

bool Thread::start(bool wait){
  if (_status != Pause)
    return false;
  else {
    _status = Run;
    pthread_mutex_unlock(&_lock);

    if (wait){
      pthread_join(_pthread, nullptr);
      _status = Stop;
    }
    return true;
  }
}

bool Thread::start(Tuple args, bool wait){
  if (_status)
    return false;
  else {
    _status = Run;
    _inputs = args;
    pthread_mutex_unlock(&_lock);

    if (wait){
      pthread_join(_pthread, nullptr);
      _status = Stop;
    }
    return true;
  }
}

Lock::Lock(): status{[&]() -> bool{ return _status; }}{
  pthread_mutex_init(&_mutex, nullptr); 
}

Lock::~Lock(){ pthread_mutex_destroy(&_mutex); }

void Lock::lock(){
  _status = true;
  pthread_mutex_lock(&_mutex);
}

void Lock::unlock(){
  pthread_mutex_unlock(&_mutex);
  _status = false;
}

void Lock::safe(Lock::Callback callback){
  _status = true;
  pthread_mutex_lock(&_mutex);
  callback();
  pthread_mutex_unlock(&_mutex);
  _status = false;
}

void Lock::wait(Lock::Callback before, Lock::Callback after){
  if (_status)
    warn(DoNothing).reason("Lock has been on locking state");

  before();
  _status = true;

  pthread_mutex_lock(&_mutex);
  pthread_mutex_unlock(&_mutex);

  _status = false;
  after();
}
} // namespace base