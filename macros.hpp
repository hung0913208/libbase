#ifndef LIBBASE_PLATFORM_MACROS_HPP_
#define LIBBASE_PLATFORM_MACROS_HPP_

#if !defined(SUPPRESS_UNUSED_ATTRIBUE)
# if defined(__GNUC__) && defined(__clang__)
#  define UNUSED(x) x __attribute__((__unused__))
# else
#  define UNUSED(x) x
# endif

# if defined(__GNUC__) && defined(__clang__)
#  define UNUSED_FUNCTION(x) __attribute__((__unused__))  x
# else
#  define UNUSED_FUNCTION(x)  x
# endif
#else
# define UNUSED(x) x
# define UNUSED_FUNCTION(x)  x
#endif

#ifdef __linux__ 
# define LINUX   1
#elif __APPLE__
# include "TargetConditionals.h"
# define APPLE   1
# if TARGET_IPHONE_SIMULATOR
#  define IOS_SIMULATOR 1
# elif TARGET_OS_IPHONE
#  define IOS_DEVICE 1
# elif TARGET_OS_MAC
#  define OSX_DEVICE 1
# else
#  error "Unknown Apple platform"
# endif
#elif _WIN32
# ifdef _WIN64
#  define WINDOWS 64
# else
#  define WINDOWS 32
# endif
#elif !defined(BSD)
# if __FreeBSD__
#  define BSD 1
#  define FREEBSD __FreeBSD__
# elif __NetBSD__
#  define BSD 1
#  define NETBSD __NetBSD__
# elif defined(__OpenBSD__)
#  define BSD 1
#  define OPENBSD __OpenBSD__
# elif defined(__DragonFly__)
#  define BSD 1
#  define DRAGONFLY __DragonFly__
# endif
#endif

#ifndef BYTE_ORDER
# define BIG_ENDIAN    0
# define LITTLE_ENDIAN 1

# if (defined(__BYTE_ORDER) && __BYTE_ORDER == __BIG_ENDIAN) || \
     (defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __BIG_ENDIAN__) || \
     (defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__) || \
      defined(__ORDER_BIG_ENDIAN__) || \
      defined(__BIG_ENDIAN__) || \
      defined(__ARMEB__) || \
      defined(__THUMBEB__) || \
      defined(__AARCH64EB__) || \
      defined(_MIBSEB) || defined(__MIBSEB) || defined(__MIBSEB__)
# define BYTE_ORDER BIG_ENDIAN
# elif (defined(__BYTE_ORDER) && __BYTE_ORDER == __LITTLE_ENDIAN) || \
       (defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __LITTLE_ENDIAN) || \
       (defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN) || \
        defined(__ORDER_LITTLE_ENDIAN__) || \
        defined(__LITTLE_ENDIAN__) || \
        defined(__ARMEL__) || \
        defined(__THUMBEL__) || \
        defined(__AARCH64EL__) || \
        defined(_MIPSEL) || defined(__MIPSEL) || defined(__MIPSEL__)
#  define BYTE_ORDER LITTLE_ENDIAN
# else
#  error "I don't know what architecture this is!"
# endif
#endif  // BYTE_ORDER

// Compiler attributes
#if (defined(__GNUC__) || defined(__APPLE__)) && !defined(SWIG)
// Compiler supports GCC-style attributes
#define BASE_ATTRIBUTE_NORETURN __attribute__((noreturn))
#define BASE_ATTRIBUTE_ALWAYS_INLINE __attribute__((always_inline))
#define BASE_ATTRIBUTE_NOINLINE __attribute__((noinline))
#define BASE_ATTRIBUTE_UNUSED __attribute__((unused))
#define BASE_ATTRIBUTE_COLD __attribute__((cold))
#define BASE_ATTRIBUTE_WEAK __attribute__((weak))
#define BASE_PACKED __attribute__((packed))
#define BASE_MUST_USE_RESULT __attribute__((warn_unused_result))
#define BASE_PRINTF_ATTRIBUTE(string_index, first_to_check) \
  __attribute__((__format__(__printf__, string_index, first_to_check)))
#define BASE_SCANF_ATTRIBUTE(string_index, first_to_check) \
  __attribute__((__format__(__scanf__, string_index, first_to_check)))
#elif defined(_MSC_VER)
// Non-GCC equivalents
#define ATTRIBUTE_NORETURN __declspec(noreturn)
#define BASE_ATTRIBUTE_ALWAYS_INLINE __forceinline
#define BASE_ATTRIBUTE_NOINLINE
#define BASE_ATTRIBUTE_UNUSED
#define BASE_ATTRIBUTE_COLD
#define BASE_ATTRIBUTE_WEAK
#define BASE_MUST_USE_RESULT
#define BASE_PACKED
#define BASE_PRINTF_ATTRIBUTE(string_index, first_to_check)
#define BASE_SCANF_ATTRIBUTE(string_index, first_to_check)
#else
// Non-GCC equivalents
#define BASE_ATTRIBUTE_NORETURN
#define BASE_ATTRIBUTE_ALWAYS_INLINE
#define BASE_ATTRIBUTE_NOINLINE
#define BASE_ATTRIBUTE_UNUSED
#define BASE_ATTRIBUTE_COLD
#define BASE_ATTRIBUTE_WEAK
#define BASE_MUST_USE_RESULT
#define BASE_PACKED
#define BASE_PRINTF_ATTRIBUTE(string_index, first_to_check)
#define BASE_SCANF_ATTRIBUTE(string_index, first_to_check)
#endif

// Control visiblity outside .so
#if defined(_WIN32)
#ifdef BASE_COMPILE_LIBRARY
#define BASE_EXPORT __declspec(dllexport)
#else
#define BASE_EXPORT __declspec(dllimport)
#endif  // BASE_COMPILE_LIBRARY
#else
#define BASE_EXPORT __attribute__((visibility("default")))
#endif  // _WIN32

#ifdef __has_builtin
#define BASE_HAS_BUILTIN(x) __has_builtin(x)
#else
#define BASE_HAS_BUILTIN(x) 0
#endif

// Compilers can be told that a certain branch is not likely to be taken
// (for instance, a CHECK failure), and use that information in static
// analysis. Giving it this information can help it optimize for the
// common case in the absence of better information (ie.
// -fprofile-arcs).
#if BASE_HAS_BUILTIN(__builtin_expect) || (defined(__GNUC__) && __GNUC__ >= 3)
#define BASE_PREDICT_FALSE(x) (__builtin_expect(x, 0))
#define BASE_PREDICT_TRUE(x) (__builtin_expect(!!(x), 1))
#else
#define BASE_PREDICT_FALSE(x) (x)
#define BASE_PREDICT_TRUE(x) (x)
#endif

// A macro to disallow the copy constructor and operator= functions
// This is usually placed in the private: declarations for a class.
#define BASE_DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&) = delete;         \
  void operator=(const TypeName&) = delete

// The BASE_ARRAYSIZE(arr) macro returns the # of elements in an array arr.
//
// The expression BASE_ARRAYSIZE(a) is a compile-time constant of type
// size_t.
#define BASE_ARRAYSIZE(a)         \
  ((sizeof(a) / sizeof(*(a))) / \
   static_cast<size_t>(!(sizeof(a) % sizeof(*(a)))))

#if defined(__GXX_EXPERIMENTAL_CXX0X__) || __cplusplus >= 201103L || \
    (defined(_MSC_VER) && _MSC_VER >= 1900)
// Define this to 1 if the code is compiled in C++11 mode; leave it
// undefined otherwise.  Do NOT define it to 0 -- that causes
// '#ifdef LANG_CXX11' to behave differently from '#if LANG_CXX11'.
#define LANG_CXX11 1
#endif

#if defined(__clang__) && defined(LANG_CXX11) && defined(__has_warning)
#if __has_feature(cxx_attributes) && __has_warning("-Wimplicit-fallthrough")
#define BASE_FALLTHROUGH_INTENDED [[clang::fallthrough]]  // NOLINT
#endif
#endif

#ifndef BASE_FALLTHROUGH_INTENDED
#define BASE_FALLTHROUGH_INTENDED \
  do {                          \
  } while (0)
#endif

#define CAST_(type, value) static_cast<decltype(type)>(value)
#define SQUEEZE(type, value) static_cast<type>(value) 
#define RVALUE(variable)  std::forward<decltype(variable)>(variable)

#ifndef __has_builtin
#define __has_builtin(x) 0
#endif
#endif  //  LIBBASE_PLATFORM_MACROS_HPP_