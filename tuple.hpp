#if !defined(LIBBASE_TUPLE_HPP_) && __cplusplus
#define LIBBASE_TUPLE_HPP_
#include "logcat.hpp"
#include "trace.hpp"

#include <functional>
#include <vector>
#include <tuple>

namespace base {
struct Tuple {
 public:
  using Ref = int*;
  using Any = void*;
  using Release = std::function<void(Any)>;
  using TypeDef = std::reference_wrapper<const std::type_info>;
  using Data    = std::vector<std::tuple<Any, Release, std::string>>;

 public:
  using Type = std::vector<TypeDef>;
  using Pair = std::tuple<TypeDef, Any, Release>;

 public:
  Tuple(const Tuple& src): _data{src._data}, _type{src._type}{
    for (auto i = 0; i < cast_(i, _data.size()); ++i){
      std::get<1>(_data[i]) = nullptr;
    }
  }

  ~Tuple(){
    /* @NOTE: because there is more than one tuple access these data at the same
     * time so, we need to make sure that these are released completedly before
     * clearing everything */
    for (auto i = 0; i < cast_(i, size()); ++i){
      if (std::get<1>(_data[i]))
        std::get<1>(_data[i])(std::get<0>(_data[i]));
    }
  }

 public:
  std::size_t size() { return _type.size(); }

  const std::type_info &type(std::size_t index) { return _type[index].get(); }

  Any &value(std::size_t index) { return std::get<0>(_data[index]); }

  std::string &name(std::size_t index) { return std::get<2>(_data[index]); }

 public:
  template <typename Type> Type get(std::string name_, Type default_) {
    /* @NOTE: get data if this name exists and its type is 'Type'
     * or return 'default_' */

    for (auto elementId = 0; elementId < cast_(elementId, _data.size());
         ++elementId) {
      if (type(elementId) == typeid(Type)) {
        auto retPointer = reinterpret_cast<Type *>(value(elementId));

        if (name(elementId) == name_)
          return *retPointer;
      }
    }
    return default_;
  }

  template <typename Type> Type get(std::size_t position, Type default_) {
    /* @NOTE: get data with type 'Type' at posistion 'position'
     * or return 'default_' */

    for (auto elementId = 0; elementId < cast_(elementId, _data.size());
         ++elementId) {
      auto retPointer = reinterpret_cast<Type *>(value(elementId));

      if (type(elementId) == typeid(Type)) {
        if (0 == position)
          return *retPointer;
        position--;
      }
    }
    return default_;
  }

  template <typename Type> Type get(std::string name_) {
    /* @NOTE: get data with name is 'name' and its type is 'Type' */

    for (auto elementId = 0; elementId < cast_(elementId, _data.size());
         ++elementId) {
      auto retPointer = reinterpret_cast<Type *>(value(elementId));

      if (type(elementId) == typeid(Type)) {
        if (name(elementId) == name_)
          return *retPointer;
      }
    }
    throw NotFound;
  }

  template <typename Type> Type get(std::size_t position = 0) {
    /* @NOTE: get data numbers 'position' of type is 'Type' */

    for (auto elementId = 0; elementId < cast_(elementId, _data.size());
         ++elementId) {

      if (typeid(Type) == type(elementId)) {
        auto retPointer = reinterpret_cast<Type *>(value(elementId));

        if (0 == position)
          return *retPointer;
        position--;
      }
    }
    throw NotFound;
  }

  template <typename Type> std::size_t add(std::string name, Type value) {
    /* @NOTE: if name == "" -> this is an anonymous element */

    auto raw = reinterpret_cast<Any>(new Type{value});
    auto release = [](Any value) { delete reinterpret_cast<Type *>(value); };

    if (raw == nullptr)
      return std::string::npos;
    _data.push_back(std::make_tuple(raw, release, name));
    _type.push_back(typeid(Type));

    return _type.size() - 1;
  }

  template <typename Type> std::size_t add(Type value) {
    return add("", value);
  }

  template <typename Type> bool exist(std::size_t count) {
    /* @NOTE: count elements with this type and make sure this have
     * at lease 'count' numbers */

    for (auto elementId = 0; elementId < cast_(elementId, _data.size());
         ++elementId) {
      if (type(elementId) == typeid(Type)) {
        if (count == 0)
          return true;
        count--;
      }
    }
    return false;
  }

  template <typename Type> bool exist(std::string name_) {
    /* @NOTE: check if elements with this name exist */

    for (auto elementId = 0; elementId < cast_(elementId, _data.size());
         ++elementId) {
      if (type(elementId) == typeid(Type)) {
        if (name(elementId) == name_)
          return true;
      }
    }
    return false;
  }

public:
  bool parse(std::vector<std::pair<std::string, TypeDef>> patterns,
             std::vector<std::function<void(Any &)>> parsers,
             bool fullsearch = false) {
    /* @NOTE: scan throught all elements and pick up all samples that match
     * completely with patterns */

    auto found = false;

    for (auto patternId = 0; patternId < cast_(patternId, patterns.size());
         ++patternId) {
      for (auto elementId = 0; elementId < cast_(elementId, _type.size());
           ++elementId) {
        auto patternType = std::get<1>(patterns[patternId]);

        if (patternType.get() == type(elementId)) {
          auto elementName = name(elementId);
          auto patternName = std::get<0>(patterns[patternId]);

          if (elementName == patternName) {
            parsers[patternId](value(elementId));

            if (!fullsearch)
              return true;
            found = true;
          }
        }
      }
    }
    return found;
  }

  bool parse(std::vector<std::tuple<std::string, TypeDef, std::size_t>> patterns,
             std::vector<std::function<void(std::string, Any &)>> parsers,
             bool fullsearch = false) {
    /* @NOTE: scan throught all elements and pick up all samples that match
     * completely with patterns */

    auto found = false;

    for (auto patternId = 0; patternId < cast_(patternId, patterns.size());
         ++patternId) {
      for (auto elementId = 0; elementId < cast_(elementId, _type.size()); ++elementId) {
        auto patternType = std::get<1>(patterns[patternId]);

        if (type(elementId) == patternType.get()) {
          auto patternName = std::get<0>(patterns[patternId]);
          auto elementName = name(elementId);

          if (elementName == patternName) {
            auto parserId = std::get<2>(patterns[patternId]);

            /* @NOTE: parsers.size() usually doesn't match with parser's
             * index of patterns[i]. Therefore, we need to check again to
             * reduce mismatch from developers */
            if (parserId < parsers.size())
              parsers[parserId](elementName, value(elementId));

            /* @NOTE: if fullsearch flag is set, we have to parse from begin
             * to end */

            if (!fullsearch) return true;
            found = true;
          }
        }
      }
    }
    return found;
  }

  bool parse(std::vector<std::string> patterns,
             std::vector<std::function<void(TypeDef, Any &)>> parsers,
             bool fullsearch = true) {
    /* @NOTE: scan throught all elements and pick up all samples that match
     * completely with patterns */

    auto found = false;

    for (auto patternId = 0; patternId < cast_(patternId, patterns.size());
         ++patternId) {
      for (auto elementId = 0; elementId < cast_(elementId, _type.size()); ++elementId) {
        auto patternName = patterns[patternId];

        if (patternName == name(elementId)) {
          parsers[patternId](type(elementId), value(elementId));

          /* @NOTE: if fullsearch flag is set, we have to parse from begin
           * to end */
          if (!fullsearch)
            return true;
          found = true;
        }
      }
    }
    return found;
  }

  bool parse(std::vector<TypeDef> patterns,
             std::vector<std::function<void(std::string, Any &)>> parsers,
             bool fullsearch = true) {
    /* @NOTE: scan throught all elements and pick up all samples that match
     * completely with patterns */

    auto found = false;

    for (auto patternId = 0; patternId < cast_(patternId, patterns.size());
         ++patternId) {
      for (auto elementId = 0; elementId < cast_(elementId, _type.size());
           ++elementId) {
        auto patternType = patterns[patternId];

        if (patternType.get() == type(elementId)) {
          parsers[patternId](name(elementId), value(elementId));

          /* @note: if fullsearch flag is set, we have to parse from begin
           * to end */
          if (!fullsearch)
            return true;
          found = true;
        }
      }
    }

    return found;
  }

 public:
  Tuple& operator=(const Tuple& src){
    for (auto i = 0; i < cast_(i, size()); ++i){
      if (std::get<1>(_data[i]))
        std::get<1>(_data[i])(std::get<0>(_data[i]));
    }

    _data = src._data;
    _type = src._type;
    for (auto i = 0; i < cast_(i, _data.size()); ++i){
      std::get<1>(_data[i]) = nullptr;
    }
    return *this;
  }

 public:
  static Tuple make() { return Tuple{}; }

  static Tuple make(std::vector<Pair> pairs) {
    auto result = Tuple{pairs.size()};
    auto anonymous = "";

    for (auto index = 0; index < cast_(index, pairs.size()); ++index) {
      auto type = std::get<0>(pairs[index]);
      auto value = std::get<1>(pairs[index]);
      auto release = std::get<2>(pairs[index]);

      result._type.push_back(type);
      result._data.push_back(std::make_tuple(value, release, anonymous));
    }
    return result;
  }

  static Tuple make(std::vector<std::string> name, std::vector<Pair> pairs) {
    auto result = Tuple{pairs.size()};

    if (name.size() != pairs.size())
      throw BadLogic.reason("name.size() != pairs.size()");

    for (auto index = 0; index < cast_(index, pairs.size()); ++index) {
      auto type = std::get<0>(pairs[index]);
      auto value = std::get<1>(pairs[index]);
      auto release = std::get<2>(pairs[index]);

      result._type.push_back(type);
      result._data.push_back(std::make_tuple(value, release, name[index]));
    }
    return result;
  }

  static std::pair<std::string, TypeDef> pattern(std::string name,
                                                 const std::type_info &type) {
    return std::make_pair(name, TypeDef{type});
  }

  static std::tuple<std::string, TypeDef, std::size_t>
  pattern(std::string name, const std::type_info &type, std::size_t parser) {
    return std::make_tuple(name, TypeDef{type}, parser);
  }

private:
  Tuple(std::size_t size = 0){
    _data.reserve(size);
    _type.reserve(size);
  }

private:
  Data _data;
  Type _type;
};

template<> inline std::size_t Tuple::add<Auto>(std::string name, Auto value) {
  auto type = value.type();
  auto raw  = value.strip();

  if (std::get<0>(raw) == nullptr)
    return std::string::npos;

  _data.push_back(std::make_tuple(std::get<0>(raw), std::get<1>(raw), name));
  _type.push_back(type);
  return _type.size() - 1;
}
} // namespace base
#endif // LIBBASE_TUPLE_HPP_
