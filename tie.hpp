#if !defined(LIBBASE_TIE_HPP_) && __cplusplus
#define LIBBASE_TIE_HPP_

#include <functional>
#include <vector>

#include "auto.hpp"

namespace base{
template<typename Input>
struct Tie{
 public:
  using Convert = std::function<base::Error(Input&, base::Auto&)>;
  using Type    = std::reference_wrapper<const std::type_info>;

  template<typename Left, typename Right>
  using Map = std::vector<std::pair<Left, Right>>;

 public:
  explicit Tie(Map<Type, Convert>&& converters):
    _converts{converters}{}

 public:
  Tie& operator=(std::vector<Input>& input){
    base::Error error{};

    for (std::size_t i = 0; i < _base.size() && i < input.size(); ++i){
      for (auto j = 0; j <  cast_(j, _converts.size()); ++j){
        if (std::get<0>(_converts[j]).get() == _base[i].type().get()){
          if ((error = std::get<1>(_converts[j])(input[i], _base[i])))
            throw error;
          break;
        }
      }
    }
    return *this;
  }

  Tie& operator=(std::vector<Input>&& input){
    base::Error error{};

    for (std::size_t i = 0; i < _base.size() && i < input.size(); ++i){
      for (auto j = 0; j <  cast_(j, _converts.size()); ++j){
        if (std::get<0>(_converts[j]).get() == _base[i].type().get()){
          if ((error = std::get<1>(_converts[j])(input[i], _base[i])))
            throw error;
          break;
        }
      }
    }
    return *this;
  }
 
 public:
  template<typename ...Args>
  static Tie<Input> tie(Map<Type, Convert>&& converts, const Args&... args){
    Tie<Input> result{rvalue(converts)};

    Tie<Input>::tie(result, args...);
    return result;
  }

 private:
  template<typename T, typename ...Args>
  static Tie<Input>& tie(Tie<Input>& self, const T &value, const Args&... args){
    if (sizeof...(args) >= 1)
      return tie(tie(self, value), args...);
    else
      return tie(self, value);
  }

  template<typename T>
  static Tie<Input>& tie(Tie<Input>& self, const T &value){
    self._base.push_back(base::Auto{}.set(value));
    return self;
  }

 private:
  std::vector<base::Auto> _base;
  Map<Type, Convert>      _converts;
};
} // namespace base
#endif  // LIBBASE_TIE_HPP_