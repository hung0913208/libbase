#if !defined(LIBBASE_CONFIG_HPP_) && __cplusplus
#define LIBBASE_CONFIG_HPP_
#include "auto.hpp"

namespace base{
namespace config{
using Slot = std::function<void(std::string&, base::Auto&)>;

base::Auto  get(std::string name);
base::Error set(std::string name, base::Auto value);
base::Error hook(std::string name, Slot slot);

template<typename Type>
Type get(std::string name){ return get(name).get<Type>(); }

template<typename Type>
base::Error set(std::string name, Type value){
  return set(name, Auto{}.set(value));
}

void start(char* argv[], int argc);
int  finish(int code, std::function<void()> before = nullptr, 
					  std::function<void()> after = nullptr);
} // namesapce config
} // namespace
#endif  // LIBBASE_CONFIG_HPP_