#if !defined(LIBBASE_LINE_HPP_) && __cplusplus
#define LIBBASE_LINE_HPP_ 1

#include "./logcat.hpp"
#include "./utils.hpp"

#include <functional>
#include <string>
#include <vector>

namespace base {
template <typename T, typename DeepT = long> struct Line {
public:
  explicit Line(T null, DeepT first, std::function<DeepT(T &&)> &&deep)
      : _deep{deep}, _current{first}, _null{null} {
    _isnull = [=](T &&value) -> bool {
      return !memcmp(&value, &null, sizeof(T));
    };
  }

  explicit Line(std::function<bool(T &&)> isnull, DeepT first,
                std::function<DeepT(T &&)> &&deep)
      : _deep{deep}, _isnull{isnull}, _current{first}, _null{} {}

  explicit Line(DeepT first, std::function<DeepT(T &&)> &&deep)
      : _deep{deep}, _isnull{[=](T &&) { return false; }}, _current{first},
        _null{} {}

public:
  Line() noexcept {}

public:
  T exchange(T value, std::function<bool(T &&)> cond) {
    /* @NOTE: in exchange, we compare 'value' with our 'null', if it is true,
     * it means we want to release the first value
     */

    if (_isnull && _isnull(std::forward<T>(value))) {
      auto ret = _buffer.size() > 0 ? _buffer[0] : _null;

      if (_buffer.size() > 0) {
        _buffer.erase(_buffer.begin());
      }

      return ret;
    } else if (cond && cond(std::forward<T>(value))) {
      /* @NOTE: we must notice if input value is capable to return if it match
       * the condition, it would be return first. This will be use to be like a
       * filter
       */

      _current = _deep(std::forward<T>(value));
      return value;
    } else {
      /* @NOTE: we perform a quick sort on limited memory */
      auto pos = bfind(_deep(std::forward<T>(value)));

      if (pos >= 0 && pos < static_cast<int>(_buffer.size())) {
        for (auto i = pos; i < static_cast<int>(_buffer.size()); ++i) {
          std::swap(_buffer[i], value);
        }
      }
      _buffer.push_back(value);

      /* @NOTE: if condition(_buffer[0]) turn true, it indicates we must release
       * it . On the otherhand, we must return _null
       */

      if (_buffer.size() > 1 && pos != 0) {
        if (cond && !cond(std::forward<T>(_buffer[0]))) {
          return _null;
        } else {
          auto ret = _buffer[0];

          _buffer.erase(_buffer.begin());
          return ret;
        }
      } else {
        return _null;
      }
    }

    return _null;
  }

  T flush() {
    return size() ? exchange(std::forward<T>(_null), nullptr) : _null;
  }

  T get() { return _buffer[0]; }

  T &null() { return _null; }

  DeepT current() { return _current; }

  DeepT begin() { return _deep(std::forward<T>(_buffer[0])); }

  std::size_t size() { return _buffer.size(); }

  T swap(T newcome, std::function<bool(T &&)> cond) {
    auto result = exchange(newcome, cond);

    if (_isnull(std::forward<T>(result))) {
      return flush();
    } else {
      return result;
    }
  }

public:
  T operator[](std::size_t idx) {
    PAssert(idx < _buffer.size());
    return _buffer[idx];
  }

private:
  int comp(DeepT left, DeepT right) {
    return left < right ? -1 : (left == right ? 0 : 1);
  }

  int bfind(DeepT val) {
    auto begin = 0, end = static_cast<int>(_buffer.size()) - 1;
    auto left = -1, right = -1;

    /* @NOTE: this is just binary search algothirm throught '_deep' value */
    while (begin + 1 < end) {
      if ((begin + end) / 2 + 1 >= static_cast<int>(_buffer.size())) {
        if (comp(val, _deep(std::forward<T>(_buffer[(begin + end) / 2]))) > 0) {
          return _buffer.size();
        } else {
          return (begin + end) / 2;
        }
      } else {
        left = comp(val, _deep(std::forward<T>(_buffer[(begin + end) / 2])));
        right =
            comp(val, _deep(std::forward<T>(_buffer[(begin + end) / 2 + 1])));
      }

      if (right == 0 || left == 0) {
        return (begin + end) / 2 + (right == 0 ? 2 : 1);
      } else if (-1 == left && right == -1) {
        end = (begin + end) / 2;
      } else if (1 == left && right == 1) {
        begin = (begin + end) / 2;
      } else if (-1 <= left && right <= 1) {
        return (begin + end) / 2 + (right < 0 ? left : right);
      }
    }

    /* @NOTE: select the aproximat posision base on the result of the searching
     * step */
    if (_buffer.size() == 1) {
      return _deep(std::forward<T>(_buffer[0])) < val;
    } else if (_buffer.size() == 2) {
      if (_deep(std::forward<T>(_buffer[0])) <= val) {
        return (_deep(std::forward<T>(_buffer[1])) > val ? 1 : 2);
      } else {
        return 0;
      }
    } else if (begin + 1 == end) {
      if (_deep(std::forward<T>(_buffer[begin])) > val) {
        return begin;
      } else {
        return (_deep(std::forward<T>(_buffer[end])) > val ? end : end + 1);
      }
    } else {
      return (_buffer.size() == 0 || left < 0) ? 0 : _buffer.size();
    }
  }

private:
  std::function<DeepT(T &&)> _deep;
  std::function<bool(T &&)> _isnull;
  std::vector<T> _buffer;
  DeepT _current;
  T _null;
};
} // namespace base
#endif // LIBBASE_LINE_HPP_