/* copyright 2017 by lazycat and it is owned by lazycat */
#ifndef LIBBASE_LOGCAT_HPP_
#define LIBBASE_LOGCAT_HPP_
#ifndef SINGLE_COLOR
#define SINGLE_COLOR 0
#endif

#define writeLog(message) printf("%s\n", message)

#if __cplusplus
#include <cstdarg>
#include <cstring>

#include <iostream>
#include <mutex>
#include <string>
#include <memory>
#include <map>

#include "utils.hpp"
#if DEBUG
#include <cassert>
#endif

#define Transparent(var) (void)(var)

namespace base{
#if DEBUG
#define __ERROR_LOCATE__ base::Error::position(__FILE__, __LINE__, __PRETTY_FUNCTION__)
#else
#define __ERROR_LOCATE__ ""
#endif
 //01264399989

#if READABLE
#define pError(code, message) base::Error{ __ERROR_LOCATE__, code, 0, std::forward<std::string>(message) }
#define errorRef(code) base::Error{ __ERROR_LOCATE__, code, 0, "", true }
#else
#define pError(code, message) base::Error{ code , 0 }
#define errorRef(code) base::Error{ code, 0, true }
#endif

#if __has_builtin(__builtin_expect) || (defined(__GNUC__) && __GNUC__ >= 3)
#define predict_false(x) (__builtin_expect(x, 0))
#define predict_true(x) (__builtin_expect(!!(x), 1))
#else
#define predict_false(x) (!x)
#define predict_true(x) (x)
#endif

#define warn(error) (error).change(1)
#define info(error) (error).change(-1)

namespace error{
void exclude(bool flag);
#endif

enum Code{
  ENoError = 0,
  EKeepContinue = 1,
  ENoSupport = 2,
  EBadLogic = 3,
  EBadAccess = 4,
  EOutOfRange = 5,
  ENotFound = 6,
  EDrainMem = 7,
  EWatchErrno = 8,
  EInterrupted = 9,
  EDoNothing = 10,
  EDoAgain = 11
};

#if __cplusplus
}

/* một số error cơ bản */
#if !DISABLE_ERROR_DEFINITION
#if READABLE
#define NoError base::Error{}
#define KeepContinue base::Error{__ERROR_LOCATE__, base::error::EKeepContinue}
#define NoSupport base::Error{__ERROR_LOCATE__, base::error::ENoSupport}
#define BadLogic base::Error{__ERROR_LOCATE__, base::error::EBadLogic}
#define BadAccess base::Error{__ERROR_LOCATE__, base::error::EBadAccess}
#define OutOfRange base::Error{__ERROR_LOCATE__, base::error::EOutOfRange}
#define NotFound base::Error{__ERROR_LOCATE__, base::error::ENotFound}
#define DrainMem base::Error{__ERROR_LOCATE__, base::error::EDrainMem}
#define WatchErrno base::Error{__ERROR_LOCATE__, base::error::EWatchErrno}
#define Interrupted base::Error{__ERROR_LOCATE__, base::error::EInterrupted}
#define DoNothing base::Error{__ERROR_LOCATE__, base::error::EDoNothing}
#define DoAgain base::Error{__ERROR_LOCATE__, base::error::EDoAgain}
#else
#define NoError base::Error{base::error::ENoError}
#define KeepContinue base::Error{base::error::EKeepContinue}
#define NoSupport base::Error{base::error::ENoSupport}
#define BadLogic base::Error{base::error::EBadLogic}
#define BadAccess base::Error{base::error::EBadAccess}
#define OutOfRange base::Error{base::error::EOutOfRange}
#define NotFound base::Error{base::error::ENotFound}
#define DrainMem base::Error{base::error::EDrainMem}
#define WatchErrno base::Error{base::error::EWatchErrno}
#define Interrupted base::Error{base::error::EInterrupted}
#define DoNothing base::Error{base::error::EDoNothing}
#define DoAgain base::Error{base::error::EDoAgain}
#endif
#endif

#define CAssert(condition, result) if (condition) return result

#if DEBUG
#define PAssert(condition) assert(condition)
#define BAssert(condition) assert(condition)
#else
#define PAssert(condition){ if (!(condition)) throw BadLogic; }
#define BAssert(condition){ if (!(condition)) return BadLogic; }
#endif

#define PCheck(error){ if (predict_true(error.code != 0)) throw error; }
#define BCheck(error){ if (predict_true(error.code != 0)) return error; }

struct Error{
#if READABLE
  std::string message;
  std::string debug;
#endif
  int  code, level;
  bool critical, reference;

 private:
  bool *_printed;
  int  *_reference;

 public:
  explicit Error(std::string&& debug, int code = 0, int level = 0, std::string&& message = "", bool ref = false);
#if !READABLE
  explicit Error(int code = 0, int level = 0, bool ref = false);
#else
  Error();
#endif

  Error(const Error&  error);
  Error(const Error&& error);

 public:
  Error& reason(std::string&& message);
  Error& change(int level);
  Error& ref(Error& error);

 public:
  virtual std::string codename() const;
  virtual ~Error();

  operator bool();
  operator int();

  bool operator==(int value);
  bool operator==(bool value);
  bool operator==(Error&& dst);

  Error& operator =(const Error& error);
  Error& operator =(Error&& error);

  Error& operator<<(const std::string& reason);
  Error& operator<<(std::string&& reason);
  
  static std::string position(const char* file, int line, const char* proc);

  void print() const;
};

struct Debug{
 public:
  explicit Debug(bool mode = false);
  virtual ~Debug();

 protected:
  Debug(){}
  
 private:
  bool _rescue;
};

struct Watch{
 public:
  std::string msg;

 public:
  Watch();
  ~Watch();

 private:
  std::string* _saved;
};

struct Color{
 public:
  enum Code{
    Reset   = 0,
    Red     = 31,
    Green   = 32,
    Yellow  = 33,
    Blue    = 34,
    Magneto = 35,
    Cyan    = 36, 
    White   = 37
  };

 public:
  explicit Color(std::size_t code);

 public:
  Color& operator<<(std::string message);
  Color& operator()();

 public:
  std::string&& message();

 private:
#if !SINGLE_COLOR
  std::size_t _code;
#endif
  std::string _message;
  bool _printed;
};
}  // namespase base
std::ostream& operator<<(std::ostream& stream, base::Error error);
std::ostream& operator<<(std::ostream& ostream, base::Color& color);
#endif

#if __cplusplus
extern "C"{
#endif
typedef int CError;

void writeLogW(const char* format, ...);
void catchLogW(unsigned int threadid, 
  void(*callback)(const char* line, int code, int level, const char* message));
#if __cplusplus
}
#else
typedef struct Debug{
  char *file, *proc;
  int   line;
} Debug;

#define __ERROR_LOCATE__ cdebug(__FILE__, __PRETTY_FUNCTION__, __LINE__)
#define error(code, message) cerror(code, 0, __ERROR_LOCATE__, message)
#define warning(code, message) cerror(code, 1, __ERROR_LOCATE__, message)

Debug cdebug(const char* file, const char* proc, int line);
int   cerror(int code, int level, Debug position, const char* message);
#endif
#endif  // LIBBASE_LOGCAT_HPP_
