#include "unittest.hpp"
#include "all.hpp"
#include "trace.hpp"
#include <vector>

#define IGNORED_LABEL "[  IGNORED ] "
#define SPLIT_SUITED  "[----------] "
#define FINISH_TEST   "[==========] "

namespace base{
namespace unittest{
Test* current;
} // namespace unittest

namespace config{
static std::map<std::string, std::vector<Test*>> *__suites__{nullptr};
} // namespace config

namespace internal{
base::Error starttest(char* UNUSED(argv[]), int UNUSED(argc)){
  return NoError;
}

base::Error cleartest(){
  if (!config::__suites__) return NoError;

  for (auto& suite: (*config::__suites__)){
    for (auto& test: std::get<1>(suite))
      delete test;
  }
  delete config::__suites__;
  config::__suites__ = nullptr;
  return NoError;
}
} // namespace internal

Test::Test(const char* suite, const char* name):
  _name{name}, _suite{suite}{
  if (!config::__suites__)
    config::__suites__ = new std::map<std::string, std::vector<Test*>>;

  if (config::__suites__->find(_suite) == config::__suites__->end())
    (*config::__suites__)[_suite] = std::vector<Test*>{};

  (*config::__suites__)[_suite].push_back(this);
  _ignore = false;
  _status = true;
}

Test::~Test(){}

void Test::render(Event event, std::string UNUSED(file), int UNUSED(line)){
  switch(event){
  case Init:
    break;

  case Expect:
    break;

  case Assert:
    break;

  default:
    break;
  }
}

void Test::expect(std::string&& prefix, std::string&& name,
                  std::string op, std::string&& value){
  if (value.size() == 0) return;
  if (prefix.size() < sizeof(RUN_LABEL) - 1)
    for (auto i = prefix.size(); i < sizeof(RUN_LABEL); ++i)
      trace::console << " ";

  trace::console << prefix << " " << name << " " << op << " " << value
                 << trace::eol;
}

void Test::expect(std::string&& prefix, std::string&& value){
  if (value.size() == 0) return;
  if (prefix.size() < sizeof(RUN_LABEL))
    for (auto i = prefix.size(); i < sizeof(RUN_LABEL); ++i)
      trace::console << " ";
  trace::console << prefix << " " << value << trace::eol;
}

void Test::actual(std::string&& prefix, std::string&& name, 
                  std::string op, std::string&& value){
  if (value.size() == 0) return;
  if (prefix.size() < sizeof(RUN_LABEL))
    for (auto i = prefix.size(); i < sizeof(RUN_LABEL); ++i)
      trace::console << " ";
  trace::console << prefix << " " << name << " " << op << " " << value
                 << trace::eol;
}

void Test::actual(std::string&& prefix, std::string&& value){
  if (value.size() == 0) return;
  if (prefix.size() < sizeof(RUN_LABEL))
    for (auto i = prefix.size(); i < sizeof(RUN_LABEL); ++i)
      trace::console << " ";
  trace::console << prefix << " " << value << trace::eol;
}

void Test::header(std::string&& description){
  trace::console << "On " << _suite << "." << _name;
  trace::console << " check " << description << trace::eol;
}

void Test::footer(){
}

void Test::error(base::Error& UNUSED(error_)){ }

bool Test::start(){
  using namespace std::chrono;

  int flag = config::get<int>("unittest.summary");
  clock_t start, end;

  try{
    trace::console << (Color{Color::Yellow} << RUN_LABEL)
                   << " " << _suite << "." << _name
                   << trace::eol;

    start = clock();
    define();
    end   = clock();
  } catch(base::Error& error){ _status = false; }
    end = clock();
  if (_ignore)
    trace::console << (Color{Color::Yellow} << IGNORED_LABEL)
                   << _suite << "." << _name 
                   << " (" << (end - start)/1000 << " ms)"
                   << trace::eol;
  else if (_status){
    trace::console << (Color{Color::Green} << PASSED_LABEL)
                   << _suite << "." << _name 
                   << " (" << (end - start)/1000 << " ms)"
                   << trace::eol;
  } else {
    trace::console << (Color{Color::Red}   << FAILED_LABEL)
                   << _suite << "." << _name
                   << " (" << (end - start)/1000 << " ms)"
                   << trace::eol << trace::eol;
  }

  for (auto summary: _summaries)
    summary.print(flag);
  return _status;
}

namespace testing{
void init(int& argc, const char** argv){
  config::start(const_cast<char**>(argv), argc);
}

int run(){
  using namespace std::chrono;
  int     count_pass = 0, count_fail = 0;
  clock_t global_start, global_end;

  auto cases = std::vector<std::string>{};

  /* @NOTE: reinit unittest config */
  config::set("unittest", Auto{}.set(true));
  config::set("unittest.crash", Auto{}.set(false));
  config::set("unittest.exit", Auto{}.set(0));

  if (config::get("unittest.summary") == nullptr)
    config::set("unittest.summary", Auto{}.set(0));

  /* @NOTE: run all testcase */

  global_start = clock();
  if (config::__suites__ && config::__suites__->size() > 0){
    for (auto& suite : (*config::__suites__)) {
      clock_t suite_start, suite_end;

      suite_start = clock();
      trace::console << (Color{Color::Green} << SPLIT_SUITED)
          << std::get<1>(suite).size();
      trace::console << ((std::get<1>(suite).size() > 1) ? " tests from "
                                                         : " test from ");
      trace::console <<  std::get<0>(suite) << trace::eol;

      for (auto& unit : std::get<1>(suite)) {
        if (config::get("unittest.crash").get<bool>())
          break;

        base::unittest::current = unit;
        if (unit->start()) count_pass++;
        else {
          cases.push_back(unit->suite() + "." + unit->name());
          count_fail++;
        }
      }
      suite_end = clock();

      /* @NOTE: finish test suite */
      trace::console << (Color{Color::Green} << SPLIT_SUITED)
                     << std::get<1>(suite).size();
      trace::console << ((std::get<1>(suite).size() > 1) ? " tests from "
                                                         : " test from ");
      trace::console <<  std::get<0>(suite) << " ("
                     << ((suite_end - suite_start) / 1000)
                     <<  " ms total)." << trace::eol << trace::eol;
    }
    global_end = clock();

    trace::console << (Color{Color::Green} << SPLIT_SUITED)
                   << "Global test environment tear-down."
                   << trace::eol;
    trace::console << (Color{Color::Green} << FINISH_TEST)
                   << (count_fail + count_pass);
    trace::console << ((count_pass + count_fail > 1) ? " tests from "
                                                     : " test from ");
    trace::console << config::__suites__->size();
    trace::console << ((config::__suites__->size() > 1) ? " test cases ran"
                                                        : " test case ran");
    trace::console << " (" << ((global_end - global_start) / 1000)
                   <<  " ms total)." << trace::eol;

    if (count_pass > 0) {
      trace::console << (Color{Color::Green} << PASSED_LABEL)
                     << count_pass << ((count_pass > 1) ? " tests." : " test.")
                     << trace::eol;
    }

    if (count_fail != 0) {
      trace::console << (Color{Color::Red} << FAILED_LABEL)
                     << count_fail << ((count_fail > 1) ? " tests." : " test.")
                     << trace::eol;
      for (auto& test : cases) {
        trace::console << (Color{Color::Red} << FAILED_LABEL) << " "
                       << test << trace::eol;
      }
      config::set<int>("unittest.exit", 1);
    }
  }

  /* @NOTE: finish all testcases */
  if (config::get("unittest.loop") == nullptr || !config::get<bool>("unittest.loop"))
    internal::cleartest();
  return config::get<int>("unittest.exit");
}
} // namespace testing
} // namespace base