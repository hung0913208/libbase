#include "./isolated.hpp"
#include "../table.hpp"

#define LEARNER  2
#define ACCEPTOR 1
#define PROPOSER 0
/* @DESIGN: 
 * - mo ta: thiet ke thuat toan moi dua tren mo hinh phan tan hoan toan
 * dua tren y tuong cac cell voi cac vai tro khac nhau se dam nhan trach 
 * nhiem tinh toan rieng va phan tan tinh toan tren toan he thong
 * - proposer: de nghi mot yeu cau
 * - accepter: tiep nhan yeu cau
 * - learner: tien hanh thuc hien yeu cau
 * - day la mo hinh paxos, van de la paxos chi thuc thi theo 1 chieu tu 
 * proposer toi learner, bay gio can thiet ke mot mo hinh co the tuong
 * tac tren tat ca cac chieu tren kenh broadcast va dam bao phan phoi 
 * request deu va xu ly hieu qua
 * - chung ta co it nhat 2 truong hop: hoac mot he thong gom 3 thanh phan 
 * day du la proposer, accepter va learner hoac mot he thong gom proposer
 * va learner
 * - accepter phai luon co it nhat 1 learner
 */
/* @NOTE: cac buoc thuc hien
 * - proposer gui 1 loi de nghi len broadcast va cho hoi am
 * - accepter nhan 1 loi de nghi check xem de nghi nay co thoa dieu kien 
 * gioi han cua accepter hay khong, neu thoa thi gui Promised nguoc lai 
 * thi Deny
 * - proposer nhan 
 */
using UIntT = unsigned char;

extern "C" struct ProposeSt{
  int a;
};

extern "C" struct AcceptSt{
  int a;
};

extern "C" struct LearnSt{
  int a;
};

namespace base {
namespace isolation {
struct Node::StateT{
 private:
  using Contract  = std::tuple<uint64_t, uint64_t>;
  using Contracts = Table<UIntT, Contract>;

 private:
  Contracts _contracts;
  Status    _status;
  UIntT     _pid;

  // union {
  //   ProposeSt proposer;
  //   AcceptSt  accepter;
  //   LearnSt   learner;
  // } _info;

 public:
  explicit StateT(UIntT pid, std::size_t mxcontract = 1000, Status status = Disable): 
    _contracts{mxcontract, [](uint64_t key) -> int{ return key; }},
    _status{status}, _pid{pid} {}


 public:
  Status status(){ return _status; }
  UIntT  id(){ return _pid; }
  
};

Node::Node(UIntT pid, UIntT type, std::function<base::Error(Message&)>&& snort):
     _snort{snort} {
  if ((type & Accepter))
    _states[ACCEPTOR] = std::shared_ptr<StateT>(new StateT{pid});
  if ((type & Learner))
    _states[LEARNER] = std::shared_ptr<StateT>(new StateT{pid});
  if ((type & Proposer))
    _states[PROPOSER] = std::shared_ptr<StateT>(new StateT{pid});
}

Node::~Node(){ }

Node::Status Node::learn_(Message& msg){
  switch (msg.type()){
  case Message::Reset:
    break;

  case Message::Proposal:
    /* @NOTE: mot loi de nghi tu proposer
     * - dua tren trang thai cua ban than de quyet dinh xem co nen tra loi
     * hay khong
     */
    break;

  case Message::Promised:
    /* @NOTE: mot accepter nao do hua hen voi mot proposer ve mot loi de nghi
     * - 
     */
    break;

  case Message::Deny:
    /* @NOTE: co mot accepter nao do tu choi loi de nghi cua mot proposer
     * -
     */
    break;

  case Message::Accepted:
    /* @NOTE: co mot proposer nao do gui mot loi dong y cho su hua hen cua
     * accepter
     * - 
     */
    break;

  case Message::Finished:
    break;
  }

  return _states[LEARNER]->status();
}

Node::Status Node::accept_(Message& msg){
  switch (msg.type()){
  case Message::Reset:
    break;

  case Message::Proposal:
    /* @NOTE: mot loi de nghi tu proposer, check xem co thap hon gia tri hien 
     * tai hay khong de tra loi
     * - 
     */
    break;

  case Message::Promised:
    /* @NOTE: mot accepter nao do hua hen voi mot proposer ve mot loi de nghi
     * - 
     */
    break;

  case Message::Deny:
    /* @NOTE: co mot accepter nao do tu choi loi de nghi cua mot proposer
     * - 
     */
    break;

  case Message::Accepted:
    /* @NOTE: co mot proposer nao do gui mot loi dong y cho su hua hen cua
     * accepter
     * - 
     */
    break;

  case Message::Finished:
    break;
  }

  return _states[ACCEPTOR]->status();
}

Node::Status Node::propose_(Message& msg){
  switch (msg.type()){
  case Message::Reset:
    break;

  case Message::Proposal:
    /* @NOTE: mot loi de nghi duoc gui tu mot ai do
     * - cac cell proposer khac gui loi de nghi toi cac acceptor de tim
     * kiem doi tac.
     * - ta can ghi nhan lai cac gia tri de luon chon gia tri lon nhat (?)
     */
    break;

  case Message::Promised:
    /* @NOTE: acceptor dong y voi loi de nghi va hua se khong chap nhan 
     * ket qua thap hon.
     * - mot acceptor dong y voi loi de nghi nay va tra ve ket qua va tra ve 
     * - khi bat duoc ket qua nay thi luu lai de luon chon gia tri lon nhat (?)
     * - 
     */
    break;

  case Message::Deny:
    /* @NOTE: mot loi de nghi bi tu choi boi mot acceptor
     * - Loi tu choi co gia tri tren toan he thong vi acceptor nay se khong chap
     * nhan mot yeu cau co gia tri thap hon gia tri cua message nay
     * - Neu gia tri de nghi dang nho hon hoac bang gia tri nay va dang o status 
     * offline thi can nhanh chong nang gia tri len cao hon 1 don vi so voi gia 
     * tri da gui cua acceptor
     */
    break;

  case Message::Accepted:
    /* @NOTE: mot proposer nao do gui mot loi dong y toi mot accepter nao do
     * - 
     */
    break;

  case Message::Finished:
    /* @NOTE: mot ket noi da duoc thiet lap giua accepter va mot proposer, gia tri
     * cua thoa thuan se duoc dung lam co so cho cac cuoc thoa thuan ke tiep */
    break;
  }

  return _states[PROPOSER]->status();
}
}  // namespace isolation
}  // namespace base
