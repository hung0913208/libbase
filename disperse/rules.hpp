#if !defined(LIBBASE_DISPERSE_RULES_HPP_) && __cplusplus
#define LIBBASE_DISPERSE_RULES_HPP_

#include "../logcat.hpp"
#include "../trace.hpp"

#include <chrono>
#include <thread>

#include <functional>
#include <vector>
#include <random>
#include <tuple>

namespace base{
template <typename Type> struct Rules {
 public:
  enum Event{Init, UnInit, Append, Remove, };
  using Track  = std::function<void(Event, Type)>;

 private:
  typedef std::size_t Index;
  typedef std::size_t Status;

  using Handle = std::function<base::Error(Type&)>;
  using Stages = std::vector<std::tuple<Status, Index>>;

  struct Node {
   private:
    Index  _next, _prev;
    Stages _stages;
    Status _status;
    Type   _context;
    bool   _isolated;

   public:
    explicit Node(Type& context, std::tuple<Status, Index>&& status, bool isolated = false):
        _status{0}, _context{context}, _isolated{isolated}{
      _stages.push_back(status);
      _next = (_prev = std::string::npos);
    }

    explicit Node(Type& context, bool isolated = false):
        _context{context}, _status{0}, _isolated{isolated}{
      _next = (_prev = std::string::npos);
    }

    Node(const Node& node){
      _next = node._next;
      _prev = node._prev;
      _stages = node._stages;
      _status = node._status;
      _context = node._context;
      _isolated = node._isolated;
    }

   public:
    inline Type&   context(){ return _context; }
    inline Stages& stages(){ return _stages; }
    inline Status  status(){ return std::get<0>(_stages[_status]); }
    inline Index   step(){ return static_cast<Index>(_status); }
    inline Index   handle(){ return std::get<1>(_stages[_status]); }
    inline Index&  next(){ return _next; }
    inline Index&  previous(){ return _prev; }
    inline bool&   isolated(){ return _isolated; }

   public:
    inline Status up(std::size_t degree){
      _status = (_status + degree) % _stages.size();
      return std::get<0>(_stages[_status]);
    }

    inline Status down(std::size_t degree){
      if (degree > _status)
        _status = _stages.size() - ((degree - _status) % _stages.size());
      else _status -= degree;
      return std::get<0>(_stages[_status]);
    }

    inline Status init(Status status){
      auto result = find(status);

      if (result != _stages.end())
        _status = result - _stages.begin();
      return std::get<0>(_stages[_status]);
    }

    inline Stages::iterator find(Status status){
      for (auto i = 0; i < cast_(i, _stages.size()); ++i)
        if (std::get<0>(_stages[i]) == status)
          return _stages.begin() +  i;
      return _stages.end();
    }

    inline bool support(Status status){ return find(status) != _stages.end(); }

    inline bool remove(Status status){
      auto result = find(status);

      if (result != _stages.end() && cast_(_status, (result - _stages.begin())) != _status){
        auto status = std::get<0>(_stages[_status]);

        _stages.erase(result);
        _status = find(status) - _stages.begin();
        return true;
      }
      return false;
    }
  };

  struct Link {
   private:
    Node* _node;
    Index _index;

   public:
    explicit Link(Index index, Node& node): _node{&node}, _index{index} {}
    explicit Link(): _node{nullptr}, _index{std::string::npos} {}
    explicit Link(const Link& link){
      _node  = link._node;
      _index = link._index;
    }

   public:
    inline Node& node(){ return *_node; }
    inline Index& index(){ return _index; }
  };

 public:
  explicit Rules(std::function<base::Error(Rules<Type>&)> sync = nullptr,
                 std::size_t pending = std::string::npos): _locked{false}{
     _sync    = sync;
     _pending = pending;
     _size    = 0;
     _online  = 0;
  }

 public:
  base::Error append(Type&& context, std::tuple<Status, Index>&& status){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    if (std::get<0>(status) > _current.size())
      return BadLogic;
    else if (std::get<0>(status) == _current.size())
      _current.push_back(std::string::npos);

    if (_links.find(context) == _links.end()){
      auto idx = 0;

      _nodes.push_back(Node(context, rvalue(status)));
      for (auto& node : _nodes)
        _links[node.context()] = Link(idx++, node);
      _size++;
    } else {
      auto& node = _links[context].node();

      for (auto& saved : node.stages())
        if (std::get<0>(status) == std::get<0>(saved)){
          if (std::get<1>(status) == std::get<1>(saved)){
            return BadLogic.reason("stage " + 
              std::to_string(std::get<0>(status)) + " has been inited");
          } else {
            std::get<1>(saved) = std::get<1>(status);
            return NoError;
          }
        }
      node.stages().push_back(status);
    }

    triggerBy(Append, context);
    return NoError;
  }

  base::Error append(Type& context, std::tuple<Status, Index>&& status){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || 
            at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    if (std::get<0>(status) > _current.size())
      return BadLogic;
    else if (std::get<0>(status) == _current.size())
      _current.push_back(std::string::npos);

    if (_links.find(context) == _links.end()){
      auto idx = 0;

      _nodes.push_back(Node(context, rvalue(status)));
      for (auto& node : _nodes)
        _links[node.context()] = Link(idx++, node);
      _size++;
    } else {
      auto& node = _links[context].node();

      for (auto& saved : node.stages())
        if (std::get<0>(status) == std::get<1>(saved)){
          return BadLogic.reason("stage " + 
              std::to_string(std::get<0>(status)) + "has been inited");
        }
       node.stages().push_back(status);
    }

    triggerBy(Append, context);
    return NoError;
  }


  base::Error remove(Type&& context, Status status = std::string::npos){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    if (_links.find(context) == _links.end())
      return NotFound;
    if (status == std::string::npos){
      auto index = _links[context].index();
      auto node  = _links[context].node();

      for (auto i = index + 1; i < _nodes.size(); ++i){
        auto& link = _links[_nodes[i].context()];

        down(at(link.node().next()).previous());
        down(at(link.node().previous()).next());

        if (id(link) > 0) id(link)--;
      }

      _links.erase(_nodes[index].context());
      _nodes.erase(_nodes.begin() + index);
    } else if (!at(context).remove(status))
      return BadAccess;

    _size--;
    triggerBy(Remove, context);
    return NoError;
  }

  base::Error remove(Type& context, Status status = std::string::npos){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    if (_links.find(context) == _links.end())
      return NotFound;
    if (status == std::string::npos){
      auto index = _links[context].index();
      auto node  = _links[context].node();

      for (auto i = index + 1; i < _nodes.size(); ++i){
        auto& link = _links[_nodes[i].context()];

        down(at(link.node().next()).previous());
        down(at(link.node().previous()).next());

        if (id(link) > 0) id(link)--;
      }

      _links.erase(_nodes[index].context());
      _nodes.erase(_nodes.begin() + index);
    } else if (status == at(_links[context]).status())
      return BadLogic;
    else {
      auto& node = at(_links[context]);
      auto  iter = node.find(status);

      if (iter == node.stages().end())
        return NotFound;
      else node.stages().erase(iter);
    }

    _size--;
    triggerBy(Remove, context);
    return NoError;
  }

  bool uninit(Type&& context){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    try{
      if (_links.find(context) == _links.end())
        return false;
      else if (_current[at(context).status()] == id(context)){
        if (at(context).next() != id(context))
          _current[at(context).status()] = at(context).next();
        else
          _current[at(context).status()] = std::string::npos;
      }

      if (at(context).previous() != std::string::npos && at(context).next() != std::string::npos){
        at(at(context).previous()).next() = at(context).next();
        at(at(context).next()).previous() = at(context).previous();
        at(context).next() = std::string::npos;
        at(context).previous() = std::string::npos;
      }

      _online--;
      triggerBy(UnInit, context);
      return true;
    } catch(base::Error&){ return false; }
  }

  bool uninit(Type& context){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    try{
      if (_links.find(context) == _links.end())
        return false;
      else if (_current[at(context).status()] == id(context)){
        if (at(context).next() != id(context))
          _current[at(context).status()] = at(context).next();
        else
          _current[at(context).status()] = std::string::npos;
      }

      if (at(context).previous() != std::string::npos && at(context).next() != std::string::npos){
        at(at(context).previous()).next() = at(context).next();
        at(at(context).next()).previous() = at(context).previous();
        at(context).next() = std::string::npos;
        at(context).previous() = std::string::npos;
      }

      _online--;
      triggerBy(UnInit, context);
      return true;
    } catch(base::Error&){ return false; }
  }

  bool init(Type&& context, std::size_t stage){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    try{
      auto status = std::string::npos;

      if (_links.find(context) == _links.end())
        return false;
      else if (at(context).stages().size() < stage)
        return false;

      status = at(context).init(stage);

      triggerBy(Init, context);
      if (_current[status] != std::string::npos){
        at(context).next() = _current[status];
        at(context).previous() = at(_current[status]).previous();

        at(_current[status]).previous() = id(context);
        at(at(context).previous()).next() = id(context);
      } else{
        _current[status] = id(context);

        at(context).next() = id(context);
        at(context).previous() = id(context);
      }

      _online++;
      return true;
    } catch(base::Error&){ return false; }
  }

  bool init(Type& context, std::size_t stage){
    auto locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (_links.find(context) == _links.end() || at(context).isolated())))
          _lock.lock(); 
      }, 
      [&](){ 
        if (_pending != std::string::npos) while(_sync(*this))
          std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
        if (locked) _lock.unlock(); 
      }
    };

    try{
      auto status = std::string::npos;

      if (_links.find(context) == _links.end())
        return false;
      else if (at(context).stages().size() < stage)
        return false;


      status = at(context).init(stage);

      triggerBy(Init, context);
      if (_current[status] != std::string::npos){
        at(context).next() = _current[status];
        at(context).previous() = at(_current[status]).previous();

        at(_current[status]).previous() = id(context);
        at(at(context).previous()).next() = id(context);
      } else{
        _current[status] = id(context);

        at(context).next() = id(context);
        at(context).previous() = id(context);
      }

      _online++;
      return true;
    } catch(base::Error&){ return false; }
  }

 public: /* @NOTE: basic controls */
  Type& select(Status status = std::string::npos, bool isolate = false){
    bool locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = (status == std::string::npos || 
                     !at(_current[status]).isolated())))
          _lock.lock();
      },
      [&](){ if (locked) _lock.unlock(); }
    };

    if (status == std::string::npos){
      bool keep{false};

      if (_size == 0 || _online == 0) throw DoNothing;
      for (status = 0; status < cast_(status, _current.size()); ++status){
        try{
          if (_current[status] == std::string::npos)
            continue;
          else if (at(_current[status]).isolated())
            continue;
          else if (_current[status] == std::string::npos)
            continue;
          else keep = true;
          break;
        } catch(base::Error& error){ throw base::Error{error}; }
      }
      if (!keep){
        _online = 0;
        throw DoNothing;
      }
    }

    if (status != std::string::npos){
      try{
        if (_current[status] != std::string::npos) {
          auto& result = at(_current[status]).context();

          at(_current[status]).isolated() = isolate;
          _current[status] = at(_current[status]).next();

          if (_pending != std::string::npos) 
            while(_sync(*this))
              std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
          return result;
        }
      } catch(base::Error &error){ throw base::Error{error}; }
      throw OutOfRange.reason("stage " + std::to_string(status) + " is empty");
    } else throw DoNothing;
  }

  Handle up(Type& context, int degree = 1){
    bool locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = !at(context).isolated()))
          _lock.lock();
      },
      [&](){ if (locked) _lock.unlock(); }
    };
    return change(context, degree);
  }

  Handle down(Type& context, int degree = 1){
    bool locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = !at(context).isolated()))
          _lock.lock();
      },
      [&](){ if (locked) _lock.unlock(); }
    };
    return change(context, -degree);
  }

  Handle wait(Type& context){
    bool locked = false;
    base::Boundary boundary{
      [&](){
        if ((locked = !at(context).isolated()))
          _lock.lock();
      },
      [&](){ if (locked) _lock.unlock(); }
    };
    return change(context, 1);
  }

  Handle seek(Type& context, int offset, int whence){
    switch(whence){
      case SEEK_SET:
        if (offset > cast_(offset, step(context)))
          return up(context, std::size_t(offset) - step(context));
        else
          return down(context, step(context) - offset);

      default:
      case SEEK_CUR:
        return up(context, offset);

      case SEEK_END:
        return up(context, at(context).stages().size() + offset);
    }
  }

  Handle seek(Type& context, Status status){
    auto index = at(context).find(status) - at(context).stages().begin();

    if (index < cast_(index, at(context).stages().size()))
      return seek(context, index, SEEK_SET);

    throw NoSupport;
  }

  bool isolate(Index index, bool flag = true, bool resync = true){
    try{
      if (index >= _current.size())
        return false;
      else
        at(index).isolated() = flag;

      while(resync && _sync && !_sync(*this)) 
        std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
    } catch(base::Error& error){
      if (error.code == base::error::EOutOfRange)
        return false;
      throw base::Error{error};
    }

    return true;
  }

  bool isolate(Type& context, bool flag = true, bool resync = true){
    try{
      at(context).isolated() = flag;

      while(resync && _sync && !_sync(*this)) 
        std::this_thread::sleep_for(std::chrono::milliseconds(_pending));
    } catch(base::Error& error){
      if (error.code == base::error::EOutOfRange)
        return false;
      throw base::Error{error};
    }

    return true;
  }

 public: /* @NOTE: advance controls */
  Handle upWhen(Type& context, Handle handle, int degree = 1){
    return (handle(context))? handle: up(context, degree);
  }

  Handle downWhen(Type& context, Handle handle, int degree = 1){
    return (handle(context))? handle: down(context, degree); }

  base::Error tryUp(Type& context, int degree = 1){
    auto error = handle(context);

    if (!error) up(context, degree);
    return error;
  }

  base::Error tryDown(Type& context, int degree = 1){
    auto error = handle(context);

    if (!error) down(context, degree);
    return error;
  }

  base::Error tryWait(Type& context){
    auto error = handle(context);

    if (!error) wait(context);
    return error;
  }

 public: /* @NOTE: status tracking */
  const Stages& stages(Type& context){ return at(context).stages(); }

  Status status(Type& context){ return at(context).status(); }

  Index  step(Type& context){ return at(context).step(); }

  base::Error handle(Type& context){ return _handles[at(context).handle()](context); }

  void track(Track catcher){ _tracks.push_back(catcher); }

  std::size_t online(){ return _online; }

 public:
  Index append(Handle handle){
    try { _handles.push_back(handle); }
    catch (...){ return std::string::npos; }

    return _handles.size() - 1;
  }

  Index modify(Index index, Handle handle){
    if (index >= _handles.size())
      return std::string::npos;
    if (!handle) _handles[index] = handle;
    else 
      _handles[index] = [](Type&) -> base::Error{ return NoSupport; };
    return index;
  }

  Index modify(std::string name_, Handle handle){
    if (find(name_) == std::string::npos)
      return std::string::npos;
    return modify(find(name_), handle);
  }

  Index modify(Type& context, Handle handle){
    if (_stickies.find(context) == _stickies.end())
      return std::string::npos;
    return modify(_stickies[context], handle);
  }

  Index find(Handle handle){
    auto result = _handles.find(handle);

    if (result != _handles.end())
      return result - _handles.begin();
    return std::string::npos;
  }

  Index find(std::string name_){
    if (_names.find(name_) == _names.end())
      return std::string::npos;
    return _names[name_];
  }

  base::Error name(std::string name_, Index handle){
    if (_names.find(name_) != _names.end())
      return BadLogic;
    _names[name_] = handle;
    return NoError;
  }

  base::Error stick(Index index, Type& context){
    if (_stickies.find(context) != _stickies.end())
      return BadLogic;
    _stickies[context] = index;
    return NoError;
  }

  base::Error unstick(Type& context){
    if (_stickies.find(context) == _stickies.end())
      return BadLogic;
    _stickies.erase(context);
    return NoError;
  }

 public:
  std::vector<Handle>& handles(){ return _handles; }

  std::vector<Node>& nodes(){ return _nodes; }

 private:
  Handle change(Type& context, int degree){
    auto handleId   = at(context).handle();
    auto oldStatus  = at(context).status();
    auto nextStatus = std::string::npos;

    if (degree > 0)
      nextStatus = at(context).up(degree);
    else
      nextStatus = at(context).down(abs(degree));

    /* @NOTE: release node cu khoi dequeue, se chi can neu dequeue con node dang cho */
    try{
      if (departFrom(oldStatus)){
        at(at(context).next()).previous() = at(context).previous();
        at(at(context).previous()).next() = at(context).next();

        if (_current[oldStatus] == id(context))
          _current[oldStatus] = at(context).next();
      } else _current[oldStatus] = std::string::npos;


      if (_current[nextStatus] == std::string::npos){
        _current[nextStatus]   = id(context);

        at(context).previous() = id(context);
        at(context).next()     = id(context);
      } else{
        at(context).next() = _current[nextStatus];
        at(context).previous() = at(_current[nextStatus]).previous();

        at(at(context).previous()).next() = id(context);
        at(_current[nextStatus]).previous()  = id(context);
      }
      return (_handles.size() <= handleId)? nullptr:_handles[handleId];
    } catch(base::Error&){ return nullptr; }
  }

 private:
  Node& at(Index index){
    if (index != std::string::npos && index < _nodes.size())
      return _links[_nodes[index].context()].node(); 
    throw OutOfRange;
  }

  Node& at(Link& link){ return link.node(); }

  Node& at(Type& context){
    if (_links.find(context) != _links.end())
      return at(_links[context]);
    else throw OutOfRange;
  }

  Index& id(Index index){
    if (index != std::string::npos && index < _nodes.size())
      return _links[_nodes[index].context()].index(); 
    throw OutOfRange;
  }

  Index& id(Link& link){ return link.index(); }

  Index& id(Type& context){ return id(_links[context]); }

  void down(Index& index){ if (index > 0) index--; }

  void up(Index& index){ if (index + 1 < _nodes.size()) index++; }

 private:
  bool departFrom(Index stage){
    if (_online == 0) return false;
    try{
      return _current[stage] != at(_current[stage]).previous() && 
             _current[stage] != at(_current[stage]).next();
    } catch(base::Error&){ return false; }
  }

  void triggerBy(Event event, Type& context){
    for (auto track: _tracks)
      track(event, context);
  }

 private:
  std::function<base::Error(Rules<Type>&)> _sync;
  std::size_t _pending, _size, _online;
  std::mutex _lock;
  bool _locked;

 private:
  std::map<Type, Link>  _links;
  std::vector<Handle>   _handles;
  std::vector<Node>     _nodes;
  std::vector<Index>    _current;
  std::vector<Track>    _tracks;

 private:
  std::map<std::string, Index> _names;
  std::map<Type, Index> _stickies;
};
} // namespace base
#endif // LIBBASE_DISPERSE_RULES_HPP_
