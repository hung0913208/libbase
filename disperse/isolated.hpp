#if !defined(LIBCGRAPH_ISOLATE_HPP_) && __cplusplus
#define LIBCGRAPH_ISOLATE_HPP_

#include "../logcat.hpp"

#include <functional>
#include <memory>

namespace base{
namespace isolation{
struct Message{
 public:
  using UIntT = uint64_t;

  enum Type{
    Reset    = -1,
    Proposal =  0,
    Promised =  1,
    Accepted =  2,
    Finished =  3,
    Deny     =  4
  };

 protected:
  struct Implement;

 protected:
  Message();

 public:
  inline Type type(){ return _type; }

  inline bool iostat(){ return _iostat; }

  inline UIntT proposed(){ return _proposed; }

  inline UIntT proposal(){ return _proposal; }

 private:
  UIntT _proposed, _proposal;
  UIntT _count, _id;
  Type  _type;
  bool  _iostat;
};

struct Node{
 public:
  enum NodeType{ Accepter = 1, Learner = 2, Proposer = 4 };
  enum Status{ Disable = -1, Idle = 0 };

 private:
  struct StateT;

  using UIntT = uint64_t;
  using Snort = std::function<base::Error(Message&)>;
  using State = std::shared_ptr<StateT>;

 public:
  explicit Node(UIntT pid, UIntT type, std::function<base::Error(Message&)>&& snort);
  virtual ~Node();

 public:
  UIntT id(){ return _id; }

 protected:
  Status learn_(Message& msg);
  Status accept_(Message& msg);
  Status propose_(Message& msg);

 private:
  State  _states[3];
  Snort  _snort;
  UIntT  _id;
};
} // namespace isolation
} // namespace base
#endif // LIBCGRAPH_ISOLATE_HPP_
