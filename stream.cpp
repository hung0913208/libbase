#include "stream.hpp"
#include "pipe.hpp"

#include <cstdio>
#include <cstdlib>

namespace base{
base::Error Stream::rByte(uint8_t& byte, bool reverse_){
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&byte)};
  Error  error{read(buffer, sizeof(byte))};

  if (error) return error;
  else if (reverse_)
    byte = reverse(byte);
  return NoError;
}

base::Error Stream::rWord(uint16_t& word, bool reverse_){
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&word)};
  Error  error{read(buffer, sizeof(word))};

  if (error) return error;
  else if (reverse_)
    word = reverse(word);
  return NoError;

}

base::Error Stream::rDWord(uint32_t& dword, bool reverse_){
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&dword)};
  Error  error{read(buffer, sizeof(dword))};

  if (error) return error;
  else if (reverse_)
    dword = reverse(dword);
  return NoError;

}

base::Error Stream::rLWord(uint64_t& lword, bool reverse_){
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&lword)};
  Error  error{read(buffer, sizeof(lword))};

  if (error) return error;
  else if (reverse_)
    lword = reverse(lword);
  return NoError;

}

uint8_t  Stream::rByte(base::Error& error, bool reverse_){
  uint8_t result{0};

  if ((error = read(&result, sizeof(result))))
    return 0;
  else if (reverse_)
    result = reverse(result);
  return result;
}

uint16_t Stream::rWord(base::Error& error, bool reverse_){
  uint16_t result{0};
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&result)};
  if ((error = read(buffer, sizeof(result))))
    return 0;
  else if (reverse_)
    result = reverse(result);
  return result;

}

uint32_t Stream::rDWord(base::Error& error, bool reverse_){
  uint32_t result{0};
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&result)};

  if ((error = read(buffer, sizeof(result))))
    return 0;
  else if (reverse_)
    result = reverse(result);
  return result;

}

uint64_t Stream::rLWord(base::Error& error, bool reverse_){
  uint64_t result{0};
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&result)};

  if ((error = read(buffer, sizeof(result))))
    return 0;
  else if (reverse_)
    result = reverse(result);
  return result;
}

uint8_t Stream::rByte(bool){
  uint8_t result{0};
  Error   error{read(&result, sizeof(result))};

  if (error) throw error;
  return result;
}

uint16_t Stream::rWord(bool reverse_){
  uint16_t result{0};
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&result)};
  Error  error{read(buffer, sizeof(result))};

  if (error) throw error;
  else if (reverse_)
    result = reverse(result);
  return result;
}

uint32_t Stream::rDWord(bool reverse_){
  uint32_t result{0};
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&result)};
  Error error{read(buffer, sizeof(result))};

  if (error) throw error;
  else if (reverse_) result = reverse(result);
  return result;
}

uint64_t Stream::rLWord(bool reverse_){
  uint64_t result{0};
  uint8_t *buffer{reinterpret_cast<uint8_t*>(&result)};
  Error error{read(buffer, sizeof(result))};

  if (error) throw error;
  else if (reverse_) result = reverse(result);
  return result;
}

base::Error Stream::wByte(uint8_t byte, bool){
  return write(&byte, sizeof(byte));
}

base::Error Stream::wWord(uint16_t word, bool reverse_){
  uint8_t *bytes{reinterpret_cast<uint8_t*>(&word)};

  for (auto i = 0; reverse_ && i < cast_(i, sizeof(word))/2; ++i)
    std::swap(bytes[i], bytes[sizeof(word) - i]);

  return write(bytes, sizeof(word));
}

base::Error Stream::wDWord(uint32_t dword, bool reverse_){
  uint8_t *bytes{reinterpret_cast<uint8_t*>(&dword)};

  for (auto i = 0; reverse_ && i < cast_(i, sizeof(dword))/2; ++i)
    std::swap(bytes[i], bytes[sizeof(dword) - i]);
  return write(bytes, sizeof(dword));
}

base::Error Stream::wLWord(uint64_t lword, bool reverse_){
  uint8_t *bytes{reinterpret_cast<uint8_t*>(&lword)};

for (auto i = 0; reverse_ && i < cast_(i, sizeof(lword))/2; ++i)
    std::swap(bytes[i], bytes[sizeof(lword) - i]);

  return write(bytes, sizeof(lword));
}

Buffer::Buffer(): Cache{}, _cref{nullptr}{
  _cbuff._begin = nullptr;
  _cbuff._end = nullptr;

  _cbuff._size = nullptr;
  _cbuff._buffer = nullptr;
  _cbuff._left = nullptr;
}

Buffer::Buffer(const Buffer& src): Cache{} {
  _cbuff._buffer = src._cbuff._buffer;
  _cbuff._begin  = src._cbuff._begin;
  _cbuff._end    = src._cbuff._end;

  _cbuff._left = src._cbuff._left;
  _cbuff._size = src._cbuff._size;
  _max     = src._max;
  _cref    = src._cref;
  _autodel = src._autodel;
  _inited  = src._inited;

  if (_cref) (*_cref)++;
}

Buffer::~Buffer(){
  if (_cref){
    if (*_cref == 0){
      if (_autodel && _cbuff._buffer)
        delete [] _cbuff._buffer;
      if (_inited){
        delete _cbuff._begin;
        delete _cbuff._end;
      }
      delete _cref;
      delete _cbuff._left;
      delete _cbuff._size;
    } else (*_cref)--;
  }
}

bool Buffer::write(uint8_t byte){
  if (size() == max())
    return false;
  else (*_cbuff._end)++;
  if (*_cbuff._end > *_cbuff._size - 1){
    *_cbuff._left = !*_cbuff._left;
    *_cbuff._end = *_cbuff._end % *_cbuff._size;
  }

  if (!_cbuff._buffer) return false;
  _cbuff._buffer[*_cbuff._end] = byte;
  return true;
}

bool Buffer::revert(int count){
  if (abs(count) > cast_(count, size())) 
    return false;
  else if (0 <= *_cbuff._begin - count && *_cbuff._begin - count < cast_(count, max()))
    *_cbuff._begin -= count;
  else{
    *_cbuff._left = !*_cbuff._left;
    if (count > 0) *_cbuff._begin += max() - count;
    else 
      *_cbuff._begin += abs(count) - cast_(count, max());
  }
  return true;
}

uint8_t Buffer::read(){
  if (size() == 0)
      throw OutOfRange;
  else {
    auto pos = (*_cbuff._begin)++;

    if (*_cbuff._begin > *_cbuff._size){
      *_cbuff._begin = (pos = pos % *_cbuff._size) + 1;
      *_cbuff._left = !*_cbuff._left;
    }

    if (!_cbuff._buffer) throw BadAccess;
    return _cbuff._buffer[pos];
  }
}

void Buffer::reset(){ *_cbuff._begin = (*_cbuff._end = 0); }

std::size_t Buffer::size(){
  if (!_cbuff._begin || !_cbuff._end) return 0;
  if (*_cbuff._begin <= *_cbuff._end)
    return *_cbuff._left? *_cbuff._end - *_cbuff._begin + 1: 0;
  else return *_cbuff._left? 0: max() - (*_cbuff._begin - *_cbuff._end) + 1;
}

std::size_t Buffer::max(){ return _cbuff._size? *_cbuff._size: 0; }

Buffer& Buffer::range(std::size_t max){
  if (cast_(_max, max) > _max)
    throw OutOfRange;
  *_cbuff._size = max;
  reset();
  return *this;
}

bool Buffer::full(){ return size() == max(); }

bool Buffer::empty(){ return size() == 0; }

int Buffer::cref(){ return *_cref; }

Rack::Rack(std::string&& path, std::size_t bsize):
    Cache{}, _buffer{}, _fd{nullptr} {
  if (bsize > 0 && bsize != std::string::npos)
    _buffer.init(new uint8_t[bsize], bsize);
  init(rvalue(path));
}

Rack::Rack(std::string&& path, Buffer&& buffer):
    Cache{}, _buffer{buffer}, _fd{nullptr} {
  init(rvalue(path)); 
}

Rack::Rack(std::string&& path, Buffer&  buffer):
    Cache{}, _buffer{buffer}, _fd{nullptr} {
  init(rvalue(path));
}

Rack::Rack(std::string& path, std::size_t bsize):
    Cache{}, _buffer{}, _fd{nullptr} {
  if (bsize > 0 && bsize != std::string::npos) 
    _buffer.init(new uint8_t[bsize], bsize);
  init(path);
}

Rack::Rack(std::string& path, Buffer&& buffer):
    Cache{}, _buffer{buffer}, _fd{nullptr} {
  init(path);
}

Rack::Rack(std::string& path, Buffer&  buffer):
    Cache{}, _buffer{buffer}, _fd{nullptr} {
  init(path);
}

Rack::~Rack(){
  /* @TODO: thuc hien qua trinh dong goi */
}

FILE* Rack::fdesc(){ return _fd; }

void Rack::init(std::string&& path){
  if (!(_fd = fopen(path.c_str(), "wr")))
    throw NotFound.reason(strerror(errno));
  /* @TODO: thuc hien qua trinh parsing */
}

void Rack::init(std::string& path){
  if (!(_fd = fopen(path.c_str(), "wr")))
    throw NotFound.reason(strerror(errno));
  /* @TODO: thuc hien qua trinh parsing */
}

bool Rack::write(uint8_t UNUSED(byte)){ return false; }

uint8_t Rack::read(){ return 0; }

bool Rack::revert(int UNUSED(count)){ return 0; }

void Rack::reset(){ }

std::size_t Rack::size(){ return 0; }

std::size_t Rack::max(){ return 0; }
} // namespace base

#if !__APPLE__
base::Deserialize<>& operator+=(base::Deserialize<>& deserialize, base::Stream&& stream){
  while ((deserialize << squeeze(char, stream.rByte())) != -1){}
  return deserialize;
}

base::Serialize<>& operator-=(base::Serialize<>& serialize, base::Stream&& stream){
  while(true)
    try{ stream.wByte(serialize.read()); }
    catch(base::Error&){ break; }

  return serialize;
}

base::Deserialize<>& operator+=(base::Deserialize<>& deserialize, base::Stream& stream){
  while ((deserialize << squeeze(char, stream.rByte())) != -1){}
  return deserialize;
}

base::Serialize<>& operator-=(base::Serialize<>& serialize, base::Stream& stream){
  while(true)
    try{ stream.wByte(serialize.read()); }
    catch(base::Error&){ break; }

  return serialize;
}

base::Deserialize<>& operator+=(base::Deserialize<>& deserialize, std::shared_ptr<base::Stream>& stream){
  while ((deserialize << squeeze(char, stream->rByte())) != -1){}
  return deserialize;
}

base::Serialize<>& operator-=(base::Serialize<>& serialize, std::shared_ptr<base::Stream>& stream){
  while(true)
    try{ stream->wByte(serialize.read()); }
    catch(base::Error&){ break; }

  return serialize;
}
#endif

std::shared_ptr<base::Stream>& operator<<(std::shared_ptr<base::Stream>& dst, base::Buffer& buffer){
  for (auto i = buffer.size(); i > 0; --i){
    auto error = dst->wByte(buffer.read());

    if (error) throw error;
  }
  return dst;
}

std::shared_ptr<base::Stream>& operator>>(std::shared_ptr<base::Stream>& src, base::Buffer& buffer){
  base::Error error = NoError;

  for (auto i = buffer.size(); i < buffer.max(); ++i){
    try{ buffer.write(src->rByte()); }
    catch(base::Error& error){ throw base::Error{error}; }
  }
  return src;
}
