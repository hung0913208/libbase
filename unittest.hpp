#if !defined(LIBBASE_UNITTEST_HPP_) && __cplusplus
#define LIBBASE_UNITTEST_HPP_
#include "logcat.hpp"
#include "utils.hpp"
#include "trace.hpp"
#include "macros.hpp"

#include <functional>
#include <memory>
#include <string>
#include <vector>

#if defined(__GNUC__) && LINUX
#include <features.h>
#endif

#define VECTOR_TO_STRING(Type)                                                 \
inline std::string to_string(std::vector<Type>& vector){                       \
  std::string result = "[ ";                                                   \
                                                                               \
  for(auto i = 0; i < cast_(i, vector.size()); ++i)                            \
    result += std::to_string(vector[i]) + ", ";                                \
  result += "]";                                                               \
  return result;                                                               \
}

namespace std{
template<typename Type>
inline std::string to_string(Type& UNUSED(value)){ return ""; }

inline std::string to_string(const std::type_info& type){
  int status;

  std::unique_ptr<char[], void (*)(void*)> result(
    abi::__cxa_demangle(type.name(), 0, 0, &status), std::free);

  if (result.get())
    return std::string(result.get());
  throw NotFound;
}

inline std::string to_string(std::string&& string_){
  return string_;
}

inline std::string to_string(const char* string_){
  return string_;
}

inline std::string to_string(bool& value){
  return value? "True": "False";
}

VECTOR_TO_STRING(bool)
VECTOR_TO_STRING(int)
VECTOR_TO_STRING(long)
VECTOR_TO_STRING(float)
VECTOR_TO_STRING(double)
VECTOR_TO_STRING(std::string)
VECTOR_TO_STRING(const char*)
} // namespace std

namespace base{
struct Test;

namespace unittest{
extern Test* current;

struct Basic{
 public:
  virtual ~Basic(){}

 protected:
  virtual void define() = 0;
  virtual bool start() = 0;
};

struct Latch{
};
} // namespace unittest

struct Teardown: unittest::Basic{
 public:
  explicit Teardown(std::string&& suite);
  virtual ~Teardown();

 public:
  bool start() final;
};

struct Test: unittest::Basic{ 
 public:
  enum CaseT{ Unknown, Boolean, Equal, LessT, LessE, GreatT, GreatE, 
              Throw, NoThrow };
  enum Event{ Init, Expect, Assert };
  enum Status{ Fail, Pass, Ignore };

 public:
  /* @NOTE: report result after finish checking your program and allow you to
   * send report to your service, collect logs and pack artifact whenever 
   * segmentfailt or fail cases appear 
   */
  struct Summary{
   public:
    std::size_t index;
    std::string description;
    std::string infomation;

   public:
    Status status;
    bool   result;

   public:
    Summary(std::size_t index, Status status):
      index{index}, status{status}, result{false}{}

    void print(int level){
      switch(status){
      case Ignore:
        if (level > 0) break;

        trace::console << "Check #" << index << ": ";
        if (!result){
          trace::console << (Color{Color::Yellow} << "[ FAIL -> IGNORED ]")
                         << description << trace::eol;
          trace::console << infomation << trace::eol;
        } else
          trace::console << (Color{Color::Yellow} << "[ OKEY -> IGNORED ]")
                         << description << trace::eol;
        break;

      case Fail:
        trace::console << "Check #" << index << ":" << description << " ";
        trace::console << (Color{Color::Red} << "[           FAIL ]")
                       << trace::eol;
        trace::console << infomation << trace::eol;
        break;

      case Pass:
        if (level <= 0) break;
        trace::console << "Check #" << index << ":" << description;
        trace::console << (Color{Color::Yellow} << "[          OKEY ]")
                       << trace::eol;
        trace::console << infomation << trace::eol;
      }
    }
  };

  /* @NOTE: by default, test case will run from begin to end and check if your
   * program is working as your expected. However, many cases are base on 
   * shared-libraries or APIs and we can't check whether or not your program
   * still working well under failing cases when remoted functions is not pass
   * Therefore, Failover would be annouced to cover these cases by simulate your 
   * program activation when it's on fail states 
   */
  struct Failover{
  };

 public:
  explicit Test(const char* suite, const char* name);
  virtual ~Test();

 public:

 protected:
  void render(Event event, std::string file, int line);

 public:
  inline std::string&& suite(){ return rvalue(_name); }
  inline std::string&& name(){ return rvalue(_suite); }

 public:
  inline Test& mock(const std::string&& file, int line){
    _file = file;
    _line = line;
    return *this;
  }

  inline Test& require(bool expect){
    _expect = expect;
    return *this;
  }

  inline Test& unitcase(CaseT type){
    _case = type;
    return *this;
  }

  inline Test& fatal(bool value){
    _fatal = value;
    return *this;
  }

 public:
  bool start() final;
  void error(base::Error& error_);

  bool check(bool condition, std::string name){
    _status = _ignore? _status: (_status? condition: _status);
    _case = Boolean;

    if (!condition){
      header((_expect? "": "!(") + name + (_expect? "": ")"));
      expect("Expected:", _expect? "True": "False");
      actual("  Actual:", _expect? "False": "True");
      footer();
      return true;
    } else return false;
  }

  bool check(bool condition, std::string test_descr, std::string explane){
    _status = _ignore? _status: (_status? condition: _status);

    if (_case == NoThrow){
      if (!condition){
        header("NOEXCEPT(" + test_descr + ")");
        expect("Expected:", rvalue(test_descr), "doesn\'t throw", "anything");
        actual("  Actual:", rvalue(explane));
        return true;
      }
    } else return BadLogic;
    return false;
  }

  bool check(bool condition, std::string test_descr, std::string catcher_descr,
             std::string explane){
    _status = _ignore? _status: (_status? condition: _status);
    if (_case == Throw){
      if (!condition){
        header("THROWED(" + test_descr + ", " + catcher_descr + ")");
        expect("Expected:", rvalue(test_descr), "throw", rvalue(catcher_descr));
        actual("  Actual:", rvalue(explane));
        footer();
        return true;
      }
    } else return BadLogic;
    return false;
  }

 #if __GNUC__ && !defined(__clang__) && LINUX
 #if __GNUC_PREREQ(5,0)
  template<typename Left, typename Right>
  bool equal(bool condition, Left& left, std::string left_descr,
                             Right& right, std::string right_descr){
    condition = condition == (left == right);
    _status = _ignore? _status: (_status? condition: _status);

    if (!condition){
      header(left_descr + (_expect? " == ": " != ") + right_descr);
      actual("  Actual:", std::to_string(left) + " != " + std::to_string(right));
      expect("Expected:", rvalue(left_descr), "==", rvalue(right_descr));
      footer();
      return true;
    }

    return false;
  }

  template<typename Left, typename Right>
  bool equal(bool condition, Left&& left, std::string left_descr,
                             Right&& right, std::string right_descr){
    condition = condition == (left == right);
    _status = _ignore? _status: (_status? condition: _status);

    if (!condition){
      header(left_descr + (_expect? " == ": " != ") + right_descr);
      actual("  Actual:", std::to_string(left) + " != " + std::to_string(right));
      expect("Expected:", rvalue(left_descr), "==", rvalue(right_descr));
      footer();
      return true;
    }

    return false;
  }

  template<typename Left, typename Right>
  bool compare(bool condition, Left&& left, std::string left_descr,
                             Right&& right, std::string right_descr){
    auto op = "";

    switch(_case){
    default:
      return BadLogic;

    case GreatT:
      op = ">";
      condition = condition == (left > right);
      goto present;
      break;

    case GreatE:
      op = strlen(op) == 0? ">=": op;
      condition = condition == (left >= right);
      goto present;
      break;

    case LessE:
      op = strlen(op) == 0? "<=": op;
      condition = condition == (left <= right);
      goto present;
      break;

    case LessT:
      op = strlen(op) == 0? "<": op;
      condition = condition == (left < right);

     present:
     _status = _ignore? _status: (_status? condition: _status);
      if (!condition){
        header((_expect? "": "!(") + 
               left_descr + " " + op + " " + right_descr +
               (_expect? "": ")"));
        actual("  Actual:", rvalue(left_descr), "=", std::to_string(left));
        actual("  Actual:", rvalue(right_descr), "=", std::to_string(right));
        expect("Expected:", rvalue(left_descr), op, rvalue(right_descr));
        footer();
        return true;
      }
      break;

    case Unknown:
      _status = _ignore? _status: false;
      return DoNothing.reason("Unknown check type");
    }

    return false;
  }

  template<typename Left, typename Right>
  bool compare(bool condition, const Left& left, std::string left_descr,
                               const Right& right, std::string right_descr){
    auto op = "";

    switch(_case){
    default:
      return BadLogic;

    case GreatT:
      op = ">";
      condition = condition == (left > right);
      goto present;
      break;

    case GreatE:
      op = strlen(op) == 0? ">=": op;
      condition = condition == (left >= right); 
      goto present;
      break;

    case LessE:
      op = strlen(op) == 0? "<=": op;
      condition = condition == (left <= right);
      goto present;
      break;

    case LessT:
      op = strlen(op) == 0? "<": op;
      condition = condition == (left < right);

     present:
     _status = _ignore? _status: (_status? condition: _status);
      if (!condition){
        header((_expect? "": "!(") + 
               left_descr + " " + op + " " + right_descr +
               (_expect? "": ")"));
        actual("  Actual:", rvalue(left_descr), "=", std::to_string(left));
        actual("  Actual:", rvalue(right_descr), "=", std::to_string(right));
        expect("Expected:", rvalue(left_descr), op, rvalue(right_descr));
        footer();
        return true;
      }
      break;

    case Unknown:
      _status = _ignore? _status: false;
      return DoNothing.reason("Unknown check type");
    }
    return false;
  }
 #else
  template<typename Left, typename Right>
  bool equal(bool condition, Left left, std::string left_descr,
                             Right right, std::string right_descr){
    condition = condition == (left == right);
    _status = _ignore? _status: (_status? condition: _status);

    if (!condition){
      header(left_descr + (_expect? " == ": " != ") + right_descr);
      actual("  Actual:", std::to_string(left) + " != " + std::to_string(right));
      expect("Expected:", rvalue(left_descr), "==", rvalue(right_descr));
      footer();
      return true;
    }

    return false;
  }

  template<typename Left, typename Right>
  bool compare(bool condition, Left left, std::string left_descr,
                               Right right, std::string right_descr){
    auto op = "";

    switch(_case){
    default:
      return BadLogic;

    case GreatT:
      op = ">";
      condition = condition == (left > right);
      goto present;
      break;

    case GreatE:
      op = strlen(op) == 0? ">=": op;
      condition = condition == (left >= right);
      goto present;
      break;

    case LessE:
      op = strlen(op) == 0? "<=": op;
      condition = condition == (left <= right);
      goto present;
      break;

    case LessT:
      op = strlen(op) == 0? "<": op;
      condition = condition == (left < right);

     present:
     _status = _ignore? _status: (_status? condition: _status);
      if (!condition){
        header((_expect? "": "!(") + 
               left_descr + " " + op + " " + right_descr +
               (_expect? "": ")"));
        actual("  Actual:", rvalue(left_descr), "=", std::to_string(left));
        actual("  Actual:", rvalue(right_descr), "=", std::to_string(right));
        expect("Expected:", rvalue(left_descr), op, rvalue(right_descr));
        footer();
        return true;
      }
      break;

    case Unknown:
      _status = _ignore? _status: false;
      return DoNothing.reason("Unknown check type");
    }

    return false;
  }
 #endif
 #else
  template<typename Left, typename Right>
  bool equal(bool condition, Left& left, std::string left_descr,
                             Right& right, std::string right_descr){
    condition = condition == (left == right);
    _status = _ignore? _status: (_status? condition: _status);

    if (!condition){
      header(left_descr + (_expect? " == ": " != ") + right_descr);
      actual("  Actual:", std::to_string(left) + " != " + std::to_string(right));
      expect("Expected:", rvalue(left_descr), "==", rvalue(right_descr));
      footer();

      return true;
    }

    return false;
  }

  template<typename Left, typename Right>
  bool equal(bool condition, Left&& left, std::string left_descr,
                             Right&& right, std::string right_descr){
    condition = condition == (left == right);
    _status = _ignore? _status: (_status? condition: _status);

    if (!condition){
      header(left_descr + (_expect? " == ": " != ") + right_descr);
      actual("  Actual:", std::to_string(left) + " != " + std::to_string(right));
      expect("Expected:", rvalue(left_descr), "==", rvalue(right_descr));
      footer();
      return true;
    }

    return false;
  }

  template<typename Left, typename Right>
  bool compare(bool condition, Left&& left, std::string left_descr,
                               Right&& right, std::string right_descr){
    auto op = "";

    switch(_case){
    default:
      return BadLogic;

    case GreatT:
      op = ">";
      condition = condition == (left > right);
      goto present;
      break;

    case GreatE:
      op = strlen(op) == 0? ">=": op;
      condition = condition == (left >= right);
      goto present;
      break;

    case LessE:
      op = strlen(op) == 0? "<=": op;
      condition = condition == (left <= right);
      goto present;
      break;

    case LessT:
      op = strlen(op) == 0? "<": op;
      condition = condition == (left < right);

     present:
     _status = _ignore? _status: (_status? condition: _status);
      if (!condition){
        header((_expect? "": "!(") + 
               left_descr + " " + op + " " + right_descr +
               (_expect? "": ")"));
        actual("  Actual:", rvalue(left_descr), "=", std::to_string(left));
        actual("  Actual:", rvalue(right_descr), "=", std::to_string(right));
        expect("Expected:", rvalue(left_descr), op, rvalue(right_descr));
        footer();
        return true;
      }
      break;

    case Unknown:
      _status = _ignore? _status: false;
      return DoNothing.reason("Unknown check type");
    }

    return false;
  }

  template<typename Left, typename Right>
  bool compare(bool condition, const Left& left, std::string left_descr,
                               const Right& right, std::string right_descr){
    auto op = "";

    switch(_case){
    default:
      return BadLogic;

    case GreatT:
      op = ">";
      condition = condition == (left > right);
      goto present;
      break;

    case GreatE:
      op = strlen(op) == 0? ">=": op;
      condition = condition == (left >= right); 
      goto present;
      break;

    case LessE:
      op = strlen(op) == 0? "<=": op;
      condition = condition == (left <= right);
      goto present;
      break;

    case LessT:
      op = strlen(op) == 0? "<": op;
      condition = condition == (left < right);

     present:
     _status = _ignore? _status: (_status? condition: _status);
      if (!condition){
        header((_expect? "": "!(") + 
               left_descr + " " + op + " " + right_descr +
               (_expect? "": ")"));
        actual("  Actual:", rvalue(left_descr), "=", std::to_string(left));
        actual("  Actual:", rvalue(right_descr), "=", std::to_string(right));
        expect("Expected:", rvalue(left_descr), op, rvalue(right_descr));
        footer();
        return true;
      }
      break;

    case Unknown:
      _status = _ignore? _status: false;
      return DoNothing.reason("Unknown check type");
    }

    return false;
  }
 #endif

 private:
  void expect(std::string&& prefix, std::string&& name, std::string op,
              std::string&& value);
  void expect(std::string&& prefix, std::string&& value);
  void actual(std::string&& prefix, std::string&& name, std::string op,
              std::string&& value);
  void actual(std::string&& prefix, std::string&& value);
  void header(std::string&& description);
  void footer();

 private:
  std::string _name, _suite;

 private:
  std::vector<Summary> _summaries;

 private:
  std::string _file;
  std::size_t _line;

 private:
  bool  _ignore,_status;
  bool  _fatal, _expect;
  CaseT _case;
};

namespace testing{
void init(int& argc, const char** argv);
int  run();
} // namespace testing
} // namespace base

/* @DEFINITION: internal macros, we MUST NOT use it in real example */
#define UNITTEST_ASSERT_BOOLEAN(condition, name, expect)                   \
  {                                                                        \
    auto error = true;                                                     \
                                                                           \
    base::unittest::current->mock(__FILE__, __LINE__)                      \
        .fatal(true)                                                       \
        .unitcase(base::Test::Boolean);                                    \
    try {                                                                  \
      error = base::unittest::current->check((condition) == expect, name); \
    } catch (base::Error & error) {                                        \
      base::unittest::current->error(error);                               \
    } catch (std::exception & except) {                                    \
      base::unittest::current->error(WatchErrno.reason(except.what()));    \
    }                                                                      \
    if (error) throw false;                                                \
  }

#define UNITTEST_ASSERT_EQUAL(left, left_descr, right, right_descr, expect)    \
  {                                                                            \
    auto error = true;                                                         \
                                                                               \
    base::unittest::current->mock(__FILE__, __LINE__)                          \
        .fatal(true)                                                           \
        .require(expect)                                                       \
        .unitcase(base::Test::Equal);                                          \
    try {                                                                      \
      error = base::unittest::current->equal(expect, (left),                   \
                                             std::string(left_descr), (right), \
                                             std::string(right_descr));        \
    } catch (base::Error & error) {                                            \
      base::unittest::current->error(error);                                   \
    } catch (std::exception & except) {                                        \
      base::unittest::current->error(WatchErrno.reason(except.what()));        \
    }                                                                          \
    if (error) throw false;                                                    \
  }

#define UNITTEST_ASSERT_LT(left, left_descr, right, right_descr, expect) \
  {                                                                      \
    auto error = true;                                                   \
                                                                         \
    base::unittest::current->mock(__FILE__, __LINE__)                    \
        .fatal(true)                                                     \
        .require(expect)                                                 \
        .unitcase(base::Test::LessT);                                    \
    try {                                                                \
      error = base::unittest::current->compare(                          \
          expect, (left), std::string(left_descr), (right),              \
          std::string(right_descr));                                     \
    } catch (base::Error & error) {                                      \
      base::unittest::current->error(error);                             \
    } catch (std::exception & except) {                                  \
      base::unittest::current->error(WatchErrno.reason(except.what()));  \
    }                                                                    \
    if (error) throw false;                                              \
  }

#define UNITTEST_ASSERT_LE(left, left_descr, right, right_descr, expect) \
  {                                                                      \
    auto error = true;                                                   \
                                                                         \
    base::unittest::current->mock(__FILE__, __LINE__)                    \
        .fatal(true)                                                     \
        .require(expect)                                                 \
        .unitcase(base::Test::LessE);                                    \
    try {                                                                \
      error = base::unittest::current->compare(                          \
          expect, (left), std::string(left_descr), (right),              \
          std::string(right_descr));                                     \
    } catch (base::Error & error) {                                      \
      base::unittest::current->error(error);                             \
    } catch (std::exception & except) {                                  \
      base::unittest::current->error(WatchErrno.reason(except.what()));  \
    }                                                                    \
    if (error) throw false;                                              \
  }

#define UNITTEST_ASSERT_GT(left, left_descr, right, right_descr, expect) \
  {                                                                      \
    auto error = true;                                                   \
                                                                         \
    base::unittest::current->mock(__FILE__, __LINE__)                    \
        .fatal(true)                                                     \
        .require(expect)                                                 \
        .unitcase(base::Test::GreatT);                                   \
    try {                                                                \
      error = base::unittest::current->compare(                          \
          expect, (left), std::string(left_descr), (right),              \
          std::string(right_descr));                                     \
    } catch (base::Error & error) {                                      \
      base::unittest::current->error(error);                             \
    } catch (std::exception & except) {                                  \
      base::unittest::current->error(WatchErrno.reason(except.what()));  \
    }                                                                    \
    if (error) throw false;                                              \
  }

#define UNITTEST_ASSERT_GE(left, left_descr, right, right_descr, expect) \
  {                                                                      \
    auto error = true;                                                   \
                                                                         \
    base::unittest::current->mock(__FILE__, __LINE__)                    \
        .fatal(true)                                                     \
        .require(expect)                                                 \
        .unitcase(base::Test::GreatE);                                   \
    try {                                                                \
      error = base::unittest::current->compare(                          \
          expect, (left), std::string(left_descr), (right),              \
          std::string(right_descr));                                     \
    } catch (base::Error & error) {                                      \
      base::unittest::current->error(error);                             \
    } catch (std::exception & except) {                                  \
      base::unittest::current->error(WatchErrno.reason(except.what()));  \
    }                                                                    \
    if (error) throw false;                                              \
  }

#define UNITTEST_CHECK_BOOLEAN(condition, name, expect)                 \
  {                                                                     \
    base::unittest::current->mock(__FILE__, __LINE__)                   \
        .fatal(false)                                                   \
        .require(expect)                                                \
        .unitcase(base::Test::Boolean);                                 \
    try {                                                               \
      base::unittest::current->check((condition) == expect, name);      \
    } catch (base::Error & error) {                                     \
      base::unittest::current->error(error);                            \
    } catch (std::exception & except) {                                 \
      base::unittest::current->error(WatchErrno.reason(except.what())); \
    }                                                                   \
  }

#define UNITTEST_CHECK_EQUAL(left, left_descr, right, right_descr, expect)    \
  {                                                                           \
    base::unittest::current->mock(__FILE__, __LINE__)                         \
        .fatal(false)                                                         \
        .require(expect)                                                      \
        .unitcase(base::Test::Equal);                                         \
    try {                                                                     \
      base::unittest::current->equal(expect, (left), std::string(left_descr), \
                                     (right), std::string(right_descr));      \
    } catch (base::Error & error) {                                           \
      base::unittest::current->error(error);                                  \
    } catch (std::exception & except) {                                       \
      base::unittest::current->error(WatchErrno.reason(except.what()));       \
    }                                                                         \
  }

#define UNITTEST_CHECK_LT(left, left_descr, right, right_descr, expect)  \
  {                                                                      \
    base::unittest::current->mock(__FILE__, __LINE__)                    \
        .fatal(false)                                                    \
        .require(expect)                                                 \
        .unitcase(base::Test::LessT);                                    \
    try {                                                                \
      base::unittest::current->compare(expect, (left),                   \
                                       std::string(left_descr), (right), \
                                       std::string(right_descr));        \
    } catch (base::Error & error) {                                      \
      base::unittest::current->error(error);                             \
    } catch (std::exception & except) {                                  \
      base::unittest::current->error(WatchErrno.reason(except.what()));  \
    }                                                                    \
  }

#define UNITTEST_CHECK_LE(left, left_descr, right, right_descr, expect)  \
  {                                                                      \
    base::unittest::current->mock(__FILE__, __LINE__)                    \
        .fatal(false)                                                    \
        .require(expect)                                                 \
        .unitcase(base::Test::LessE);                                    \
    try {                                                                \
      base::unittest::current->compare(expect, (left),                   \
                                       std::string(left_descr), (right), \
                                       std::string(right_descr));        \
    } catch (base::Error & error) {                                      \
      base::unittest::current->error(error);                             \
    } catch (std::exception & except) {                                  \
      base::unittest::current->error(WatchErrno.reason(except.what()));  \
    }                                                                    \
  }

#define UNITTEST_CHECK_GT(left, left_descr, right, right_descr, expect)  \
  {                                                                      \
    base::unittest::current->mock(__FILE__, __LINE__)                    \
        .fatal(false)                                                    \
        .require(expect)                                                 \
        .unitcase(base::Test::GreatT);                                   \
    try {                                                                \
      base::unittest::current->compare(expect, (left),                   \
                                       std::string(left_descr), (right), \
                                       std::string(right_descr));        \
    } catch (base::Error & error) {                                      \
      base::unittest::current->error(error);                             \
    } catch (std::exception & except) {                                  \
      base::unittest::current->error(WatchErrno.reason(except.what()));  \
    }                                                                    \
  }

#define UNITTEST_CHECK_GE(left, left_descr, right, right_descr, expect) \
  {                                                                     \
    base::unittest::current->mock(__FILE__, __LINE__)                   \
        .fatal(false)                                                   \
        .require(expect)                                                \
        .unitcase(base::Test::GreatE);                                  \
    try {                                                               \
      !base::unittest::current->check(expect, left, left_descr, right,  \
                                      right_descr);                     \
    } catch (base::Error & error) {                                     \
      base::unittest::current->error(error);                            \
    } catch (std::exception & except) {                                 \
      base::unittest::current->error(WatchErrno.reason(except.what())); \
    }                                                                   \
  }

#define UNITTEST_THROW(test, test_descr, catcher, catcher_descr, drop)       \
  {                                                                          \
    base::unittest::current->mock(__FILE__, __LINE__)                        \
        .fatal(drop)                                                         \
        .require(true)                                                       \
        .unitcase(base::Test::Throw);                                        \
    try {                                                                    \
      test;                                                                  \
      base::unittest::current->check(false, test_descr, catcher_descr,       \
                                     "din't throw anything");                \
    } catch (catcher & error) {                                              \
      base::unittest::current->check(true, test_descr, catcher_descr, "");   \
    } catch (std::exception & except) {                                      \
      base::unittest::current->check(false, test_descr, catcher_descr,       \
                                     std::string{"throw "} + except.what()); \
    } catch (...) {                                                          \
      !base::unittest::current->check(false, test_descr, catcher_descr,      \
                                      "throw #Unknown type");                \
    }                                                                        \
  }

#define UNITTEST_NO_THROW(test, test_descr, drop)                            \
  {                                                                          \
    base::unittest::current->mock(__FILE__, __LINE__)                        \
        .fatal(drop)                                                         \
        .require(true)                                                       \
        .unitcase(base::Test::NoThrow);                                      \
    try {                                                                    \
      test;                                                                  \
      base::unittest::current->check(true, test_descr, "");                  \
    } catch (std::exception & except) {                                      \
      base::unittest::current->check(false, test_descr,                      \
                                     std::string{"throw "} + except.what()); \
    } catch (...) {                                                          \
      base::unittest::current->check(false, test_descr,                      \
                                     "throw #Unknown type");                 \
    }                                                                        \
  }

/* @DEFINITION: all macros below are the UI toolbox of my unittest */
#define TEST(Suite, Case)                                              \
  namespace test {                                                     \
  namespace Suite {                                                    \
  struct Case : base::Test {                                           \
   public:                                                             \
    Case() : Test{#Suite, #Case} { render(Init, __FILE__, __LINE__); } \
                                                                       \
   protected:                                                          \
    void define() final;                                               \
  };                                                                   \
                                                                       \
  static auto Unit##Case = new test::Suite::Case();                    \
  } /* namespace Suite */                                              \
  } /* namespace test  */                                              \
                                                                       \
  void test::Suite::Case::define()

#define TEARDOWN(Suite)                                              \
  namespace test {                                                   \
  struct TearDown##Suite : base::Teardown {                          \
   public:                                                           \
    Suite() : Teardown{#Suite} { render(Init, __FILE__, __LINE__); } \
                                                                     \
   protected:                                                        \
    void define() final;                                             \
    static auto Teardown##Suite = new test::TearDown##Suite();       \
  } /* namespace test */                                             \
  void                                                               \
  test::Suite::define()

#define RUN_ALL_TESTS() base::testing::run()

#define EXPECT_TRUE(condition) \
  UNITTEST_CHECK_BOOLEAN(condition, #condition, true)
#define ASSERT_TRUE(condition) \
  UNITTEST_ASSERT_BOOLEAN(condition, #condition, true)

#define EXPECT_FALSE(condition) \
  UNITTEST_CHECK_BOOLEAN(condition, #condition, false)
#define ASSERT_FALSE(condition) \
  UNITTEST_ASSERT_BOOLEAN(condition, #condition, false)

#define EXPECT_EQ(left, right) \
  UNITTEST_CHECK_EQUAL(left, #left, right, #right, true)
#define ASSERT_EQ(left, right) \
  UNITTEST_ASSERT_EQUAL(left, #left, right, #right, true)

#define EXPECT_NEQ(left, right) \
  UNITTEST_CHECK_EQUAL(left, #left, right, #right, false)
#define ASSERT_NEQ(left, right) \
  UNITTEST_ASSERT_EQUAL(left, #left, right, #right, false)

#define EXPECT_LT(left, right) UNITTEST_CHECK_LT(left, #left, right, #right)
#define ASSERT_LT(left, right) UNITTEST_ASSERT_LT(left, #left, right, #right)

#define EXPECT_LE(left, right) UNITTEST_CHECK_LE(left, #left, right, #right)
#define ASSERT_LE(left, right) UNITTEST_ASSERT_LE(left, #left, right, #right)

#define EXPECT_GT(left, right) UNITTEST_CHECK_GT(left, #left, right, #right)
#define ASSERT_GT(left, right) UNITTEST_ASSERT_GT(left, #left, right, #right)

#define EXPECT_GE(left, right) UNITTEST_CHECK_GE(left, #left, right, #right)
#define ASSERT_GE(left, right) UNITTEST_ASSERT_GE(left, #left, right, #right)

#define EXPECT_THROW(test, catcher) \
  UNITTEST_THROW(test, #test, catcher, #catcher, false)

#define ASSERT_THROW(test, catcher) \
  UNITTEST_THROW(test, #test, catcher, #catcher, true)

#define EXPECT_NO_THROW(test) UNITTEST_NO_THROW(test, #test, false)
#define ASSERT_NO_THROW(test) UNITTEST_NO_THROW(test, #test, true)

/* @NOTE: definition of autochecking fail cases */
#endif  // LIBBASE_UNITTEST_HPP_