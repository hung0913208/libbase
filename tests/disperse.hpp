#if !defined(TESTCASE_DISPERSE_HPP_)
#define TESTCASE_DISPERSE_HPP_
#include "../all.hpp"

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(Rules);
#if CHECK_CASE_RULES_BASIC || !CHECK_CASE
BOOST_AUTO_TEST_CASE(DisperseBasicRules1) {
  base::Rules<int> rules;

  /* @NOTE: them cac su kien cho node cac node,
   * cac su kien se theo thu tu tu tren xuong
   */
  rules.append(0, std::make_tuple(0, 0));
  rules.append(0, std::make_tuple(1, 0));
  rules.append(0, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(0, 0));
  rules.append(1, std::make_tuple(1, 0));

  /* @NOTE: khoi tao trang thai khoi dau cho cac node */
  BOOST_REQUIRE(rules.init(0, 2) == true); /* xoa buoc so 2 cua node 0 */
  BOOST_REQUIRE(rules.init(1, 1) == true); /* xoa buoc 1 cua node 1 */

  /* @NOTE: xoa mot step cua mot node */

  BOOST_REQUIRE(rules.remove(1, 2) == false);
  BOOST_REQUIRE(rules.remove(1, 1) == true);

  /* @NOTE: thao tac voi node */
  BOOST_CHECK_THROW({ rules.select(0); }, base::Error);

  rules.up(rules.select(1));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 2);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(2));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 1);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(1));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 0);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(0));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 0);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 1);
  BOOST_CHECK_THROW({ rules.wait(rules.select(2)); }, base::Error);
  rules.seek(rules.select(0), 0, SEEK_SET);
  rules.wait(rules.select(1));
  BOOST_REQUIRE(rules.isolate(1, true) == true);
  BOOST_REQUIRE(rules.isolate(3, true) == false);
  BOOST_REQUIRE(rules.isolate(rules.nodes()[0].context(), true) == true);
}

BOOST_AUTO_TEST_CASE(DisperseBasicRules2) {
  base::Rules<int> rules;

  /* @NOTE: them cac su kien cho node cac node,
   * cac su kien se theo thu tu tu tren xuong
   */
  rules.append(0, std::make_tuple(0, 0));
  rules.append(0, std::make_tuple(1, 0));
  rules.append(0, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(0, 0));
  rules.append(1, std::make_tuple(1, 0));

  /* @NOTE: khoi tao trang thai khoi dau cho cac node */
  BOOST_REQUIRE(rules.init(0, 2) == true); /* xoa buoc so 2 cua node 0 */
  BOOST_REQUIRE(rules.init(1, 1) == true); /* xoa buoc 1 cua node 1 */

  /* @NOTE: xoa mot step cua mot node */

  BOOST_REQUIRE(rules.remove(1, 2) == false);
  BOOST_REQUIRE(rules.remove(1, 1) == true);

  /* @NOTE: thao tac voi node */
  BOOST_CHECK_THROW({ rules.select(0); }, base::Error);

  rules.up(rules.select(1));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 2);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(2));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 1);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(1));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 0);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 0);
  BOOST_REQUIRE(rules.select(0) == 1);
  rules.down(rules.select(0));
  BOOST_REQUIRE(rules.status(rules.nodes()[0].context()) == 2);
  BOOST_REQUIRE(rules.status(rules.nodes()[1].context()) == 0);
  rules.wait(rules.select(2));
  rules.seek(rules.select(0), 0, SEEK_SET);
  BOOST_CHECK_THROW({ rules.wait(rules.select(1)); }, base::Error);
}
#endif // CHECK_CASE_RULES_BASIC

#if CHECK_CASE_ISOLATED_NODE_BASIC
#define MAX_ISOLATED_NODE 1000

using namespace base::isolation;
BOOST_AUTO_TEST_CASE(DisperseIsolatedNode) {
  std::vector<Node> nodes;

  for (auto i = 0; i < MAX_ISOLATED_NODE; ++i)
    nodes.push_back(
        Node{Node::Accepter | Node::Learner | Node::Proposer, nullptr});
}
#endif // CHECK_CASE_ISOLATED_NODE_BASIC
BOOST_AUTO_TEST_SUITE_END();
#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#if CHECK_CASE_RULES_BASIC || !CHECK_CASE
TEST(Disperse, BasicRules1) {
  base::Rules<int> rules;

  /* @NOTE: them cac su kien cho node cac node,
   * cac su kien se theo thu tu tu tren xuong
   */
  rules.append(0, std::make_tuple(0, 0));
  rules.append(0, std::make_tuple(1, 0));
  rules.append(0, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(0, 0));
  rules.append(1, std::make_tuple(1, 0));

  /* @NOTE: khoi tao trang thai khoi dau cho cac node */
  EXPECT_TRUE(rules.init(0, 2) == true); /* xoa buoc so 2 cua node 0 */
  EXPECT_TRUE(rules.init(1, 1) == true); /* xoa buoc 1 cua node 1 */

  /* @NOTE: xoa mot step cua mot node */

  EXPECT_TRUE(rules.remove(1, 2) == false);
  EXPECT_TRUE(rules.remove(1, 1) == true);

  /* @NOTE: thao tac voi node */
  EXPECT_THROW({ rules.select(0); }, base::Error);

  rules.up(rules.select(1));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 2);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(2));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 1);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(1));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 0);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(0));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 0);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 1);
  EXPECT_THROW({ rules.wait(rules.select(2)); }, base::Error);
  rules.seek(rules.select(0), 0, SEEK_SET);
}

TEST(Disperse, BasicRules2) {
  base::Rules<int> rules;

  /* @NOTE: them cac su kien cho node cac node,
   * cac su kien se theo thu tu tu tren xuong
   */
  rules.append(0, std::make_tuple(0, 0));
  rules.append(0, std::make_tuple(1, 0));
  rules.append(0, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(2, 0));
  rules.append(1, std::make_tuple(0, 0));
  rules.append(1, std::make_tuple(1, 0));

  /* @NOTE: khoi tao trang thai khoi dau cho cac node */
  EXPECT_TRUE(rules.init(0, 2) == true); /* xoa buoc so 2 cua node 0 */
  EXPECT_TRUE(rules.init(1, 1) == true); /* xoa buoc 1 cua node 1 */

  /* @NOTE: xoa mot step cua mot node */

  EXPECT_TRUE(rules.remove(1, 2) == false);
  EXPECT_TRUE(rules.remove(1, 1) == true);

  /* @NOTE: thao tac voi node */
  EXPECT_THROW({ rules.select(0); }, base::Error);

  rules.up(rules.select(1));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 2);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(2));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 1);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 0);
  rules.down(rules.select(1));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 0);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 0);
  EXPECT_TRUE(rules.select(0) == 1);
  rules.down(rules.select(0));
  EXPECT_TRUE(rules.status(rules.nodes()[0].context()) == 2);
  EXPECT_TRUE(rules.status(rules.nodes()[1].context()) == 0);
  rules.wait(rules.select(2));
  rules.seek(rules.select(0), 0, SEEK_SET);
  // rules.wait(rules.select(1));
}
#endif // CHECK_CASE_RULES_BASIC

#if CHECK_CASE_ISOLATED_NODE_BASIC
#define MAX_ISOLATED_NODE 1000

using namespace base::isolation;
TEST(Disperse, IsolatedNode) {
  std::vector<Node> nodes;

  for (auto i = 0; i < MAX_ISOLATED_NODE; ++i)
    nodes.push_back(
        Node{Node::Accepter | Node::Learner | Node::Proposer, nullptr});
}
#endif // CHECK_CASE_ISOLATED_NODE_BASIC
#endif // BOOST_TEST_MAIN
#endif // TESTCASE_DISPERSE_HPP_
