#if TRACE == 1 && !defined(LIBTEST_WAVF_HPP_) && __cplusplus
#define LIBTEST_WAVF_HPP_

#include "../all.hpp"

#include <functional>
#include <fstream>
#include <string>
#include <ios>

#include <cmath>

extern "C" struct Header {
  uint8_t  chunkId[4];      // RIFF string
  uint32_t chunkSize;       // overall size of file in bytes
  uint8_t  format[4];       // WavFE string
  uint8_t  schunk1Id[4];    // fmt string with trailing null char
  uint32_t schunk1Size;     // length of the format data
  uint16_t audioFormat;     // format type. 1-PCM, 3- IEEE float, 6 - 8bit A law, 7 - 8bit mu law
  uint16_t numChannels;     // no.of channels
  uint32_t sampleRate;      // sampling rate (blocks per second)
  uint32_t byteRate;        // SampleRate * NumChannels * BitsPerSample/8
  uint16_t blockAlign;      // NumChannels * BitsPerSample/8
  uint16_t bitsPerSample;   // bits per sample, 8- 8bits, 16- 16 bits etc
  uint8_t  schunk2ID[4];    // DATA string or FLLR string
  uint32_t schunk2Size;     // NumSamples * NumChannels * BitsPerSample/8 - size of the next chunk that will be read
};

namespace test {
template <typename OutputT = float>
struct WavF : protected base::Stream {
 public:
  explicit WavF(std::string&& path, std::ios_base::openmode mode = std::ios_base::in | std::ios_base::out): Stream{
    [&](uint8_t* buffer, std::size_t size) -> base::Error{ /* reading cb */
      if (_fstream.is_open()){
        if (!_loaded){
          _fstream.read(reinterpret_cast<char*>(buffer), size);
        } else if (size != sizeof(OutputT) && (size % sizeof(OutputT)))
          return NoSupport;
        else {
          _fstream.read(reinterpret_cast<char*>(buffer), size);

          for (auto i = 0; i < cast_(i, size / sizeof(OutputT)); ++i){
            OutputT reverse{0};

            if (typeid(OutputT) == typeid(float) || typeid(OutputT) == typeid(double))
              reverse = base::reverse(*(reinterpret_cast<int*>(buffer) + i)) / std::pow(2.0, 32);
            else
              reverse = base::reverse(*(reinterpret_cast<OutputT*>(buffer) + i));

            *(reinterpret_cast<OutputT*>(buffer + i * sizeof(OutputT))) = reverse;
          }
        }
        return NoError;
      } else return NotFound;
    },
    [&](uint8_t* buffer, std::size_t size) -> base::Error{ /* writing cb */
      if (_fstream.is_open()){
        if (!_loaded){
          _fstream.write(reinterpret_cast<char*>(buffer), size);
        } else if (size != sizeof(OutputT))
          return NoSupport;
        else {
          for (auto i = 0; i < cast_(i, size / sizeof(OutputT)); ++i){
            auto reverse = base::reverse(*(reinterpret_cast<OutputT*>(buffer) + i));

            if (typeid(OutputT) == typeid(float) || typeid(OutputT) == typeid(double)){
              reverse = *reinterpret_cast<OutputT*>(base::reverse(*(reinterpret_cast<int*>(buffer) + i)));
            } else
              *(reinterpret_cast<OutputT*>(buffer + i * sizeof(OutputT))) = reverse;
          }

          _fstream.write(reinterpret_cast<char*>(buffer), size);
        }
        return NoError;
      } else return DoNothing;
    }}, _offset{std::string::npos}, _boffset{std::string::npos}, _pbody{std::string::npos},
  _fstream{path, mode}, _header{}, _loaded{false} {

    if (sizeof(OutputT) > 4 && typeid(OutputT) != typeid(double)) throw NoSupport;
    else{
      auto error = config();

      if (error) throw error;
    }
  }

 public:
  base::Error open(std::string&& path, std::ios_base::openmode mode = std::ios_base::in | std::ios_base::out){
    _fstream.open(path, mode);
    return _fstream.is_open() ? config() : DoNothing;
  }

  void close(){ _fstream.close(); }

 public:
  base::Error seek(long offset, int whence){
    if (!_fstream.is_open()) return DoNothing;
    else {
      auto szfile = size();

      switch (whence){
      case SEEK_SET:
        BAssert(offset * cast_(offset, sizeof(OutputT)));

        _boffset = _pbody + offset * cast_(offset, sizeof(OutputT));
        _offset  = offset;
        break;

      case SEEK_CUR:
        BAssert(offset * cast_(offset, sizeof(OutputT)) < szfile - _boffset);

        _boffset += offset * cast_(offset, sizeof(OutputT));
        _offset  += offset;
        break;

      case SEEK_END:
        BAssert(offset < 0);

        _boffset += size() + offset * cast_(offset, sizeof(OutputT));
        _offset  += offset;
        break;
      }

      _fstream.seekg(_boffset, std::ios::beg);
      return NoError;
    }
  }

  std::size_t tell(){ return _offset; }

  Header& header(){ return _header; }

 public:
  WavF<OutputT>& operator>>(OutputT& value){
    auto error = read(reinterpret_cast<uint8_t*>(&value), sizeof(value));

    if (error) throw error;
    return *this;
  }

  WavF<OutputT>& operator<<(OutputT&& value){
    auto error =  write(reinterpret_cast<uint8_t*>(&value), sizeof(value));

    if (error) throw error;
    return *this;
  }

 private:
  std::size_t size(){
    std::size_t save = _fstream.tellg(), result = 0;

    _fstream.seekg(0, std::ios::end);
    result = _fstream.tellg();
    _fstream.seekg(save, std::ios::beg);

    return result;
  }

  base::Error config(){
    REGISTER_RETURNING_BUFFER_WITH_INIT(base::Error) = NoError;
    BUILD_FTRACKING([&]() -> base::Error{
      if (!_loaded){
        auto error = read(reinterpret_cast<uint8_t*>(&_header), sizeof(_header));

        if (error) return error;
        else {
         #if BYTE_ORDER!=BIG_ENDIAN
          CHECK_PASSING_LINE(_header.chunkSize     = base::reverse(_header.chunkSize));
          CHECK_PASSING_LINE(_header.schunk1Size   = base::reverse(_header.schunk1Size));
          CHECK_PASSING_LINE(_header.audioFormat   = base::reverse(_header.audioFormat));
          CHECK_PASSING_LINE(_header.numChannels   = base::reverse(_header.numChannels));
          CHECK_PASSING_LINE(_header.sampleRate    = base::reverse(_header.sampleRate));
          CHECK_PASSING_LINE(_header.byteRate      = base::reverse(_header.byteRate));
          CHECK_PASSING_LINE(_header.blockAlign    = base::reverse(_header.blockAlign));
          CHECK_PASSING_LINE(_header.bitsPerSample = base::reverse(_header.bitsPerSample));
          CHECK_PASSING_LINE(_header.schunk2Size   = base::reverse(_header.schunk2Size));
         #endif
          TAKE_FSNAPSHOT("header", std::function<std::string()>{[&]() -> std::string {
            std::string result{};

            result.append("{\n\tHeader::chunkID     = [").append(std::string{char(_header.chunkId[0])}).append(", ")
                  .append(std::string{char(_header.chunkId[1])}).append(", ")
                  .append(std::string{char(_header.chunkId[2])}).append(", ")
                  .append(std::string{char(_header.chunkId[3])}).append("];");
            result.append("\n\tHeader::chunkSize   = ").append(base::i2hex(_header.chunkSize)).append(";");
            result.append("\n\tHeader::format      = [").append(std::string{char(_header.format[0])}).append(", ")
                  .append(std::string{char(_header.format[1])}).append(", ")
                  .append(std::string{char(_header.format[2])}).append(", ")
                  .append(std::string{char(_header.format[3])}).append("];");
            result.append("\n\tHeader::schunk1Id   = [").append(std::string{char(_header.schunk1Id[0])}).append(", ")
                  .append(std::string{char(_header.schunk1Id[1])}).append(", ")
                  .append(std::string{char(_header.schunk1Id[2])}).append(", ")
                  .append(std::string{char(_header.schunk1Id[3])}).append("];");
            result.append("\n\tHeader::schunk1Size = ").append(base::i2hex(_header.schunk1Size)).append(";");
            result.append("\n\tHeader::audioFormat = ").append(base::i2hex(_header.audioFormat)).append(";");
            result.append("\n\tHeader::numChannels = ").append(base::i2hex(_header.numChannels)).append(";");
            result.append("\n\tHeader::sampleRate  = ").append(base::i2hex(_header.sampleRate)).append(";");
            result.append("\n\tHeader::byteRate    = ").append(base::i2hex(_header.byteRate)).append(";");
            result.append("\n\tHeader::blockAlign  = ").append(base::i2hex(_header.blockAlign)).append(";");
            result.append("\n\tHeader::schunk2ID   = [").append(std::string{char(_header.schunk2ID[0])}).append(", ")
                  .append(std::string{char(_header.schunk2ID[1])}).append(", ")
                  .append(std::string{char(_header.schunk2ID[2])}).append(", ")
                  .append(std::string{char(_header.schunk2ID[3])}).append("];");
            result.append("\n\tHeader::schunk2Size = ").append(base::i2hex(_header.schunk2Size)).append("\n}");
            return result;
          }});
          _pbody   = (_boffset = _fstream.tellg());
          _loaded  = true;
          _offset  = 0;
        }
      }
      return NoError;
    });
    return RETURN_FROM_TRACKING;
  }

 private:
  std::size_t  _offset, _boffset, _pbody;
  std::fstream _fstream;

 private:
  Header _header;
  bool   _loaded;
};
} // test
#endif // LIBTEST_WAVF_HPP_