#if !defined(LIBBASE_TABLE_TEST_HPP_) && __cplusplus
#define LIBBASE_TABLE_TEST_HPP_
#include "all.hpp"

#define MAX_TESTCASE_SIMTABLE 4

namespace base {
namespace test {

template <typename Key, typename Value>
std::size_t simtable(std::function<Key()> gen, std::vector<Value> values,
                     std::size_t max, std::size_t loop) {
 #if TRACE == 1
  REGISTER_RETURNING_BUFFER_WITH_INIT(std::size_t) = 0;
  auto body = [&]() -> std::size_t {
 #else
 BEGIN_RTRACKING(std::size_t)
    auto result = 0;
 #endif
    base::Table<Key, Value> table{
        max, [](Key &&key) -> int { return std::hash<Key>{}(key); }};

   #if TRACE==1
    CHECK_PASSING_LINE(++RETURNING_BUFFER);
   #else

    CHECK({ ++result; });
   #endif

    for (auto i = 0; i < cast_(i, loop); ++i) {
     #if TRACE==1
      CHECK_PASSING_LINE(std::cout << i << ' ');
     #endif
#if !BOOST_TEST_MAIN
     #if TRACE==1
      TAKE_FSNAPSHOT(
          "table.value", std::function<std::string()>{[&]() -> std::string {
            std::string result{};

            for (auto i = 0; i < cast_(i, max); ++i)
              result.append(std::to_string(table.value(i))).append(", ");

            return result;
          }});

      TAKE_FSNAPSHOT("table.key",
                     std::function<std::string()>{[&]() -> std::string {
                       std::string result{"{ "};

                       for (auto i = 0; i < cast_(i, max); ++i) {
                         result.append(std::to_string(table.key(i)));
                         if (i < cast_(i, max - 1))
                           result.append(", ");
                       }
                       return result.append(" }");
                     }});
     #endif
#if DEBUG
     #if TRACE==1
      TAKE_FSNAPSHOT("table.index",
                     std::function<std::string()>{[&]() -> std::string {
                       std::string result{"{ "};

                       for (auto i = 0; i < cast_(i, max); ++i) {
                         result.append(std::to_string(table.index_(i)));
                         if (i < cast_(i, max - 1))
                           result.append(", ");
                       }
                       return result.append(" }");
                     }});
     #endif
#endif
#endif

      switch (i % 4) {
      default:
        table.put(gen(), rvalue(values[rand() % values.size()]));
        break;

      case 1: {
        auto key = gen();

        table.put(key, rvalue(values[rand() % values.size()]));
        break;
      }

      case 2: {
        auto key = gen();

        table.put(key, values[rand() % values.size()]);
        break;
      }

      case 3: {
        auto key = gen();
        auto value = values[rand() % values.size()];

        table.put(key, value);
        break;
      }
      }
    }

   #if TRACE==1
    CHECK_PASSING_LINE(++RETURNING_BUFFER);
   #else
    CHECK({ ++result; });
   #endif

#if !NDEBUG
    for (auto i = 0; i < cast_(i, loop); ++i) {
      try {
        table.get(rvalue(gen()));
      } catch (base::Error&) {
      }
      try {
        auto key = gen();

        table.get(key);
      } catch (base::Error&) {
      }

      try {
        table.index(rvalue(gen()));
      } catch (base::Error&) {
      }
      try {
        auto key = gen();

        table.index(key);
      } catch (base::Error&) {
      }

      if (i < cast_(i, table.size())) {
        try {
          table.get(table.key(i));
        } catch (base::Error&) {
        }

        try {
          table.index(table.key(i));
        } catch (base::Error&) {
        }
        try {
          auto key = table.key(i);

          table.get(key);
        } catch (base::Error&) {
        }
        try {
          auto key = table.key(i);

          table.index(key);
        } catch (base::Error&) {
        }
      }
    }
#endif

#if NDEBUG
    auto none = Value{};
    auto begin = clock();

    begin = clock();
    for (auto i = 0; i < 4 * cast_(i, loop); ++i)
      table.get(gen(), none);
    auto end = clock();
    std::cout << "Perform (no exception, solution 1) " << 4 * loop
              << " in: " << (end - begin) / 1000.0 << " (ms)" << std::endl;
#endif

#if NDEBUG
    begin = clock();

    table.none() = &none;
    for (auto i = 0; i < 4 * cast_(i, loop); ++i)
      table.get(gen());
    end = clock();
    std::cout << "Perform (no exception, solution 2) " << 4 * loop
              << " in: " << (end - begin) / 1000.0 << " (ms)" << std::endl;
#endif
  #if TRACE==1
    CHECK_PASSING_LINE(++RETURNING_BUFFER);
  #else
    CHECK({ ++result; });
  #endif
    for (auto i = 0; i < cast_(i, loop); ++i) {
      try {
        table.clr(gen());
      } catch (base::Error&) {
      };
      try {
        auto value = values[rand() % values.size()];

        if (i < cast_(i, table.size()))
          table.clr(table.key(i), rvalue(value));

        table.clr(gen(), rvalue(value));
      } catch (base::Error&) {
      };
      try {
        auto value = values[rand() % values.size()];

        if (i < cast_(i, table.size())) {
          auto key = table.key(i);

          table.clr(key, rvalue(value));
        }

        table.clr(gen(), values[rand() % values.size()]);
      } catch (base::Error&) {
      };
    }

    for (auto i = 0; i < cast_(i, max); ++i) {
      try {
        if (table.index(table.key(i)) >= 0)
          PAssert(table.get(table.key(i)) == table.value(i));
      } catch (base::Error &) {
      }
    }

  #if TRACE==1
    CHECK_PASSING_LINE(++RETURNING_BUFFER);
    return RETURNING_BUFFER;
   #else
    CHECK({ ++result; });
    return result;
   #endif
 #if TRACE==1
  };
  BUILD_FTRACKING(body);
  return RETURN_FROM_TRACKING;
 #else
  END_FTRACKING;
 #endif
}
} // namespace test
} // namespace base

#endif // LIBBASE_TABLE_TEST_HPP_
