#ifndef TESTCASE_TIE_HPP_
#define TESTCASE_TIE_HPP_
#include "../all.hpp"

namespace test{
namespace utils{  
std::vector<std::string> split(const char* raw, const char sep){
  auto result = std::vector<std::string>{};
  auto size   = strlen(raw);
  auto begin  = 0;

  for (auto i = 0; i < cast_(i, size); ++i){
    if (raw[i] == sep){
      result.push_back(std::string{raw + begin, raw + i});
      begin = i + 1;
    }
  }

  result.push_back(std::string{raw + begin, raw + size});
  return result;
}

template<typename ...Args>
base::Tie<std::string> tie(Args&... args){
  using Convert = std::function<base::Error(std::string&, base::Auto&)>;
  using Type    = std::reference_wrapper<const std::type_info>;

  std::vector<std::pair<Type, Convert>> converts{};
  converts.push_back(std::make_pair(
    std::cref(typeid(int)),
    Convert{[](std::string& input, base::Auto& output) -> base::Error{
        return output.move(stoi(input));
      }
    })
  );
  converts.push_back(std::make_pair(
    std::cref(typeid(std::string)),
    Convert{[](std::string& input, base::Auto& output) -> base::Error{
        return output.move(std::string{input});
      }
    })
  );
  converts.push_back(std::make_pair(
    std::cref(typeid(char*)),
    Convert{[](std::string& input, base::Auto& output) -> base::Error{
        return output.move(const_cast<char*>(input.c_str()));
      }
    })
  );

  return base::Tie<std::string>::tie(rvalue(converts), args...);
}
} // namespace utils
} // namespace test

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(Tie);
#if CHECK_CASE_LIBBASE_TIE || !CHECK_CASE
BOOST_AUTO_TEST_CASE(Basic) {
  auto name  = std::string{};
  auto index = 0;

  BOOST_REQUIRE_NO_THROW(test::utils::tie(name, index) = test::utils::split("abcd:10", ':'));
  BOOST_REQUIRE(name == "abcd");
  BOOST_REQUIRE(index == 10);
}
#endif
BOOST_AUTO_TEST_SUITE_END();
#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#if CHECK_CASE_LIBBASE_TIE || !CHECK_CASE
TEST(Tie, Basic) {
  auto name  = std::string{};
  auto index = 0;

  EXPECT_NO_THROW(test::utils::tie(name, index) = test::utils::split("abcd:10", ':'));
  EXPECT_EQ(name, "abcd");
  EXPECT_EQ(index, 10);
}
#endif
#endif
#endif  // TESTCASE_TIE_HPP_