#if !defined(TESTCASE_UNITTEST_HPP_) && __cplusplus
#define TESTCASE_UNITTEST_HPP_

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif
#include "../all.hpp"
#include "../unittest.hpp"
#include <boost/test/unit_test.hpp>

/**
* Make available program's arguments to all tests, recieving
* this fixture.
*/
struct ArgsFixture {
 public:
  ArgsFixture(): argc(boost::unit_test::framework::master_test_suite().argc),
                 argv(boost::unit_test::framework::master_test_suite().argv){}
 public:
  int argc;
  char **argv;
};

TEST(Example, Base){
  EXPECT_TRUE(1 == 1);
}

// BOOST_AUTO_TEST_SUITE(UnitTest);
#if CHECK_CASE_LIBBASE_UNITTEST || !CHECK_CASE
BOOST_FIXTURE_TEST_CASE(UnitTest, ArgsFixture) {
  base::testing::init(argc, (const char**)(argv));

  BOOST_REQUIRE(RUN_ALL_TESTS() == 0);
}
#endif
// BOOST_AUTO_TEST_SUITE_END();
#endif
#endif  // TESTCASE_UNITTEST_HPP_