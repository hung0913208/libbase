#ifndef TESTCASE_TRACE_HPP_
#include "../trace.hpp"

void procedure(){
  BEGIN_PTRACKING()
    std::cout << "hello world";
    SNAPSHOT_WITH(std::to_string, 12);
  END_PTRACKING;
}

std::string result(){
  BEGIN_RTRACKING(std::string)
    SNAPSHOT_WITH(std::to_string, 12);
    return std::string{"hello world"};
  END_FTRACKING;
}

struct Victim{
  std::string sample = "hello world";

  std::string lresult(){
    BEGIN_LTRACKING(std::string)
      SNAPSHOT(sample);
      return sample;
    END_FTRACKING;
  }

  std::string rresult(){
    BEGIN_RTRACKING(std::string)
      SNAPSHOT(sample);
      return sample;
    END_FTRACKING;
  }
};

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(Pascal);
#if CHECK_CASE_LIBBASE_PATRACE || !CHECK_CASE
BOOST_AUTO_TEST_CASE(Basic) {
  Victim victim{};

  procedure();
  BOOST_REQUIRE(result() == "hello world");
  BOOST_REQUIRE(victim.rresult() == "hello world");
  BOOST_REQUIRE(victim.lresult() == "hello world");
}
#endif
BOOST_AUTO_TEST_SUITE_END();
#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#if CHECK_CASE_LIBBASE_PATRACE || !CHECK_CASE
TEST(Pascal, Basic) {
  Victim victim{};

  procedure();
  EXPECT_TRUE(result() == "hello world");
  EXPECT_TRUE(victim.rresult() == "hello world");
  EXPECT_TRUE(victim.lresult() == "hello world");
}
#endif
#endif
#endif  // TESTCASE_TRACE_HPP_
