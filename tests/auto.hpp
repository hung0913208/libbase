#ifndef TESTCASE_AUTO_HPP_
#define TESTCASE_AUTO_HPP_
#include "../all.hpp"

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_REQUIRE_DYN_LINK
#endif

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(Auto);
#if CHECK_CASE_LIBBASE_AUTO || !CHECK_CASE
BOOST_AUTO_TEST_CASE(Basic) {
  base::Auto a, b;
  bool c = false;

  a = 10;
  b = "abc"; // be carefull, this type is 'const char*' not 'char*' or 'string'

  BOOST_REQUIRE((a == 10) == true);

  /* @NOTE: we can't compare std::string easy with this method */
  BOOST_REQUIRE((b != "cde") != true);

  BOOST_REQUIRE(b.nametype() == "char const*");
  /* @NOTE: at least, it must be something like bellow */
  BOOST_CHECK_THROW(c = !strcmp(b.get<char*>(), "cde"), base::Error);       // fail
  BOOST_REQUIRE(c == false);
  BOOST_REQUIRE_NO_THROW(
    BOOST_REQUIRE(!strcmp(b.get<const char*>(), "cde") == false)); // done
  BOOST_REQUIRE_NO_THROW(BOOST_REQUIRE(!strcmp(b.get<const char*>(), "abc")));  // done

  BOOST_CHECK_THROW(c = (a == b), base::Error);
  BOOST_CHECK_THROW(a.get<char*>(), base::Error);
  BOOST_REQUIRE((a.type().get() == b.type().get()) == false);

  a = "abc";
  BOOST_REQUIRE_NO_THROW(a.get<const char*>());
  BOOST_REQUIRE(a.type().get() == b.type().get());

  a = 100;
  b = 20;

  BOOST_REQUIRE_NO_THROW(b.get<int>());
  BOOST_REQUIRE((a == b.get<int>()) == false);
}
#endif

#if CHECK_CASE_LIBBASE_AUTO_ADVANCE || !CHECK_CASE 
BOOST_AUTO_TEST_CASE(Advance) {
  base::Auto a;
  a = 10;
  base::Auto b = a;
  base::Auto c;

  int   tmp0;
  char* tmp1;

  BOOST_REQUIRE_NO_THROW(c = std::string("cacd"));

  /* @NOTE: we can't compare std::string easy with this method */
  BOOST_REQUIRE_NO_THROW(BOOST_REQUIRE((c == "cacd") == false));

  /* @NOTE: at least, it must be something like bellow */
  BOOST_REQUIRE_NO_THROW(BOOST_REQUIRE(c.get<std::string>() == "cacd")); 

  BOOST_REQUIRE(a == 10);

  BOOST_REQUIRE_NO_THROW(b.get<int>());
  BOOST_REQUIRE(a == b.get<int>());

  BOOST_REQUIRE((a.push(tmp1).code == base::error::ENoError) == false);
  BOOST_REQUIRE(a.push(tmp0).code  == base::error::ENoError);
  BOOST_REQUIRE((a.push(tmp0).code == base::error::ENoError) == false);

  BOOST_REQUIRE_NO_THROW(a.set(100));
  BOOST_REQUIRE_NO_THROW(a.set(tmp0));
}
#endif
BOOST_AUTO_TEST_SUITE_END();

#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#if CHECK_CASE_LIBBASE_AUTO_BASIC || !CHECK_CASE
TEST(Auto, Basic) {
  base::Auto a, b;
  bool c = false;

  a = 10;
  b = "abc"; // be carefull, this type is 'const char*' not 'char*' or 'string'

  EXPECT_TRUE(a == 10);

  /* @NOTE: we can't compare std::string easy with this method */
  EXPECT_FALSE(b != "cde");

  EXPECT_TRUE(b.nametype() == "char const*");
  /* @NOTE: at least, it must be something like bellow */
  EXPECT_THROW(c = !strcmp(b.get<char*>(), "cde"), base::Error);       // fail
  EXPECT_NO_THROW(EXPECT_FALSE(!strcmp(b.get<const char*>(), "cde"))); // done
  EXPECT_NO_THROW(EXPECT_TRUE(!strcmp(b.get<const char*>(), "abc")));  // done

  EXPECT_THROW(c = (a == b), base::Error);
  EXPECT_FALSE(c);
  EXPECT_THROW(a.get<char*>(), base::Error);
  EXPECT_FALSE(a.type().get() == b.type().get());

  a = "abc";
  EXPECT_NO_THROW(a.get<const char*>());
  EXPECT_TRUE(a.type().get() == b.type().get());

  a = 100;
  b = 20;

  ASSERT_NO_THROW(b.get<int>());
  EXPECT_FALSE(a == b.get<int>());
}
#endif

#if CHECK_CASE_LIBBASE_AUTO_ADVANCE || !CHECK_CASE 
TEST(Auto, Advance) {
  base::Auto a;
  a = 10;
  base::Auto b = a;
  base::Auto c;

  int   tmp0;
  char* tmp1;

  EXPECT_NO_THROW(c = std::string("cacd"));

  /* @NOTE: we can't compare std::string easy with this method */
  EXPECT_NO_THROW(EXPECT_FALSE(c == "cacd"));

  /* @NOTE: at least, it must be something like bellow */
  EXPECT_NO_THROW(EXPECT_TRUE(c.get<std::string>() == "cacd")); 

  EXPECT_TRUE(a == 10);

  EXPECT_NO_THROW(b.get<int>());
  EXPECT_TRUE(a == b.get<int>());

  EXPECT_FALSE(a.push(tmp1).code == base::error::ENoError);
  EXPECT_TRUE(a.push(tmp0).code  == base::error::ENoError);
  EXPECT_FALSE(a.push(tmp0).code == base::error::ENoError);

  EXPECT_NO_THROW(a.set(100));
  EXPECT_NO_THROW(a.set(tmp0));
}
#endif
#endif
#endif  // TESTCASE_AUTO_HPP_