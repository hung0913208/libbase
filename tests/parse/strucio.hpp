#ifndef TESTCASE_PARSE_STRUCIO_GRAMMAR_HPP_
#define TESTCASE_PARSE_STRUCIO_GRAMMAR_HPP_
#include "../templates/strucio.hpp"

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif

#include <boost/test/unit_test.hpp>
using namespace base::grammar::pattern::strucio;

BOOST_AUTO_TEST_SUITE(ParseStrucIO);
#if CHECK_CASE_PULL_SIMPLE || !defined(CHECK_CASE)
BOOST_AUTO_TEST_CASE(PullBasic) {
  uint8_t a;
  base::Deserialize<> pattern(Lexical<>::make_shared(byte_[a] >> word_));

  BOOST_REQUIRE((pattern << "123") == base::grammar::Don);
  BOOST_REQUIRE(a == '1');
}
#endif // CHECK_CASE_PULL_SIMPLE
BOOST_AUTO_TEST_SUITE_END();
#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

using namespace base::grammar::pattern::strucio;
#if CHECK_CASE_PULL_SIMPLE || !defined(CHECK_CASE)
TEST(StrucIO, PullBasic) {
  uint8_t a;
  uint16_t b;
  base::Deserialize<> pattern(Lexical<>::make_shared(byte_[a] >> word_[b]));

  pattern << "123";
  std::cout << a << "" << b << std::endl;
}
#endif // CHECK_CASE_PULL_SIMPLE

#if CHECK_CASE_PULL_REPEATE
TEST(StrucIO, PullRepeat) {
  uint8_t a;
  uint16_t b;
  base::Deserialize<> pattern(Lexical<>::make_shared(byte_[a] >> word_[b]));

  pattern << "123";
  std::cout << a << "" << b << std::endl;
}
#endif
#endif // BOOST_TEST_MAIN
#endif // TESTCASE_PARSE_STRUCIO_GRAMMAR_HPP_