#ifndef TESTCASE_PARSE_EBNF_GRAMMAR_HPP_
#define TESTCASE_PARSE_EBNF_GRAMMAR_HPP_
#include "../templates/ebnf.hpp"
#include "all.hpp"

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif

#include <boost/test/unit_test.hpp>
using namespace base::grammar::pattern::ebnf;

BOOST_AUTO_TEST_SUITE(ParseEBNF);
#if CHECK_CASE_EBNF_NUMLETTER || !CHECK_CASE
BOOST_AUTO_TEST_CASE(NumLetter) {
  base::Deserialize<> pattern(Lexical<>::make_shared(number >> letter));

  BOOST_REQUIRE((pattern << '1' << '2' << 'a') == base::grammar::Don);
  BOOST_REQUIRE(pattern.reset() == false);
  BOOST_REQUIRE((pattern << '1' << '2' << '0') != base::grammar::Don);
  BOOST_REQUIRE(pattern.reset() == false);
  BOOST_REQUIRE((pattern << "12a") == base::grammar::Don);
  BOOST_REQUIRE(pattern.reset() == false);
  BOOST_REQUIRE((pattern << "120") != base::grammar::Don);
}
#endif // CHECK_CASE_EBNF_NUMLETTER

#if CHECK_CASE_EBNF_MATCH || !CHECK_CASE
BOOST_AUTO_TEST_CASE(Match) {
  base::Deserialize<> b(match("GET"));

  BOOST_REQUIRE((b << "GET") == base::grammar::Don);
  BOOST_REQUIRE(b.reset() == false);
  BOOST_REQUIRE((b << "GES") != base::grammar::Don);
  BOOST_REQUIRE(b.reset() == false);
  BOOST_REQUIRE((b << "GAT") != base::grammar::Don);
  BOOST_REQUIRE(b.reset() == false);
  BOOST_REQUIRE((b << "PET") != base::grammar::Don);
}
#endif // CHECK_CASE_EBNF_MATCH
BOOST_AUTO_TEST_SUITE_END();
#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

using namespace base::grammar::pattern::ebnf;
#if CHECK_CASE_EBNF_NUMLETTER || !CHECK_CASE
TEST(ENBF, NumLetter) {
  base::Deserialize<> pattern(Lexical<>::make_shared(number >> letter));

  EXPECT_TRUE((pattern << '1' << '2' << 'a') == base::grammar::Don);
  EXPECT_TRUE(pattern.reset() == false);
  EXPECT_TRUE((pattern << '1' << '2' << '0') != base::grammar::Don);
  EXPECT_TRUE(pattern.reset() == false);
  EXPECT_TRUE((pattern << "12a") == base::grammar::Don);
  EXPECT_TRUE(pattern.reset() == false);
  EXPECT_TRUE((pattern << "120") != base::grammar::Don);
}
#endif // CHECK_CASE_EBNF_NUMLETTER

#if CHECK_CASE_EBNF_MATCH || !CHECK_CASE
TEST(EBNF, Match) {
  base::Deserialize<> b(match("GET"));

  EXPECT_TRUE((b << "GET") == base::grammar::Don);
  EXPECT_TRUE(b.reset() == false);
  EXPECT_TRUE((b << "GES") != base::grammar::Don);
  EXPECT_TRUE(b.reset() == false);
  EXPECT_TRUE((b << "GAT") != base::grammar::Don);
  EXPECT_TRUE(b.reset() == false);
  EXPECT_TRUE((b << "PET") != base::grammar::Don);
}
#endif // CHECK_CASE_EBNF_MATCH

#if CHECK_CASE_EBNF_HTTP || !CHECK_CASE
TEST(EBNF, HTTP) {
  /* http protocol use libgrammar::EBNF */
  auto Method = str_("OPTIONS") | str_("GET") | str_("HEAD") | str_("POST") |
                str_("PUT") | str_("DELETE") | str_("TRACE") | str_("CONNECT");
  auto CRLF = str_("\r\n");
  /* auto SP = +' ';
   * auto Request_URI = +letter;
   * auto HTTP_Version = "HTTP/" >> number >> "." >> number;
   * auto Request_Line = Method >> SP >> Request_URI >> SP >> HTTP_Version >>
   * CRLF;
   */
  /*
   * auto Context_Name = +letter;
   * auto Context_Value = +letter;
   * auto Request = Request_Line >> *(+(Context_Name >> SP >> ':' >> SP >>
   * Context_Value >> +SP) >> CRLF) >> CRLF;
   */
  /* Request << "GET /hello_world HTTP/1.1\r\n\r\n";
   */
}
#endif // CHECK_CASE_EBNF_HTTP

#if CHECK_CASE_EBNF_PASCAL || !CHECK_CASE
#endif

#if CHECK_CASE_EBNF_PYTHON || !CHECK_CASE
#endif

#if CHECK_CASE_EBNF_C || !CHECK_CASE
#endif
#endif // BOOST_TEST_MAIN
#endif // TESTCASE_PARSE_EBNF_GRAMMAR_HPP_