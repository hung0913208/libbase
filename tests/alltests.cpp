#include "./stream.hpp"
#include "./table.hpp"
#include "./line.hpp"
#include "./disperse.hpp"
#include "./unittest.hpp"
#include "./generate/strucio.hpp"
#include "./parse/ebnf.hpp"
#include "./parse/strucio.hpp"
#include "./wav.hpp"
#include "./auto.hpp"
#include "./tuple.hpp"
#include "./tie.hpp"
#include "./trace.hpp"

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif

#include <boost/test/unit_test.hpp>

struct Config{
  std::string console{"console.txt"};

  Config(){ 
    base::config::start(nullptr, 0); 
    base::config::set("console", base::Auto{}.set(console));
    base::config::set("monitor", base::Auto{}.set(true));
  }
  ~Config(){ base::config::finish(0); }
};

BOOST_GLOBAL_FIXTURE(Config);

BOOST_AUTO_TEST_SUITE(Basic)
#if TRACE && (CHECK_CASE_LIBBASE_LINE || !CHECK_CASE)
BOOST_AUTO_TEST_CASE(Line) {
#if NDEBUG
  auto result = base::test::simline(
      1.0, 3.0, 100000,
      std::function<double(int)>{[](int) -> double { return rand(); }});
#else
  auto result = base::test::simline(
      1.0, 3.0, 10,
      std::function<double(int)>{[](int) -> double { return rand(); }});

  std::cout << result << std::endl;
  BOOST_REQUIRE(result == MAX_TESTCASE_SIMLINE);
#endif
}
#endif  // CHECK_CASE_LIBBASE_LINE

#if TRACE && (CHECK_CASE_LIBBASE_TABLE || !CHECK_CASE)
BOOST_AUTO_TEST_CASE(Table) {
#if NDEBUG
  auto result = base::test::simtable<int, double>(
      []() -> int { return rand(); }, {1.0, 2.0, 3.0}, 1000000, 250000000);
#else
  auto result = base::test::simtable<int, double>(
      []() -> int { return rand(); }, {1.0, 2.0, 3.0}, 100, 20);
  BOOST_REQUIRE(result == MAX_TESTCASE_SIMTABLE);
#endif
}
#endif  // CHECK_CASE_LIBBASE_TABLE

#if TRACE && (CHECK_CASE_LIBBASE_STREAM || !CHECK_CASE)
BOOST_AUTO_TEST_CASE(Buffer) {
  auto result =
      base::test::simbuffer(4, {uint8_t(1), uint8_t(2), uint8_t(3), uint8_t(4),
                                uint8_t(5), uint8_t(6), uint8_t(7), uint8_t(8),
                                uint8_t(9), uint8_t(10), uint8_t(11),
                                uint8_t(12), uint8_t(13)});
  BOOST_REQUIRE(result == MAX_TESTCASE_SIMBUFFER);
}

BOOST_AUTO_TEST_CASE(Stream) {
  auto result =
      base::test::simstream({uint8_t(1), uint8_t(2), uint8_t(3), uint8_t(4),
                             uint8_t(5), uint8_t(6), uint8_t(7), uint8_t(8),
                             uint8_t(9), uint8_t(10), uint8_t(11), uint8_t(12),
                             uint8_t(13)});
  BOOST_REQUIRE(result == MAX_TESTCASE_SIMSTREAM);
}
#endif  // CHECK_CASE_LIBBASE_STREAM
BOOST_AUTO_TEST_SUITE_END();
#else

#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#if TRACE && (CHECK_CASE_LIBBASE_LINE || !CHECK_CASE)
TEST(Base, Line) {
#if NDEBUG
  auto result = base::test::simline(
      1.0, 3.0, 100000,
      std::function<double(int)>{[](int) -> double { return rand(); }});
#else
  auto result = base::test::simline(
      1.0, 3.0, 10,
      std::function<double(int)>{[](int) -> double { return rand(); }});
  EXPECT_TRUE(result == MAX_TESTCASE_SIMLINE);
#endif
}
#endif  // CHECK_CASE_LIBBASE_LINE

#if TRACE && (CHECK_CASE_LIBBASE_TABLE || !CHECK_CASE)
TEST(Base, Table) {
#if NDEBUG
  auto result = base::test::simtable<int, double>(
      []() -> int { return rand(); }, {1.0, 2.0, 3.0}, 1000000, 250000000);
#else
  auto result = base::test::simtable<int, double>(
      []() -> int { return rand(); }, {1.0, 2.0, 3.0}, 100, 20);
  EXPECT_TRUE(result == MAX_TESTCASE_SIMTABLE);
#endif
}
#endif  // CHECK_CASE_LIBBASE_TABLE

#if TRACE && (CHECK_CASE_LIBBASE_STREAM || !CHECK_CASE)
TEST(Basic, Buffer) {
  auto result =
      base::test::simbuffer(4, {uint8_t(1), uint8_t(2), uint8_t(3), uint8_t(4),
                                uint8_t(5), uint8_t(6), uint8_t(7), uint8_t(8),
                                uint8_t(9), uint8_t(10), uint8_t(11),
                                uint8_t(12), uint8_t(13)});
  EXPECT_TRUE(result == MAX_TESTCASE_SIMBUFFER);
}

TEST(Basic, Stream) {
  auto result =
      base::test::simstream({uint8_t(1), uint8_t(2), uint8_t(3), uint8_t(4),
                                uint8_t(5), uint8_t(6), uint8_t(7), uint8_t(8),
                                uint8_t(9), uint8_t(10), uint8_t(11),
                                uint8_t(12), uint8_t(13)});
  EXPECT_TRUE(result == MAX_TESTCASE_SIMSTREAM);
}
#endif  // CHECK_CASE_LIBBASE_STREAM

#if CHECK_CASE_READ_WAV
TEST(Wav, Read) {
  test::WavF<> wrong("/tmp/wrong.wav");
  test::WavF<> pass("/tmp/pass.wav");
}
#endif  // CHECK_CASE_READ_WAV

#include <config.hpp>
int main(int argc, char **argv) {
  using namespace base;

  std::string console{"console.txt"};

  base::config::start(argv, argc);
 #ifdef GTEST_INCLUDE_GTEST_GTEST_H_ 
  ::testing::InitGoogleTest(&argc, argv);
 #else
  base::testing::init(argc, (const char**)(argv));
 #endif

  base::config::set("console", base::Auto{}.set(console));
  base::config::set("monitor", base::Auto{}.set(true));
  return base::config::finish(RUN_ALL_TESTS());
}
#endif
