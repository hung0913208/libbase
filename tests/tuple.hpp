#ifndef TESTCASE_TUPLE_HPP_
#define TESTCASE_TUPLE_HPP_
#include "../all.hpp"

base::Tuple test_make_inside(){
  return base::Tuple::make();
}

base::Tuple test_expand_inside(base::Tuple tuple){
  for (auto i = 0; i < 6; i++)
    tuple.add(i);

  return tuple;
}

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(Tuple);
#if CHECK_CASE_LIBBASE_TUPLE || !CHECK_CASE
BOOST_AUTO_TEST_CASE(Basic) {
  auto tuple  = base::Tuple::make();
  auto raw    = base::Auto();
  auto copy   = tuple;
  auto inside = test_make_inside();

  raw = 10;

  for (auto i = 0; i < 6; i++)
    BOOST_REQUIRE(tuple.add(i) == std::size_t(i));
  BOOST_REQUIRE(tuple.size() == 6);

  for (auto i = 0; i < cast_(i, tuple.size()); ++i)
    BOOST_REQUIRE(tuple.type(i) == typeid(int));
  for (auto i = 0; i < cast_(i, tuple.size()); ++i)
    BOOST_REQUIRE_NO_THROW(tuple.get<int>(i));
  for (auto i = 0; i < cast_(i, tuple.size()); i++)
    BOOST_REQUIRE(tuple.get<int>(i) == i);

  BOOST_REQUIRE(tuple.add(raw) == 6);
  BOOST_REQUIRE(tuple.get<int>(6) == 10);

  auto expanded = test_expand_inside(tuple);

  BOOST_REQUIRE(expanded.size() == 13);
}
#endif
BOOST_AUTO_TEST_SUITE_END();

#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#if CHECK_CASE_LIBBASE_TUPLE || !CHECK_CASE
TEST(Tuple, Basic) {
  auto tuple = base::Tuple::make();
  auto raw   = base::Auto();
  auto copy   = tuple;
  auto inside = test_make_inside();

  raw = 10;

  for (auto i = 0; i < 6; i++){
    EXPECT_EQ(tuple.add(i), std::size_t(i));
  }
  EXPECT_EQ(tuple.size(), std::size_t(6));

  for (auto i = 0; i < cast_(i, tuple.size()); ++i)
    EXPECT_TRUE(tuple.type(i) == typeid(int));
  for (auto i = 0; i < cast_(i, tuple.size()); ++i)
    EXPECT_NO_THROW(tuple.get<int>(i));
  for (auto i = 0; i < cast_(i, tuple.size()); i++)
    EXPECT_EQ(tuple.get<int>(i), i);

  EXPECT_EQ(tuple.add(raw), std::size_t(6));
  EXPECT_EQ(tuple.get<int>(6), 10);

  auto expanded = test_expand_inside(tuple);
  EXPECT_EQ(expanded.size(), std::size_t(13));
}
#endif
#endif
#endif  // TESTCASE_TUPLE_HPP_