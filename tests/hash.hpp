#ifndef TESTCASE_HASH_HPP_
#define TESTCASE_HASH_HPP_

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif

#include <boost/test/unit_test.hpp>
BOOST_AUTO_TEST_CASE(Basic) {}
#if CHECK_CASE_LIBBASE_HASH
#endif
#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#if CHECK_CASE_LIBBASE_HASH
TEST(Hash, Basic) {}
#endif
#endif
#endif // TESTCASE_HASH_HPP_