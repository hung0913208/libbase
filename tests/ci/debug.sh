bash $1/tools/notify.sh "#general" "Build on \`$(git rev-parse --short HEAD)\` failed! Please check and run debug if it's needed"
bash $1/tools/debug.sh gdbserver Debug tests/alltests
bash $1/tools/debug.sh valgrind Debug tests/alltests
