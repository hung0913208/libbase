unameOut="$(uname -s)"
case "${unameOut}" in
        Linux*)     machine=Linux;;
        Darwin*)    machine=Mac;;
        CYGWIN*)    machine=Cygwin;;
        MINGW*)     machine=MinGw;;
        *)          machine="UNKNOWN:${unameOut}"
esac

if [[ $machine == 'Linux' ]] || [[ $machine == 'Mac' ]]; then
    cd $1/build
    bash $1/tools/run.sh Release 
    if [ $? != 0 ]; then
        exit 33
    fi
    bash $1/tools/run.sh Profiling
    if [ $? != 0 ]; then
        exit 33
    fi
    bash $1/tools/run.sh Debug tests/alltests
    if [ $? == 0 ]; then
        export TOOL="gdbserver"
        cd $1/build
        bash $1/tools/run.sh Debug tests/alltests
    fi
    if [ $? == 0 ]; then
        export TOOL="valgrind"
        cd $1/build
        bash $1/tools/valgrind.sh Coverage tests/alltests
    fi

    # if [[ $machine == 'Linux' ]]; then
    if [[ "$CC" =~ "clang" ]] || [[ "$CXX" =~ "clang++" ]]; then
        exit 0
    else
        cd $1/build
        bash $1/tools/coverage.sh LibBase_coverage
        if [ $? == 0 ]; then
            export TOOL=""
            exit 0
        else
            exit 33
        fi
    fi
fi