unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

cd $1/build
if [[ $machine == 'Linux' ]] && [[ $2 == "Ubuntu" ]]; then
    sudo apt-get update
    sudo apt-get install -y lcov valgrind gdb libboost-all-dev llvm
    sudo bash $1/tools/gtest.sh $BRANCH $1/build 
elif [[ $machine == 'Linux' ]] && [[ $2 == "Opensuse" ]]; then
    sudo zypper install -y lcov valgrind gdb boost-devel llvm
    sudo bash $1/tools/gtest.sh $BRANCH $1/build 
elif [ $machine == 'Mac' ]; then
    brew bundle
    brew install llvm
    brew install lcov
    brew install valgrind
    brew install tree
fi

