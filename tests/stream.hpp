#if !defined(LIBBASE_STREAM_TEST_HPP_) && __cplusplus
#define LIBBASE_STREAM_TEST_HPP_
#include "../all.hpp"

#define MAX_TESTCASE_SIMBUFFER 3
#define MAX_TESTCASE_SIMSTREAM 0

namespace base{
namespace test{
std::size_t simbuffer(std::size_t max, std::vector<uint8_t> data){
 #if TRACE==1
  REGISTER_RETURNING_BUFFER_WITH_INIT(std::size_t) = 0;

  auto body = [&]() -> std::size_t{
 #else
   BEGIN_RTRACKING(std::size_t)
    auto result = 0;
 #endif
    try{
      Buffer buffer;

      PCheck(buffer.init(new uint8_t[max], max));

     #if TRACE==1
      CHECK_PASSING_LINE(RETURNING_BUFFER++);
     #else
      CHECK({ ++result; });
     #endif

      int UNUSED(i) = 0;
      for (auto item: data){
        i++;
        auto recved = 0;

        if (buffer.write(item))
          continue;

        PAssert(buffer.full());
        try{ while (buffer.read()) ++recved; } 
        catch(base::Error& error){
          PAssert(error == base::error::EOutOfRange);
          PAssert(buffer.empty());
        }

        PAssert(buffer.write(item));
       #if TRACE==1
        TAKE_VSNAPSHOT("recved", recved);
       #else
        SNAPSHOT(recved);
       #endif
        PAssert(recved == cast_(recved, max));
      }

     #if TRACE==1
      CHECK_PASSING_LINE(RETURNING_BUFFER++);
      TAKE_VSNAPSHOT("buffer.size()", buffer.size());
     #else
      CHECK({ ++result; });
      SNAPSHOT(buffer.size());
     #endif
      PAssert(data.size()%max == buffer.size());

      PAssert(buffer.revert((buffer.max() - buffer.size())/2));
     #if TRACE==1
      TAKE_VSNAPSHOT("buffer.size() after revert", buffer.size());
     #else
      SNAPSHOT(buffer.size());
     #endif

      PAssert(!buffer.revert(buffer.max()));
      PAssert((buffer.size() == buffer.max()) == buffer.full());
      PAssert((buffer.size() == 0) == buffer.empty());
     #if TRACE==1
      CHECK_PASSING_LINE(RETURNING_BUFFER++);
     #else
      CHECK({ ++result; });
     #endif
    } catch(base::Error&){}

   #if TRACE==1
    return RETURNING_BUFFER;
   #else
    return result;
   #endif

 #if TRACE==1
  };
  BUILD_FTRACKING(body);
  return RETURN_FROM_TRACKING;
 #else
  END_FTRACKING;
 #endif
}

std::size_t simstream(std::vector<uint8_t> data){
  auto stream = std::make_shared<Stream>(
    [](uint8_t*, std::size_t&) -> base::Error{
      return NoError;
    },
    [](uint8_t*, std::size_t&) -> base::Error{
      return NoError;
    }
  );

  uint8_t  u8;
  uint16_t u16;
  uint32_t u32;
  uint64_t u64;

  stream->input() = std::make_shared<Buffer>();
  stream->output() = std::make_shared<Buffer>();

  std::dynamic_pointer_cast<Buffer>(stream->input())->init(new uint8_t[data.size()], data.size());
  std::dynamic_pointer_cast<Buffer>(stream->output())->init(new uint8_t[data.size()], data.size());

  u8 = stream->rByte();
  stream->wByte(u8);
  u16 = stream->rWord();
  stream->wWord(u16);
  u32 = stream->rDWord();
  stream->wDWord(u32);
  u64 = stream->rLWord();
  stream->wLWord(u64);

  stream->copyFrom(&u32, 1);
  // stream->copyTo(&u32, 1);

  stream->revert(true, 1);
  stream->flush();
  return 0;
}

std::size_t simauto(){
  return 0;
}
} // namespace test
} // namespace base
#endif  // LIBBASE_STREAM_TEST_HPP_