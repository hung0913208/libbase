#ifndef TESTCASE_GENERATE_STRUCIO_GRAMMAR_HPP_
#define TESTCASE_GENERATE_STRUCIO_GRAMMAR_HPP_
#include "../templates/strucio.hpp"

#ifdef BOOST_TEST_MAIN
#ifndef BOOST_TEST_DYN_LINK
#define BOOST_TEST_DYN_LINK
#endif

#include <boost/test/unit_test.hpp>
using namespace base::grammar::pattern::strucio;

BOOST_AUTO_TEST_SUITE(GenerateStrucIO);
#if CHECK_CASE_PUSH_SIMPLE || !defined(CHECK_CASE)
BOOST_AUTO_TEST_CASE(PushBasic) {
  uint8_t a = 10;
  uint16_t b = 20;

  char c;
  base::Serialize<> pattern(Lexical<>::make_shared(byte_[a] >> word_[b]));

  pattern >> c;
  pattern >> c;
  pattern >> c;
}
#endif // CHECK_CASE_PULL_SIMPLE
BOOST_AUTO_TEST_SUITE_END();
#else
#if USE_BASE_UNITTEST
#include "../unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

using namespace base::grammar::pattern::strucio;

#if CHECK_CASE_PUSH_SIMPLE || !defined(CHECK_CASE)
TEST(StrucIO, PushBasic) {
  uint8_t a = 10;
  uint16_t b = 20;

  char c;

  base::Serialize<> pattern(Lexical<>::make_shared(byte_[a] >> word_[b]));

  pattern >> c;
  EXPECT_TRUE(c == 10);
  pattern >> c;
  EXPECT_TRUE(c == 0);
  pattern >> c;
  EXPECT_TRUE(c == 20);
}
#endif // CHECK_CASE_PUSH_SIMPLE

#if CHECK_CASE_PUSH_REPEATE
TEST(StrucIO, PushRepeate) {
  uint8_t a = 10, c = 0;
  uint16_t b = 20, d = 0;

  base::Serialize<> pattern(Lexical<>::make_shared(byte_[a] >> word_[b]));

  pattern >> c >> d;
  std::cout << c << " " << d << std::endl;
}
#endif // CHECK_CASE_PUSH_REPEATE
#endif // BOOST_TEST_MAIN
#endif // TESTCASE_GENERATE_STRUCIO_GRAMMAR_HPP_