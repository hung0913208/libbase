#if !defined(LIBBASE_LINE_TEST_HPP_) && __cplusplus
#define LIBBASE_LINE_TEST_HPP_
#include "../all.hpp"

#define MAX_TESTCASE_SIMLINE 5

namespace base {
namespace test {
template <typename Type>
std::size_t simline(Type none, Type first, int max,
                    std::function<Type(int)> rand) {
 #if TRACE==1
  REGISTER_RETURNING_BUFFER_WITH_INIT(std::size_t) = 0;

  auto body = [&]() -> std::size_t {
 #else
  BEGIN_RTRACKING(std::size_t)
    auto result = 0;
 #endif
    auto swift = false;
    Line<Type, Type> num_line_0(none, first,
                                [](Type &&value) -> Type { return value; });

    Line<Type, Type> num_line_1(
        [&](Type &&value) -> bool {
          swift = !swift;
          return swift && value == none;
        },
        first, [=](Type &&value) -> Type { return value; });

    Line<std::shared_ptr<Type>, Type> num_line_2(
        first, [&](std::shared_ptr<Type> &&value) -> Type {
          return value ? *value : none;
        });

    Line<Type, Type> num_line_3(first,
                                [](Type &&value) -> Type { return value; });
   #if TRACE==1
    CHECK_CHECKING_LINE(++RETURNING_BUFFER);
   #else
    CHECK({ ++result; });
   #endif
    {
      for (auto i = 0; i < max; i++) {
        num_line_0.exchange(
            rand(i),
            std::function<bool(Type &&)>{[&num_line_0, &i](Type &&val) -> bool {
              return val - 1 == num_line_0.current() && (i % 2);
            }});
        num_line_1.exchange(
            rand(i),
            std::function<bool(Type &&)>{[&num_line_1, &i](Type &&val) -> bool {
              return val - 1 == num_line_1.current() && (i % 2);
            }});
        num_line_2.exchange(
            std::make_shared<Type>(rand(i)),
            std::function<bool(std::shared_ptr<Type> &&)>{
                [&num_line_2, &i](std::shared_ptr<Type> &&val) -> bool {
                  return *val - 1 == num_line_2.current() && (i % 2);
                }});
        num_line_3.exchange(
            rand(i),
            std::function<bool(Type &&)>{[&num_line_3, &i](Type &&val) -> bool {
              return val - 1 == num_line_3.current() && (i % 2);
            }});

        num_line_0.exchange(rand(i), nullptr);
        num_line_1.exchange(rand(i), nullptr);
        num_line_2.exchange(std::make_shared<Type>(rand(i)), nullptr);
        num_line_3.exchange(rand(i), nullptr);

        if (i % 2) {
          num_line_0.swap(
              rand(i), std::function<bool(Type &&)>{
                           [&num_line_0, &i](Type &&val) -> bool {
                             return val - 1 == num_line_0.current() && (i % 2);
                           }});
          num_line_1.swap(
              rand(i), std::function<bool(Type &&)>{
                           [&num_line_1, &i](Type &&val) -> bool {
                             return val - 1 == num_line_1.current() && (i % 2);
                           }});
          num_line_2.swap(
              std::make_shared<Type>(rand(i)),
              std::function<bool(std::shared_ptr<Type> &&)>{
                  [&num_line_2, &i](std::shared_ptr<Type> &&val) -> bool {
                    return *val - 1 == num_line_2.current() && (i % 2);
                  }});
          num_line_3.swap(
              rand(i), std::function<bool(Type &&)>{
                           [&num_line_3, &i](Type &&val) -> bool {
                             return val - 1 == num_line_3.current() && (i % 2);
                           }});

          num_line_0.swap(rand(i), nullptr);
          num_line_1.swap(rand(i), nullptr);
          num_line_2.swap(std::make_shared<Type>(rand(i)), nullptr);
          num_line_3.swap(rand(i), nullptr);
        }
      }
    }

   #if TRACE==1
    CHECK_CHECKING_LINE(++RETURNING_BUFFER);
   #else
    CHECK({ ++result; });
   #endif
    {
      num_line_0.get();
      num_line_0.size();
      num_line_0.begin();
      num_line_1.get();
      num_line_1.size();
      num_line_1.begin();
      num_line_2.get();
      num_line_2.size();
      num_line_2.begin();
      num_line_3.get();
      num_line_3.size();
      num_line_3.begin();

      num_line_0[0];
      num_line_1[0];
      num_line_2[0];
      num_line_3[0];
    }

   #if TRACE==1
    CHECK_CHECKING_LINE(++RETURNING_BUFFER);
   #else
    CHECK({ ++result; });
   #endif
    {
     #if TRACE==1
      CHECK_CHECKING_LINE(while ((num_line_0.flush()) != num_line_0.null()){});k
      CHECK_CHECKING_LINE(while ((num_line_1.flush()) != num_line_1.null()){});
      CHECK_CHECKING_LINE(while ((num_line_2.flush()) != num_line_2.null()){});
      CHECK_CHECKING_LINE(while ((num_line_3.flush()) != num_line_3.null()){});
     #else
      CHECK(while ((num_line_0.flush()) != num_line_0.null()){});
      CHECK(while ((num_line_1.flush()) != num_line_1.null()){});
      CHECK(while ((num_line_2.flush()) != num_line_2.null()){});
      CHECK(while ((num_line_3.flush()) != num_line_3.null()){});
     #endif
    }

   #if TRACE==1
    CHECK_CHECKING_LINE(++RETURNING_BUFFER);
   #else
    CHECK({ ++result; });
   #endif
    {
     #if TRACE==1
      CHECK_CHECKING_LINE(num_line_0.get());
      CHECK_CHECKING_LINE(num_line_0.size());
      CHECK_CHECKING_LINE(num_line_0.begin());
      CHECK_CHECKING_LINE(num_line_1.get());
      CHECK_CHECKING_LINE(num_line_1.size());
      CHECK_CHECKING_LINE(num_line_1.begin());
      CHECK_CHECKING_LINE(num_line_2.get());
      CHECK_CHECKING_LINE(num_line_2.size());
      CHECK_CHECKING_LINE(num_line_2.begin());
      CHECK_CHECKING_LINE(num_line_3.get());
      CHECK_CHECKING_LINE(num_line_3.size());
      CHECK_CHECKING_LINE(num_line_3.begin());
     #else
      CHECK(num_line_0.get());
      CHECK(num_line_0.size());
      CHECK(num_line_0.begin());
      CHECK(num_line_1.get());
      CHECK(num_line_1.size());
      CHECK(num_line_1.begin());
      CHECK(num_line_2.get());
      CHECK(num_line_2.size());
      CHECK(num_line_2.begin());
      CHECK(num_line_3.get());
      CHECK(num_line_3.size());
      CHECK(num_line_3.begin());
     #endif
    }

   #if TRACE==1
    CHECK_CHECKING_LINE(++RETURNING_BUFFER);
    return RETURNING_BUFFER;
   #else
    CHECK({ ++result; });
    return result;
   #endif

 #if TRACE==1
  };

  BUILD_FTRACKING(body);
  return RETURN_FROM_TRACKING;
 #else
  END_FTRACKING;
 #endif
}
} // namespase test
} // namespace base
#endif  // LIBBASE_LINE_TEST_HPP_