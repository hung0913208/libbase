#ifndef LIBBASE_GRAMMAR_STRUCIO_HPP_
#define LIBBASE_GRAMMAR_STRUCIO_HPP_
#include "all.hpp"

#define STRUCIO_PROPERTY_PREFIX std::string{"strucio_"}
#define STRUCIO_GRAMMAR_TYPE 1

namespace base{
namespace grammar{
namespace strucio{
using byte  = uint8_t;
using word  = uint16_t;
using dword = uint32_t;
using lword = uint64_t;

template<typename Type>
Status flate(base::Pipe<>&, std::size_t index, char, bool){ return index + 1== sizeof(Type)? Don: Unf; }

template<typename Type>
Lexical<> attribute(Lexical<> lexical){
  auto property = STRUCIO_PROPERTY_PREFIX + typeid(Type).name();

  Property::assign(property, [](Property* prop, std::shared_ptr<Context> context) -> Context*{
    auto gram = std::dynamic_pointer_cast<Grammar<>>(context);
    auto name = STRUCIO_PROPERTY_PREFIX + typeid(Type).name();

    if (prop->name != name) return nullptr;
    else if (gram){
      auto source = std::dynamic_pointer_cast<Attribute<Type>>(gram->gate());
      auto encode = [](Type& value) -> std::vector<char>{
        std::vector<char> result;

       #if BYTE_ORDER == BIG_ENDIAN
        value = base::reverse(value);
       #endif
        for (auto i = sizeof(Type); i > 0; --i)
          result.push_back((reinterpret_cast<char*>(&value))[i - 1]);

        return result;
      };
      auto decode = [](std::vector<char>& value) -> Type{
        Type result;

        for (auto i = 0; i < cast_(i, value.size()); ++i)
          (reinterpret_cast<char*>(&result))[i] = value[i];

       #if BYTE_ORDER == BIG_ENDIAN
        value = base::reverse(value);
       #endif

        return result;
      };

      gram->bridge<Type>(std::make_shared<Bridge<Type>>(source, decode, encode));
    }
    return nullptr;
  });
  return lexical.set(property, LIBBASE_GRAMMAR_TRUE_VALUE);
}
} // namespace strucio

namespace pattern{
namespace strucio{
namespace def = grammar::strucio;
/* patterns for parsing with Struct IO */
/* functions */
template<typename Type> Lexical<> flate(){
  return Lexical<>(STRUCIO_GRAMMAR_TYPE, std::make_shared<Grammar<>>(def::flate<Type>, def::flate<Type>));
}

/* defifnitions */
static auto byte_  = def::attribute<def::byte>(flate<def::byte>());
static auto word_  = def::attribute<def::word>(flate<def::word>());
static auto dword_ = def::attribute<def::dword>(flate<def::dword>());
static auto lword_ = def::attribute<def::lword>(flate<def::lword>());

// static auto str_ = group(dword_ >> def::sequence());
// static auto lst_ = Lexical<>(STRUCIO_GRAMMAR_TYPE, std::make_shared<Grammar<>>(def::));
} // namespace strucio
} // namespace pattern
} // namespace grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_STRUCK_HPP_