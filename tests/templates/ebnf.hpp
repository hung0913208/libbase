#ifndef LIBBASE_GRAMMAR_EBNF_HPP_
#define LIBBASE_GRAMMAR_EBNF_HPP_

#include "all.hpp"

#define EBNF_GRAMMAR_TYPE 0 

namespace base{
namespace grammar{
namespace ebnf{
namespace parser{
static auto equal = std::function<Status(Content<>&, std::size_t, char, bool)>{
  [](Content<>& self, std::size_t, char buff, bool) -> Status{
    return self.content()[0] == buff? Don: Fal;
  }
};

static auto not_equal = std::function<Status(Content<>&, std::size_t, char, bool)>{
  [](Content<>& self, std::size_t, char buff, bool) -> Status{
    return self.content()[0] != buff? Don: Fal;
  }
};

static auto range_without_bounder = std::function<Status(Content<>&, std::size_t, char, bool)>{
  [](Content<>& self, std::size_t, char buff, bool) -> Status{
    return self.content()[0] < buff && buff < self.content()[1]? Don: Fal;
  }
};

static auto range_with_right_bounder = std::function<Status(Content<>&, std::size_t, char, bool)>{
  [](Content<>& self, std::size_t, char buff, bool) -> Status{
    return self.content()[0] < buff && buff <= self.content()[1]? Don: Fal;
  }
};

static auto range_with_left_bounder = std::function<Status(Content<>&, std::size_t, char, bool)>{
  [](Content<>& self, std::size_t, char buff, bool) -> Status{
    return self.content()[0] <= buff && buff < self.content()[1]? Don: Fal;
  }
};

static auto range_with_bounder = std::function<Status(Content<>&, std::size_t, char, bool)>{
  [](Content<>& self, std::size_t, char buff, bool) -> Status{
    return self.content()[0] <= buff && buff <= self.content()[1]? Don: Fal;
  }
};
}

namespace generator{
static auto equal = std::function<Status(Content<>&, std::size_t, char&, bool)>{
  [](Content<>&, std::size_t, char&, bool) -> Status{
    return Don;
  }
};

static auto not_equal = std::function<Status(Content<>&, std::size_t, char&, bool)>{
  [](Content<>&, std::size_t, char&, bool) -> Status{
    return Don;
  }
};

static auto range_without_bounder = std::function<Status(Content<>&, std::size_t, char&, bool)>{
  [](Content<>&, std::size_t, char&, bool) -> Status{
    return Don;
  }
};

static auto range_with_left_bounder = std::function<Status(Content<>&, std::size_t, char&, bool)>{
  [](Content<>&, std::size_t, char&, bool) -> Status{
    return Don;
  }
};

static auto range_with_right_bounder = std::function<Status(Content<>&, std::size_t, char&, bool)>{
  [](Content<>&, std::size_t, char&, bool) -> Status{
    return Don;
  }
};

static auto range_with_bounder = std::function<Status(Content<>&, std::size_t, char&, bool)>{
  [](Content<>&, std::size_t, char&, bool) -> Status{
    return Don;
  }
};
}

template <typename ByteT=char>
std::vector<Lexical<ByteT>> make_concat_gram(std::vector<ByteT> samples){
  auto result = std::vector<Lexical<ByteT>>{};
  auto pull = std::function<Status(Content<ByteT>&, std::size_t, ByteT, bool)>{
    [](Content<ByteT>& self, std::size_t, ByteT buff, bool) -> Status{
      return self.content()[0] == buff? Don: Fal;
    }
  };
  auto push = std::function<Status(Content<ByteT>&, std::size_t, ByteT&, bool)>{
    [](Content<ByteT>&, std::size_t, ByteT&, bool) -> Status{
      return Don;
    }
  };

  for (auto& sample: samples)
    result.push_back(Lexical<ByteT>{EBNF_GRAMMAR_TYPE,
      std::make_shared<Content<ByteT>>(sample, pull, push)});

  return result;
}

template <typename ByteT=char>
std::vector<Lexical<ByteT>> make_concat_gram(std::vector<Lexical<ByteT>> group){
  std::vector<Lexical<ByteT>> result{};

  for (auto& item: group){
    if (result.size())
      result = result >> item;
    else result.push_back(item);
  }

  return result;
}

template <typename ByteT=char>
std::vector<Lexical<ByteT>> make_concat_gram(std::basic_string<ByteT> samples){
  auto result = std::vector<Lexical<ByteT>>{};
  auto pull = std::function<Status(Content<ByteT>&, std::size_t, ByteT, bool)>{
    [](Content<ByteT>& self, std::size_t, ByteT buff, bool) -> Status{
      return self.content()[0] == buff? Don: Fal;
    }
  };
  auto push = std::function<Status(Content<ByteT>&, std::size_t, ByteT&, bool)>{
    [](Content<ByteT>&, std::size_t, ByteT&, bool) -> Status{
      return Don;
    }
  };

  for (auto& sample: samples)
    result.push_back(Lexical<ByteT>{EBNF_GRAMMAR_TYPE, 
      std::make_shared<Content<ByteT>>(sample, pull, push)});

  return result;
}

template <typename ByteT=char> 
Lexical<ByteT> make_single_gram(ByteT sample){
  auto pull = std::function<Status(base::Pipe<ByteT>&, std::size_t, ByteT, bool)>{
    [sample](base::Pipe<ByteT>&, std::size_t, ByteT  buff, bool) -> Status{
      return buff == sample? Don: Fal;
    }};
  auto push = std::function<Status(base::Pipe<ByteT>&, std::size_t, ByteT&, bool)>{
    [](base::Pipe<ByteT>&, std::size_t, ByteT& UNUSED(buff), bool) -> Status{
      return Don;
    }};
  
  return Lexical<ByteT>{EBNF_GRAMMAR_TYPE, std::make_shared<Grammar<ByteT>>(pull, push)};
}

std::vector<char> regex(std::string pattern);
} // namespace ebnf

namespace pattern{
/* patterns for parsing with EBFS language */
namespace ebnf{
/* definitions */
namespace range{
static auto digit  = {'0', '9'};
static auto higher = {'A', 'Z'};
static auto lower  = {'a', 'z'};
} // namespace range

namespace def{
static auto digit = std::make_shared<Content<>>(range::digit, 
    grammar::ebnf::parser::range_with_bounder, grammar::ebnf::generator::range_with_bounder);

static auto higher = std::make_shared<Content<>>(range::higher, 
    grammar::ebnf::parser::range_with_bounder, grammar::ebnf::generator::range_with_bounder);

static auto lower = std::make_shared<Content<>>(range::lower, 
    grammar::ebnf::parser::range_with_bounder, grammar::ebnf::generator::range_with_bounder);
} // namespace def

static auto letter = Lexical<>{EBNF_GRAMMAR_TYPE, def::higher} | Lexical<>{EBNF_GRAMMAR_TYPE, def::lower};
static auto digit  = Lexical<>{EBNF_GRAMMAR_TYPE, def::digit};
static auto number = +grammar::ebnf::make_single_gram<>('-') >> *digit;
static auto string =  grammar::ebnf::make_single_gram<>('\"') >> *letter >> grammar::ebnf::make_single_gram<>('\"');

/* functions */
static Lexical<> match(std::string value){
  auto pull = [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
    return result == grammar::Inv? grammar::Unf :result;
  };
  auto push = [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
    return result == grammar::Inv? grammar::Unf :result;
  };
  return Lexical<>{EBNF_GRAMMAR_TYPE, std::make_shared<grammar::Sequence<>>(rvalue(grammar::ebnf::make_concat_gram(value)), pull, push)};
}

static Lexical<> UNUSED_FUNCTION(match)(std::vector<std::string> values){
  Lexical<> result;

  for (auto& value: values)
    if (result)
      result = result | match(value);
    else result = match(value);

  return result;
}

static Lexical<> UNUSED_FUNCTION(str_)(std::string sample) {
  auto decide = [](std::size_t, std::size_t, Status status, bool) -> Status{
    return status;
  };
  auto result = std::make_shared<grammar::Parallel<>>(decide);
  auto gram   = std::vector<Lexical<>>{};


  gram.push_back(grammar::ebnf::make_single_gram(sample[0]));
  for (int i = 1; i < cast_(i, sample.size()); ++i)
    gram = gram >> grammar::ebnf::make_single_gram<>(sample[i]);

  return Lexical<>{EBNF_GRAMMAR_TYPE, result};
}
} // namespace ebnf
} // namespace pattern
} // namespace grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_EBNF_HPP_