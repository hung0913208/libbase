#if !__APPLE__
#include "ebnf.hpp"

namespace base{
namespace grammar{
namespace ebnf{
std::vector<char> regex(std::string pattern){
  std::vector<char> result{};

  for (auto i = 0; i < cast_(i, pattern.size()); ++i){
    if (pattern[i] == '-' && 0 < i && i < cast_(i, pattern.size() - 1)){ 
      if ('a' <= pattern[i - 1] && pattern[i - 1] <= 'z' && 'a' <= pattern[i + 1] && pattern[i + 1] <= 'z'){
        for(auto beg = pattern[i - 1] + 1; beg <= pattern[i + 1]; ++beg)
          result.push_back(beg);
        i++;
        continue;
      }
      if ('A' <= pattern[i - 1] && pattern[i - 1] <= 'Z' && 'A' <= pattern[i + 1] && pattern[i + 1] <= 'Z'){
        for(auto beg = pattern[i - 1] + 1; beg <= pattern[i + 1]; ++beg)
          result.push_back(beg);
        i++;
        continue;
      }
      if ('0' <= pattern[i - 1] && pattern[i - 1] <= '9' && '0' <= pattern[i + 1] && pattern[i + 1] <= '9'){
        for(auto beg = pattern[i - 1] + 1; beg <= pattern[i + 1]; ++beg)
          result.push_back(beg);
        i++;
        continue;
      }
    }
    result.push_back(pattern[i]);
  }
  return result;
}

} // namespace ebnf

namespace pattern{
namespace ebnf{

} // namespace ebnf
} // namespace pattern
} // namespace grammar
} // namespace base
#endif