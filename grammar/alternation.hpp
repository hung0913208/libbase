#ifndef LIBBASE_GRAMMAR_ALTERNATION_HPP_
#define LIBBASE_GRAMMAR_ALTERNATION_HPP_
#include <vector>
#include <memory>

#include "sequence.hpp"
#include "parallel.hpp"
#include "../utils.hpp"

namespace gram = base::grammar;
namespace base{
namespace grammar{
namespace exception{
template<typename ByteT=char>
Lexical<ByteT> alternation(Lexical<ByteT> left, Lexical<ByteT> right){
  auto left_ = left.template turnTo<gram::Parallel<ByteT>>();
  auto right_ = right.template turnTo<gram::Parallel<ByteT>>();

  if (left_ && right_){
    left_->merge(right_);
    return left;
  }
  return Lexical<ByteT>{};
}
} // namespace alternation
} // namespace grammar
} // namespace base

template<typename ByteT>
struct Alternation: gram::Grammar<ByteT>{
 public:
  explicit Alternation(Alternation<ByteT>* src):
    gram::Grammar<ByteT>{
      [this](base::Pipe<ByteT>& UNUSED(pipe), std::size_t idx, ByteT in, bool trial) -> gram::Status{
        auto lret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto rret = _group[i]->pull(idx, in, trial);

          lret = (rret == gram::Fal || rret == gram::Inv)? lret: rret;
          if (lret == gram::Don)
            return lret;
        }
        return lret;
      },
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT& out, bool trial) -> gram::Status{
        auto ret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto ret = _group[i]->pull(idx, out, trial);

          if (ret == gram::Don)
            return ret;
        }
        return ret;
      }
    } { 
    for (auto item: src->_group) 
      _group.push_back(Lexical<ByteT>{item.type(), std::shared_ptr<gram::Grammar<ByteT>>{item->clone()}});
  }

  explicit Alternation(Lexical<ByteT>&& left, Lexical<ByteT>&& right):
    gram::Grammar<ByteT>{
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT in, bool trial) -> gram::Status{
        auto lret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto rret = _group[i]->pull(idx, in, trial);

          lret = (rret == gram::Fal || rret == gram::Inv)? lret: rret;

          if (lret == gram::Don)
            return lret;
        }
        return lret;
      },
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT& out, bool trial) -> gram::Status{
        auto ret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto ret = _group[i]->pull(idx, out, trial);

          if (ret == gram::Don)
            return ret;
        }
        return ret;
      }
    }
  {
    append(left).append(right);
  }
  
  explicit Alternation(Lexical<ByteT>& left, Lexical<ByteT>&& right):
    gram::Grammar<ByteT>{
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT in, bool trial) -> gram::Status{
        auto lret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto rret = _group[i]->pull(idx, in, trial);

          lret = (rret == gram::Fal || rret == gram::Inv)? lret: rret;

          if (lret == gram::Don)
            return lret;
        }
        return lret;
      },
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT& out, bool trial) -> gram::Status{
        auto ret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto ret = _group[i]->pull(idx, out, trial);

          if (ret == gram::Don)
            return ret;
        }
        return ret;
      }
    }
  {
    append(left).append(right);
  }

  explicit Alternation(Lexical<ByteT>&& left, Lexical<ByteT>& right):
    gram::Grammar<ByteT>{
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT in, bool trial) -> gram::Status{
        auto lret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto rret = _group[i]->pull(idx, in, trial);

          lret = (rret == gram::Fal || rret == gram::Inv)? lret: rret;

          if (lret == gram::Don)
            return lret;
        }
        return lret;
      },
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT& out, bool trial) -> gram::Status{
        auto ret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto ret = _group[i]->pull(idx, out, trial);

          if (ret == gram::Don)
            return ret;
        }
        return ret;
      }
    }
  {
    append(left).append(right);
  }

  explicit Alternation(Lexical<ByteT>& left, Lexical<ByteT>& right):
    gram::Grammar<ByteT>{
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT in, bool trial) -> gram::Status{
        auto lret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto rret = _group[i]->pull(idx, in, trial);

          lret = (rret == gram::Fal || rret == gram::Inv)? lret: rret;

          if (lret == gram::Don)
            return lret;
        }
        return lret;
      },
      [this](base::Pipe<ByteT>&, std::size_t idx, ByteT& out, bool trial) -> gram::Status{
        auto ret = gram::Fal;

        for (auto i = 0; i < cast_(i, _group.size()); ++i){
          auto ret = _group[i]->pull(idx, out, trial);

          if (ret == gram::Don)
            return ret;
        }
        return ret;
      }
    }
  {
    append(left).append(right);
  }

 public:
  void append(std::vector<Lexical<ByteT>>&& group){
    for (auto i = 0; i < cast_(i, group.size()); ++i) append(group[i]);
  }

  void append(std::vector<Lexical<ByteT>>& group){
    for (auto i = 0; i < cast_(i, group.size()); ++i) append(group[i]);
  }

  Alternation<ByteT>& append(Lexical<ByteT>&& next){
    _group.push_back(next);
    return *this;
  }

  Alternation<ByteT>& append(Lexical<ByteT>& next){
    _group.push_back(next);
    return *this;
  }

  gram::Grammar<ByteT>* clone_(){ return new Alternation<ByteT>(this); }

 protected:
  base::Error reset_(){
    auto error = NoError;

    for (auto& mem: _group)
      if ((error = mem->reset()))
        return error;
    return NoError;
  }

 private:
  std::vector<Lexical<ByteT>> _group;
};

template <typename ByteT> 
Lexical<ByteT> operator|(Lexical<ByteT> &&left, Lexical<ByteT> &&right){
  if (left.type() != right.type()) throw NoSupport;
  else{
    auto result = base::grammar::exception::alternation<ByteT>(left, right);

    if (result) return result;
    else{
      auto orgram = std::dynamic_pointer_cast<Alternation<ByteT>>(left.grammar());

      if (!orgram)
        return Lexical<ByteT>{left.type(),
          std::make_shared<Alternation<ByteT>>(left, right)};
      else{
        orgram->append(right);
        return left;
      } 
    }
  }
}

template <typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, Lexical<ByteT> &&right){
  return right | base::grammar::Sequence<ByteT>((left), 
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT>
Lexical<ByteT> operator|(Lexical<ByteT> &&left, std::vector<Lexical<ByteT>> &&right){
  return right | left;
}

template<typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, std::vector<Lexical<ByteT>> &&right){
  return left | base::grammar::Sequence<ByteT>(right, 
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template <typename ByteT> 
Lexical<ByteT> operator|(Lexical<ByteT> &&left, Lexical<ByteT> &right){
  if (left.type() != right.type()) throw NoSupport;
  else{
    auto orgram = std::dynamic_pointer_cast<Alternation<ByteT>>(left.grammar());

    if (!orgram)
      return Lexical<ByteT>{left.type(), std::make_shared<Alternation<ByteT>>(left, right)};
    else{
      orgram->append(right);
      return left;
    } 
  }
}

template <typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, Lexical<ByteT> &right){
  return right | base::grammar::Sequence<ByteT>((left), 
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT>
Lexical<ByteT> operator|(Lexical<ByteT> &&left, std::vector<Lexical<ByteT>> &right){
  return right | left;
}

template<typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, std::vector<Lexical<ByteT>> &right){
  return left | base::grammar::Sequence<ByteT>(right, 
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}


template <typename ByteT> 
Lexical<ByteT> operator|(Lexical<ByteT> &left, Lexical<ByteT> &&right){
  if (left.type() != right.type()) throw NoSupport;
  else{
    auto result = base::grammar::exception::alternation<ByteT>(left, right);

    if (result) return result;
    else{
      auto orgram = std::dynamic_pointer_cast<Alternation<ByteT>>(left.grammar());

      if (!orgram)
        return Lexical<ByteT>{left.type(),
          std::make_shared<Alternation<ByteT>>(left, right)};
      else{
        orgram->append(right);
        return left;
      } 
    }
  }
}

template <typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, Lexical<ByteT> &&right){
  return right | base::grammar::Sequence<ByteT>((left), 
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT>
Lexical<ByteT> operator|(Lexical<ByteT> &left, std::vector<Lexical<ByteT>> &&right){
  return right | left;
}

template<typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, std::vector<Lexical<ByteT>> &&right){
  return left | base::grammar::Sequence<ByteT>(right, 
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template <typename ByteT> 
Lexical<ByteT> operator|(Lexical<ByteT> &left, Lexical<ByteT> &right){  
  if (left.type() != right.type()) throw NoSupport;
    else{
    auto result = base::grammar::exception::alternation<ByteT>(left, right);

    if (result) return result;
    else{
      auto orgram = std::dynamic_pointer_cast<Alternation<ByteT>>(left.grammar());

      if (!orgram)
        return Lexical<ByteT>{left.type(),
          std::make_shared<Alternation<ByteT>>(left, right)};
      else{
        orgram->append(right);
        return left;
      } 
    }
  }
}

template <typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, Lexical<ByteT> &right){
  return right | base::grammar::Sequence<ByteT>((left), 
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [left](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT>
Lexical<ByteT> operator|(Lexical<ByteT> &left, std::vector<Lexical<ByteT>> &right){
  return right | left;
}

template<typename ByteT>
Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, std::vector<Lexical<ByteT>> &right){
  return left | base::grammar::Sequence<ByteT>(right, 
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [right](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}
#endif // LIBBASE_GRAMMAR_ALTERNATION_HPP_
