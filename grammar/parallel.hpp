#if !defined(LIBBASE_GRAMMAR_PARALLEL_HPP_) && __cplusplus
#define LIBBASE_GRAMMAR_PARALLEL_HPP_

#include "pipe.hpp"

#include <functional>
#include <vector>

namespace base{
namespace grammar{
template <typename ByteT=char> struct Parallel: Grammar<ByteT>{
 public:
  using Decide = std::function<Status(std::size_t, std::size_t, Status, bool)>;

 public:
  explicit Parallel(Decide decide): Grammar<ByteT>{
    [=](Pipe<ByteT>&, std::size_t index, ByteT in, bool trial) -> Status{
      auto result = Inv;
      auto dev    = 0;

      auto condition = [&](Status node, Status& overall) -> bool{
        if (dev < cast_(dev, _deserializes.size())){
          if (dev == 0) return true;

          overall = _pull(dev, index, node, trial);
          return overall != Fal && overall != Don;
        }
        return false;
      };
      auto task = [&]() -> Status{ return (_deserializes[dev] << in).status(); };
      auto error = NoError;

      if ((error = this->_pipe->scatter(result, condition, task)))
        throw error;
      else if ((error = this->_pipe->waiting()))
        throw error;

      return dev == 0? Fal: result;
    },
    [=](Pipe<ByteT>&, std::size_t index, ByteT& out, bool trial) -> Status{
      auto result = Inv;
      auto dev = 0;

      auto condition = [&](Status node, Status& overall) -> bool{
        if (dev < cast_(dev, _deserializes.size())){
          if (dev == 0) return true;

          overall = _push(dev, index, node, trial);
          return overall != Fal && overall != Don;
        }
        return false;
      };
      auto task = [&]() -> Status{ return (_serializes[dev] >> out).status(); };
      auto error = NoError;

      if ((error = this->_pipe->scatter(result, condition, task)))
        throw error;
      else if ((error = this->_pipe->waiting()))
        throw error;

      return dev == 0? Fal: result;
    }
  }{
    _pull = decide;
    _push = decide;
  }

  explicit Parallel(Decide pull, Decide push): Grammar<ByteT>{
    [=](Pipe<ByteT>&, std::size_t index, ByteT in, bool trial) -> Status{
      auto result = Fal;

      for (auto dev = 0; dev < cast_(dev, _deserializes.size()); ++dev){
        if (_deserializes[dev].finished())
          continue;
        result = _pull(dev, index, (_deserializes[dev] << in).status(), trial);
        if (result == Fal || result == Don)
          return result;
        else result = Unf;
      }
      return result;
    },
    [=](Pipe<ByteT>&, std::size_t index, ByteT& out, bool trial) -> Status{
      auto result = Fal;

      for (auto dev = 0; dev < cast_(dev, _serializes.size()); ++dev){
        if (_serializes[dev].finished())
          continue;
        result = _push(dev, index, (_serializes[dev] >> out).status(), trial);
        if (result == Fal || result == Don)
          return result;
        else result = Unf;
      }
      return result;
    }
  }, _pull{pull}, _push{push} {}

 public:
  Parallel<ByteT>& merge(std::shared_ptr<Parallel<ByteT>>& src){
    _deserializes.insert(_deserializes.end(), src->_deserializes.begin(), src->_deserializes.end());
    _serializes.insert(_serializes.end(), src->_serializes.begin(), src->_serializes.end());
    return *this;
  }

  Parallel<ByteT>& merge(std::shared_ptr<Parallel<ByteT>>&& src){
    _deserializes.insert(_deserializes.end(), src->_deserializes.begin(), src->_deserializes.end());
    _serializes.insert(_serializes.end(), src->_serializes.begin(), src->_serializes.end());
    return *this;
  }

  Parallel<ByteT>& merge(Lexical<ByteT>& lexical){
    _deserializes.push_back(lexical);
    _serializes.push_back(lexical);
    return *this;
  }

  Parallel<ByteT>& merge(Lexical<ByteT>&& lexical){
    _deserializes.push_back(lexical);
    _serializes.push_back(lexical);
    return *this;
  }

  Parallel<ByteT>& merge(std::vector<Lexical<ByteT>>& lexical){
    _deserializes.append(lexical);
    _serializes.append(lexical);
    return *this;
  }

  Parallel<ByteT>& merge(std::vector<Lexical<ByteT>>&& lexical){
    _deserializes.append(ravlue(lexical));
    _serializes.append(ravlue(lexical));
    return *this;
  }

 private:
  std::vector<Deserialize<ByteT>> _deserializes;
  std::vector<Serialize<ByteT>>   _serializes;

 private:
  Decide _pull, _push;
};
} // namespace grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_PARALLEL_HPP_