#ifndef LIBBASE_GRAMMAR_CONTENT_HPP_
#define LIBBASE_GRAMMAR_CONTENT_HPP_

#include "pipe.hpp"

#include <functional>
#include <vector>

template<typename ByteT> struct Lexical;

namespace base{
namespace grammar{
template<typename ByteT=char>
struct Content: Grammar<ByteT>{ 
 public:
  using Pull = std::function<base::grammar::Status(Content<ByteT>&, std::size_t, ByteT,  bool)>;
  using Push = std::function<base::grammar::Status(Content<ByteT>&, std::size_t, ByteT&, bool)>;

 public:
  explicit Content(std::vector<ByteT> content, Pull in_, Push out_):
    Grammar<ByteT>{
      [&](Pipe<ByteT>&, std::size_t step, ByteT in, bool trial)  -> Status{ 
        return _in(*this, step, in, trial);
      }, 
      [&](Pipe<ByteT>&, std::size_t step, ByteT& out, bool trial) -> Status{
        return _out(*this, step, out, trial);
      }
    }, _content{content}, _in{in_}, _out{out_} {}

  explicit Content(ByteT* content, std::size_t count, Pull in_, Push out_):
    Grammar<ByteT>{
      [&](Pipe<ByteT>&, std::size_t step, ByteT in, bool trial)  -> Status{ 
        return _in(*this, step, in, trial);
      }, 
      [&](Pipe<ByteT>&, std::size_t step, ByteT& out, bool trial) -> Status{
        return _out(*this, step, out, trial);
      }
    }, _in{in_}, _out{out_} {
      for (auto i = 0; i < cast_(i, count); ++i)
        _content.push_back(content[i]);
    }

  explicit Content(ByteT sample, Pull in_, Push out_):
    Grammar<ByteT>{
      [&](Pipe<ByteT>&, std::size_t step, ByteT in, bool trial)  -> Status{ 
        return _in(*this, step, in, trial);
      }, 
      [&](Pipe<ByteT>&, std::size_t step, ByteT& out, bool trial) -> Status{
        return _out(*this, step, out, trial);
      }
    }, _in{in_}, _out{out_} {
      _content.push_back(sample);
    }

 public:
  std::vector<ByteT>& content(){ return _content; }

 private:
  std::vector<ByteT> _content;

 private:
  Pull _in;
  Push _out;  
};
} // grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_CONTENT_HPP_