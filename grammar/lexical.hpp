#if !defined(LIBBASE_GRAMMAR_LEXICAL_HPP_) && __cplusplus
#define LIBBASE_GRAMMAR_LEXICAL_HPP_
#include "../trace.hpp"

#include "grammar.hpp"
#include "property.hpp"

#include <vector>

template<typename ByteT=char>
struct Lexical{
 private:
  using PProperty = std::shared_ptr<base::grammar::Property>;

 public:
  using Grammar  = base::grammar::Grammar<ByteT>;
  using Property = base::grammar::Property;

  using Pull = std::function<base::grammar::Status(base::Pipe<ByteT>&, std::size_t, ByteT, bool)>;
  using Push = std::function<base::grammar::Status(base::Pipe<ByteT>&, std::size_t, ByteT&, bool)>;
  using Rset = std::function<base::Error()>;

  template<typename Type>
  using Attribute = std::shared_ptr<base::grammar::Attribute<Type, ByteT>>;

 public:
  explicit Lexical(std::size_t type, std::shared_ptr<Grammar>&& grammar)
    : _grammar{grammar}, _type{type} {}

  explicit Lexical(std::size_t type,
                   std::shared_ptr<Grammar>&& grammar,
                   std::map<std::string, std::string>&& properties)
    : _grammar{grammar}, _type{type} {
      for (auto item: properties) set(std::get<0>(item), std::get<1>(item));
    }

  explicit Lexical(std::size_t type, std::shared_ptr<Grammar>& grammar)
    : _grammar{grammar}, _type{type} {}

  explicit Lexical(std::size_t type,
                   std::shared_ptr<Grammar>& grammar,
                   std::map<std::string, std::string>&& properties)
    : _grammar{grammar}, _type{type} {
      for (auto item: properties) set(std::get<0>(item), std::get<1>(item));
    }

 public:
  Lexical(const Lexical& lexical): 
    _grammar{lexical._grammar}, _properties{lexical._properties}, 
    _type{lexical._type} {}

  Lexical(const Lexical&& lexical): 
    _grammar{lexical._grammar}, _properties{lexical._properties},
    _type{lexical._type} {}

 public:
   Lexical(){ }
  ~Lexical(){ }

 public:
  Lexical& set(std::string name, std::string attribute){
    _properties[name] = std::make_shared<base::grammar::Property>(name, attribute);
    return *this;
  }

  PProperty& get(std::string name){ return _properties[name]; }

  std::shared_ptr<Grammar> grammar(){
    /* @BUGS: [Trace] khong the khoi tao duoc mot tracer */
    _grammar->init(_properties);

    for (auto level = 0, max = 0; level <= max; ++level)
      for (auto item : _properties){
        auto curr = Property::level(std::get<0>(item));

        if (curr == -1)
          continue;
        else if (curr == level){
          if (std::get<1>(item)->handler)
            PAssert(std::get<1>(item)->handler(std::get<1>(item).get(), _grammar) == nullptr);
        } else max = curr > max ? curr : max;
      }

    for (auto item : _properties){
      auto curr = Property::level(std::get<0>(item));

      if (curr == -1)
        if (std::get<1>(item)->handler){
          auto result = std::get<1>(item)->handler(std::get<1>(item).get(), _grammar);

          if (dynamic_cast<Grammar*>(result))
            return std::shared_ptr<Grammar> {dynamic_cast<Grammar*>(result)};
        }
    }
    return _grammar;
  }

  std::size_t type(){ return _type; }

 public:
  template<typename Type> std::shared_ptr<Type> turnTo(){
    return std::dynamic_pointer_cast<Type>(_grammar);
  }

 public:
  std::shared_ptr<Grammar> operator->() const { 
    if (_grammar) return _grammar;
    throw OutOfRange;
  }

  Lexical<ByteT>& operator=(const Lexical<ByteT>& src){
    _grammar = src._grammar;
    _properties = src._properties;
    _type = src._type;
    return *this;
  }

  Lexical<ByteT>& operator=(Lexical<ByteT>&& src){
    _grammar = src._grammar;
    _properties = src._properties;
    _type = src._type;
    return *this;
  }

  template<typename Type> 
  Lexical<ByteT>& operator[](Type& variable){
    _grammar->reinit(_properties);

    for (auto item: _properties){
      auto curr = Property::level(std::get<0>(item));

      if (curr == -1) continue;
      else if (std::get<1>(item)->handler)
        PAssert(std::get<1>(item)->handler(std::get<1>(item).get(), _grammar) == nullptr);
    }

    (*_grammar)[variable].uninit();
    return *this;
  }

  template<typename Type> 
  Lexical<ByteT>& operator()(std::function<Type&()> bridge){
    _grammar->reinit(_properties);

    for (auto item: _properties){
      auto curr = Property::level(std::get<0>(item));

      if (curr == -1) continue;
      else if (std::get<1>(item)->handler)
        PAssert(std::get<1>(item)->handler(std::get<1>(item).get(), _grammar) == nullptr);
    }

    (*_grammar)(bridge).uninit();
    return *this;
  }

  template<typename Type> 
  Lexical<ByteT>& operator()(Attribute<Type>&& attribute){
    _grammar->reinit(_properties);

    for (auto item: _properties){
      auto curr = Property::level(std::get<0>(item));

      if (curr == -1) continue;
      else if (std::get<1>(item)->handler)
        PAssert(std::get<1>(item)->handler(std::get<1>(item).get(), _grammar) == nullptr);
    }

    (*_grammar)(attribute).uninit();
    return *this;
  }

  template<typename Type> 
  Lexical<ByteT>& operator()(Attribute<Type>&  attribute){
    _grammar->reinit(_properties);

    for (auto item: _properties){
      auto curr = Property::level(std::get<0>(item));

      if (curr == -1) continue;
      else if (std::get<1>(item)->handler)
        PAssert(std::get<1>(item)->handler(std::get<1>(item).get(), _grammar) == nullptr);
    }

    (*_grammar)(attribute).uninit();
    return *this;
  }

  bool operator!=(std::size_t type){ return _type != type; }

  operator bool(){ return _grammar != nullptr; }

 public:
  static Lexical<ByteT> make_shared(std::size_t type, base::grammar::Template<ByteT>&& executor){
    return Lexical<ByteT>{type, std::make_shared<Grammar>(executor.pulling(), executor.pushing(), executor.reseting())};
  }

  static Lexical<ByteT> make_shared(std::size_t type, base::grammar::Template<ByteT>&  executor){
    return Lexical<ByteT>{type, std::make_shared<Grammar>(executor.pulling(), executor.pushing(), executor.reseting())};
  }

  static Lexical<ByteT> make_shared(std::size_t type, Pull pull, Push push, Rset reset = nullptr){
    return Lexical<ByteT>{type, std::make_shared<Grammar>(pull, push, reset)};
  }

  static std::vector<Lexical<ByteT>> make_shared(std::vector<Lexical<ByteT>>&& source){
    std::vector<Lexical<ByteT>> result;

    for (auto& item: source)
      result.push_back(Lexical<ByteT>::make_shared(item));
    return result;
  }

  static std::vector<Lexical<ByteT>> make_shared(std::vector<Lexical<ByteT>>&  source){
    std::vector<Lexical<ByteT>> result;

    for (auto& item: source)
      result.push_back(Lexical<ByteT>::make_shared(item));
    return result;
  }

  static Lexical<ByteT> make_shared(Lexical<ByteT>&& source){
    auto apply = source.grammar();

    if (apply != source._grammar)
      return Lexical<ByteT>{source._type, apply};
    return Lexical<ByteT>{source._type, std::shared_ptr<Grammar>{apply->clone()}};
  }

  static Lexical<ByteT> make_shared(Lexical<ByteT>& source){
    auto apply = source.grammar();

    if (apply != source._grammar)
      return Lexical<ByteT>{source._type, apply};
    return Lexical<ByteT>{source._type, std::shared_ptr<Grammar>{apply->clone()}};
  }

 private:
  std::shared_ptr<Grammar>  _grammar;
  std::map<std::string, PProperty> _properties;
  std::size_t _type;
};
#endif // LIBBASE_GRAMMAR_LEXICAL_HPP_