#if !defined(LIBBASE_PIPE_HPP_) && __cplusplus
#define LIBBASE_PIPE_HPP_

#include <functional>
#include <memory>
#include <vector>

#include "grammar.hpp"

#include "../logcat.hpp"
#include "../trace.hpp"
#include "../utils.hpp"

namespace base{
struct Stream;

template <typename ByteT=char>
struct Deserialize: Pipe<ByteT>{
 private:
  using Step = std::shared_ptr<grammar::Grammar<ByteT>>;

 public:
  explicit Deserialize(std::vector<Lexical<ByteT>>&& lexicals):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexicals));
    if (error) throw error;
  }

  explicit Deserialize(std::vector<Lexical<ByteT>>& lexicals):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(lexicals);
    if (error) throw error;
  }

  explicit Deserialize(std::vector<grammar::Grammar<ByteT>>&& lexical): 
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexical));
    if (error) throw error;
  }

  explicit Deserialize(std::vector<grammar::Grammar<ByteT>>& lexical): 
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(lexical);
    if (error) throw error;
  }

  explicit Deserialize(std::vector<Step>& steps):
      Pipe<ByteT>{}, _status{grammar::Inv}, _steps{steps}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(steps);
    if (error) throw error;
  }

  explicit Deserialize(grammar::Grammar<ByteT>&& grammar):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(grammar));
    if (error) throw error;
  }

  explicit Deserialize(grammar::Grammar<ByteT>& grammar):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(grammar);
    if (error) throw error;
  }

  explicit Deserialize(Lexical<ByteT>&& lexical):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexical));
    if (error) throw error;
  }

  explicit Deserialize(Lexical<ByteT>& lexical):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexical));
    if (error) throw error;
  }

 public:
  base::Error append(std::vector<Lexical<ByteT>>&& lexicals){
    auto type = std::string::npos;

    for (auto& lexical: lexicals){
      auto splited = lexical.grammar()->split();

      if (type != std::string::npos && lexical != std::string::npos && lexical != type)
        return BadLogic;
      else type = lexical.type();

      if (splited.size() > 0)
        for (auto& step: splited) _steps.push_back(step.grammar());
      else
        _steps.push_back(lexical.grammar());
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<Lexical<ByteT>>& lexicals){
    auto type = std::string::npos;

    for (auto& lexical: lexicals){
      auto splited = lexical.grammar()->split();

      if (type != std::string::npos && lexical != std::string::npos && lexical != type)
        return BadLogic;
      else type = lexical.type();

      if (splited.size() > 0)
        for (auto step: splited) _steps.push_back(step.grammar());
      else
        _steps.push_back(lexical.grammar());
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<grammar::Grammar<ByteT>>&& lexical){
    for (auto& gram: lexical){
      auto splited = gram.split();

      if (splited.size() > 0)
        for (auto& step: splited) _steps.push_back(step.grammar());
      else _steps.push_back(&gram);
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<grammar::Grammar<ByteT>>& lexical){
    for (auto& gram: lexical){
      auto splited = gram.split();

      if (splited.size() > 0)
        for (auto& step: splited) _steps.push_back(step.grammar());
      else _steps.push_back(&gram);
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<Step>& steps){
    for (auto step: steps) step->pipe(this);
    return NoError;
  }

  base::Error append(grammar::Grammar<ByteT>&& grammar){
    auto splited = grammar->split();

    if (splited.size() > 0)
      for (auto& step: splited) _steps.push_back(step.grammar());
    else _steps.push_back(grammar);

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(grammar::Grammar<ByteT>& grammar){
    auto splited = grammar->split();

    if (splited.size() > 0)
      for (auto& step: splited) _steps.push_back(step.grammar());
    else _steps.push_back(grammar);

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(Lexical<ByteT>&& lexical){
    auto splited = lexical.grammar()->split();

    if (splited.size() > 0)
      for (auto& step: splited) _steps.push_back(step.grammar());
    else
      _steps.push_back(lexical.grammar());
    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(Lexical<ByteT>& lexical){
    auto splited = lexical.grammar()->split();

    if (splited.size() > 0)
      for (auto& step: splited)
        _steps.push_back(step.grammar());
    else
      _steps.push_back(lexical.grammar());

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error write(ByteT value){ 
    if ((*this) == -1) 
      return OutOfRange;

    *this << value;
    return NoError;
  }

  base::Error reset(){
    auto error = NoError;
    if (_step_i == std::string::npos)
      return DoNothing;

    for (auto& step: _steps) 
      error = step->reset();
    if (!error){
      _step_i = std::string::npos;
      _status = grammar::Inv;
      _index  = 0;
    }
    return error;
  }

 public:
  Deserialize<ByteT>& operator<<(ByteT byte){
    process(byte);
    return *this;
  }

  Deserialize<ByteT>& operator<<(std::vector<ByteT>&& array){
    for (auto i = 0; i < cast_(i, array.size()); ++i)
      if (((*this) << array[i]) == grammar::Fal || status() == grammar::Don)
        break;
    return *this;
  }

  Deserialize<ByteT>& operator<<(std::basic_string<ByteT>&& string){
    for (auto i = 0; i < cast_(i, string.size()); ++i)
      if (((*this) << string[i]) == grammar::Fal || status() == grammar::Don)
        break;
    return *this;
  }

  Deserialize<ByteT>& operator<<(std::vector<ByteT>& array){
    for (auto i = 0; i < cast_(i, array.size()); ++i)
      if (((*this) << array[i]) == grammar::Fal || status() == grammar::Don)
        break;
    return *this;
  }

  Deserialize<ByteT>& operator<<(std::basic_string<ByteT>& string){
    for (auto i = 0; i < cast_(i, string.size()); ++i)
      if (((*this) << string[i]) == grammar::Fal || status() == grammar::Don)
        break;
    return *this;
  }

  operator int(){ 
    if (_status == grammar::Inv){
      if (_step_i == std::string::npos) return grammar::Unf;
      else if (_index == 0 && _status == grammar::Inv)
        return grammar::Unf;
    }
    return _status;
  }

 public:
  grammar::Status status(){ return _status; }

  std::size_t step(){ return _step_i; }

  bool finished(){ return _step_i == _steps.size(); }

 public:
  std::shared_ptr<grammar::Context> prev(std::size_t step) final{
    if (_step_i < step) throw OutOfRange;
    return _steps[_step_i - step]->shared_from_this();
  }

  std::shared_ptr<grammar::Context> next(std::size_t step) final{
    if (_step_i + step >= _steps.size())
      throw OutOfRange;
    return _steps[_step_i + step]->shared_from_this();
  }

 public:
  base::Error scatter(grammar::Status& result,
                      std::function<bool(grammar::Status, grammar::Status&)>&& condition,
                      std::function<grammar::Status()>&& task) override{
    while (condition(task(), result)){}
    return NoError;
  }

  base::Error waiting() override{ return NoError; }

 protected:
  void process(ByteT& byte) override{
    if (_step_i == std::string::npos && _steps.size() > 0)
      _step_i = 0;
    else if (_step_i >= _steps.size())
      return;

    if (_step_i != std::string::npos) {
      auto result = grammar::Fal;

      /*   Voi truong hop state == Pal, day la trang thai yeu cua Step va de
       * dang bi chuyen qua step moi neu ket qua do step moi tot hon step hien
       * tai. Vi vay ta phai kiem tra truoc khi thuc hien step hien tai voi
       * truong hop nay
       */
      if (_status == grammar::Pal && _step_i + 1 < _steps.size())
        result = _steps[_step_i + 1]->pull(0, byte, true);

      if (_status == grammar::Pal && result == grammar::Unf) {
        _step_i += 1;
        _index = 0;
      } else if (_status == grammar::Pal && result == grammar::Don) {
        _index = 0;

        if (++_step_i + 1 == _steps.size())
          _step_i = loop ? 0 : _step_i;
        else
          result = grammar::Inv;
      } else {
        result = _steps[_step_i]->pull(_index, byte, false);

        if (result == grammar::Don) {
          /*   step tu chu dong thoat khoi qua trinh nap du lieu. Khi dieu nay
           * xay ra, ta khong chac he thong da okey hay do chua co quyet dinh ro
           * rang tu step tiep theo. Day la tinh trang nhap nhang. Neu khong co
           * step tiep theo thi hien nhien ket qua la Don.
           */
          _index = 0;

          if (++_step_i == _steps.size())
            _step_i = loop ? 0 : _step_i;
          else
            result = grammar::Inv;
        } else if (result == grammar::Fal) 
          /* Fal chu dong, tu huy ngay lap tuc */
          _step_i = loop ? std::string::npos : _steps.size();
        else if (result == grammar::Pal ||
                 (result == grammar::Inv && _step_i + 1 < _steps.size())) {
          /* tinh trang nhap nhang, step khong chac day la ki tu dung hay sai.
           * Tat ca phu thuoc va step tiep theo
           * - Unf: Tiep tuc follow step nay de khang dinh chac chan hay khong
           * chac chan ket qua trong tuong lai
           * - Pal: Follow nhung san sang tu bo step nay neu step ke tiep tra ve
           * Unf hoac Don
           * - Fal or Inv: huy hoan toan, khong co khai niem nhap nhang trong
           * day,
           * - Don: Okey, luu lai ki tu nay va nhay qua step tiep theo, roi vao
           * trang thai nhap nhang.
           */
          auto dil = result == grammar::Inv;

          result = _steps[_step_i + 1]->pull(0, byte, true);

          if (result == grammar::Unf || (dil && result == grammar::Pal)) {
            _step_i += 1;
            _index = 0;
          } else if (result == grammar::Don) {
            _index = 0;

            if (++_step_i + 1 == _steps.size())
              _step_i = loop ? 0 : _step_i + 1;
            else
              result = grammar::Inv;
          } else
            result = dil ? grammar::Fal : grammar::Pal;
        } else if (result != grammar::Unf)
          result = grammar::Fal;
      }
      if (result < 0)
        ++_index;
      _status = result;
    } else
      _status = grammar::Fal;
  }

 private:
  std::vector<Step> _steps;
  grammar::Status   _status;
  std::size_t       _step_i, _index;

 public:
  bool loop;
};

template <typename ByteT = char>
struct Serialize: Pipe<ByteT>{
 private:
  using Step = std::shared_ptr<grammar::Grammar<ByteT>>;

 public:
  explicit Serialize(std::vector<Lexical<ByteT>>&& lexicals):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexicals));
    if (error) throw error;
  }

  explicit Serialize(std::vector<Lexical<ByteT>>& lexicals):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(lexicals);
    if (error) throw error;
  }

  explicit Serialize(std::vector<grammar::Grammar<ByteT>>&& lexical): 
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexical));
    if (error) throw error;
  }

  explicit Serialize(std::vector<grammar::Grammar<ByteT>>& lexical): 
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(lexical);
    if (error) throw error;
  }

  explicit Serialize(std::vector<Step>& steps):
      Pipe<ByteT>{}, _status{grammar::Inv}, _steps{steps}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(steps);
    if (error) throw error;
  }

  explicit Serialize(grammar::Grammar<ByteT>&& grammar):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(grammar));
    if (error) throw error;
  }

  explicit Serialize(grammar::Grammar<ByteT>& grammar):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(grammar);
    if (error) throw error;
  }

  explicit Serialize(Lexical<ByteT>&& lexical):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexical));
    if (error) throw error;
  }

  explicit Serialize(Lexical<ByteT>& lexical):
      Pipe<ByteT>{}, _status{grammar::Inv}, _step_i{std::string::npos}, _index{0}, loop{false} {
    auto error = append(rvalue(lexical));
    if (error) throw error;
  }

 public:
  base::Error append(std::vector<Lexical<ByteT>>&& lexicals){
    auto type = std::string::npos;

    for (auto& lexical: lexicals){
      auto splited = lexical.grammar()->split();

      if (type != std::string::npos && lexical != std::string::npos && lexical != type)
        return BadLogic;
      else type = lexical.type();

      if (splited.size() > 0)
        for (auto& step: splited) _steps.push_back(step.grammar());
      else
        _steps.push_back(lexical.grammar());
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<Lexical<ByteT>>& lexicals){
    auto type = std::string::npos;

    for (auto& lexical: lexicals){
      auto splited = lexical.grammar()->split();

      if (type != std::string::npos && lexical != std::string::npos && lexical != type)
        return BadLogic;
      else type = lexical.type();

      if (splited.size() > 0)
        for (auto step: splited) _steps.push_back(step.grammar());
      else
        _steps.push_back(lexical.grammar());
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<grammar::Grammar<ByteT>>&& lexical){
    for (auto& gram: lexical){
      auto splited = gram.split();

      if (splited.size() > 0)
        for (auto& step: splited) _steps.push_back(step.grammar());
      else _steps.push_back(&gram);
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<grammar::Grammar<ByteT>>& lexical){
    for (auto& gram: lexical){
      auto splited = gram.split();

      if (splited.size() > 0)
        for (auto& step: splited) _steps.push_back(step.grammar());
      else _steps.push_back(&gram);
    }

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(std::vector<Step>& steps){
    for (auto step: steps) step->pipe(this);
    return NoError;
  }

  base::Error append(grammar::Grammar<ByteT>&& grammar){
    auto splited = grammar->split();

    if (splited.size() > 0)
      for (auto& step: splited) _steps.push_back(step.grammar());
    else _steps.push_back(grammar);

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(grammar::Grammar<ByteT>& grammar){
    auto splited = grammar->split();

    if (splited.size() > 0)
      for (auto& step: splited) _steps.push_back(step.grammar());
    else _steps.push_back(grammar);

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(Lexical<ByteT>&& lexical){
    auto splited = lexical.grammar()->split();

    if (splited.size() > 0)
      for (auto& step: splited) _steps.push_back(step.grammar());
    else
      _steps.push_back(lexical.grammar());
    for (auto step: _steps) step->pipe(this);
    return NoError;
  }

  base::Error append(Lexical<ByteT>& lexical){
    auto splited = lexical.grammar()->split();

    if (splited.size() > 0)
      for (auto& step: splited)
        _steps.push_back(step.grammar());
    else
      _steps.push_back(lexical.grammar());

    for (auto step: _steps) step->pipe(this);
    return NoError;
  }
  ByteT read(){ 
    ByteT result;

    if ((*this) == -1) 
      throw OutOfRange;

    *this >> result;
    return result;
  }

  grammar::Status eof(){
    switch(_status){
    case grammar::Inv:
    case grammar::Unf:
      if (_steps.size() == _step_i + 1){
        _status = grammar::Don;
        break;
      }
    case grammar::Pal:
      _status = grammar::Fal;
      break;

    default:
      break;
    }

    _step_i = _steps.size();
    return _status;
  }

  base::Error reset(){
    auto error = NoError;

    if (_step_i == std::string::npos)
      return DoNothing;

    for (auto& step: _steps) 
      error = step->reset();

    if (error) return error;
    _step_i = std::string::npos;
    _status = grammar::Inv;
    _index  = 0;
    return NoError;
  }

 public:
  Serialize& operator>>(ByteT &byte){
    process(byte);
    return *this;
  }

  Serialize<ByteT>& operator>>(std::vector<ByteT>& array){
    ByteT c;

    while(((*this) >> c) != grammar::Fal && status() != grammar::Don)
      array.push_back(c);
    return *this;
  }

  Serialize<ByteT>& operator>>(std::basic_string<ByteT>&& string){
    ByteT c;

    while(((*this) >> c) != grammar::Fal && status() != grammar::Don)
      string += c;
    return *this;
  }

  operator int(){ 
    if (_status == grammar::Inv){
      if (_step_i == std::string::npos) return grammar::Unf;
      else if (_index == 0 && _status == grammar::Inv)
        return grammar::Unf;
    }
    return _status; 
  }

 public:
  grammar::Status status(){ return _status; }

  std::size_t step(){ return _step_i; }

  bool finished(){ return _step_i == _steps.size(); }

 public:
  std::shared_ptr<grammar::Context> prev(std::size_t step) final{
    if (_step_i < step) throw OutOfRange;
    return _steps[_step_i - step]->shared_from_this();
  }

  std::shared_ptr<grammar::Context> next(std::size_t step) final{
    if (_step_i + step >= _steps.size())
      throw OutOfRange;
  return _steps[_step_i + step]->shared_from_this();
  }

 public:
  base::Error scatter(grammar::Status& result,
                      std::function<bool(grammar::Status, grammar::Status&)>&& condition,
                      std::function<grammar::Status()>&& task) override{
    while (condition(task(), result)){}
    return NoError;
  }

  base::Error waiting() override{ return NoError; }

 protected:
  void process(ByteT& byte) override{
    if (_step_i == std::string::npos && _steps.size() > 0)
      _step_i = 0;
    else if (_step_i >= _steps.size())
      return;

    if (_step_i != std::string::npos){
      auto result = grammar::Fal;

      /*   Voi truong hop state == Pal, day la trang thai yeu cua Step va de dang bi chuyen qua step moi neu ket qua do step 
       * moi tot hon step hien tai. Vi vay ta phai kiem tra truoc khi thuc hien step hien tai voi truong hop nay
       */
      if (_status == grammar::Pal && _step_i + 1 < _steps.size())
        result = _steps[_step_i + 1]->push(0, byte, true);

      if (_status == grammar::Pal && result == grammar::Unf){
        _step_i += 1;
        _index   = 0;
      } else if (_status == grammar::Pal && result == grammar::Don){
        _index  = 0;

        if (++_step_i + 1 == _steps.size())
          _step_i = loop? 0: _step_i + 1;
        else result = grammar::Inv;
      } else{
        result = _steps[_step_i]->push(_index, byte, false);

        if (result == grammar::Don){ 
          /*   step tu chu dong thoat khoi qua trinh nap du lieu. Khi dieu nay xay ra, ta khong chac he thong da okey hay 
           * do chua co quyet dinh ro rang tu step tiep theo. Day la tinh trang nhap nhang. Neu khong co step tiep theo 
           * thi hien nhien ket qua la Don.
           */
          _index  = 0;

          if (++_step_i == _steps.size())
            _step_i = loop? 0: _step_i;
          else result = grammar::Inv;
        } else if (result == grammar::Fal) /* Fal chu dong, tu huy ngay lap tuc */
          _step_i = loop? std::string::npos: _steps.size();
        else if (result == grammar::Pal || (result == grammar::Inv && _step_i + 1 < _steps.size())){
          /* tinh trang nhap nhang, step khong chac day la ki tu dung hay sai. Tat ca phu thuoc va step tiep theo 
           * - Unf: Tiep tuc follow step nay de khang dinh chac chan hay khong chac chan ket qua trong tuong lai
           * - Pal: Follow nhung san sang tu bo step nay neu step ke tiep tra ve Unf hoac Don
           * - Fal or Inv: huy hoan toan, khong co khai niem nhap nhang trong day,
           * - Don: Okey, luu lai ki tu nay va nhay qua step tiep theo, roi vao trang thai nhap nhang.
           */
          auto dil = result == grammar::Inv;

          result = _steps[_step_i + 1]->push(0, byte, true);
          if (result == grammar::Unf || (dil && result == grammar::Pal)){
            _step_i += 1;
            _index   = 0;
          } else if (result == grammar::Don){
            _index  = 0;

          if (++_step_i + 1 == _steps.size())
            _step_i = loop? 0: _step_i + 1;
          else result = grammar::Inv;
          } else result = dil? grammar::Fal: grammar::Pal;
        } else if (result != grammar::Unf) result = grammar::Fal;
      }
      if (result < 0) ++_index;
      _status = result;
    } else _status = grammar::Fal;
  }

 private:
  std::vector<Step> _steps;
  grammar::Status   _status;
  std::size_t       _step_i, _index;
 
 public:
  bool loop;
};

base::Deserialize<>& operator+=(base::Deserialize<>& deserialize, base::Stream&& stream);
base::Serialize<>& operator-=(base::Serialize<>& serialize, base::Stream&& stream);
base::Deserialize<>& operator+=(base::Deserialize<>& deserialize, base::Stream& stream);
base::Serialize<>& operator-=(base::Serialize<>& serialize, base::Stream& stream);

base::Deserialize<>& operator+=(base::Deserialize<>& deserialize, std::shared_ptr<base::Stream>& stream);
base::Serialize<>& operator-=(base::Serialize<>& serialize, std::shared_ptr<base::Stream>& stream);
} // namespace base
#endif
