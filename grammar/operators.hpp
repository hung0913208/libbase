#if !defined(LIBBASE_GRAMMAR_OPERATORS_HPP_) && __cplusplus
#define LIBBASE_GRAMMAR_OPERATORS_HPP_

#include <vector>
#include <memory>
#include <map>

#include "lexical.hpp"
#include "../logcat.hpp"

// concatenation
template<typename ByteT=char> std::vector<Lexical<ByteT>>   operator>>(Lexical<ByteT> &&src, Lexical<ByteT> &&target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>&& operator>>(std::vector<Lexical<ByteT>> &&src, Lexical<ByteT> &&target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>   operator>>(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &&target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>   operator>>(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &&target);

template<typename ByteT=char> std::vector<Lexical<ByteT>>   operator>>(Lexical<ByteT> &&src, Lexical<ByteT> &target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>&& operator>>(std::vector<Lexical<ByteT>> &&src, Lexical<ByteT> &target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>   operator>>(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>   operator>>(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &target);

template<typename ByteT=char> std::vector<Lexical<ByteT>>  operator>>(Lexical<ByteT> &src, Lexical<ByteT> &&target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>& operator>>(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &&target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>  operator>>(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &&target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>  operator>>(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &&target);

template<typename ByteT=char> std::vector<Lexical<ByteT>>  operator>>(Lexical<ByteT> &src, Lexical<ByteT> &target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>& operator>>(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>  operator>>(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &target);
template<typename ByteT=char> std::vector<Lexical<ByteT>>  operator>>(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &target);

// alternation
template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &&left, Lexical<ByteT> &&right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, Lexical<ByteT> &&right);
template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &&left, std::vector<Lexical<ByteT>> &&right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, std::vector<Lexical<ByteT>> &&right);

template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &&left, Lexical<ByteT> &right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, Lexical<ByteT> &right);
template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &&left, std::vector<Lexical<ByteT>> &right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &&left, std::vector<Lexical<ByteT>> &right);

template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &left, Lexical<ByteT> &&right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, Lexical<ByteT> &&right);
template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &left, std::vector<Lexical<ByteT>> &&right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, std::vector<Lexical<ByteT>> &&right);

template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &left, Lexical<ByteT> &right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, Lexical<ByteT> &right);
template<typename ByteT=char> Lexical<ByteT> operator|(Lexical<ByteT> &left, std::vector<Lexical<ByteT>> &right);
template<typename ByteT=char> Lexical<ByteT> operator|(std::vector<Lexical<ByteT>> &left, std::vector<Lexical<ByteT>> &right);

// optional
template<typename ByteT=char> Lexical<ByteT> operator+(Lexical<ByteT> &&target);
template<typename ByteT=char> Lexical<ByteT> operator+(std::vector<Lexical<ByteT>>&& target);

template<typename ByteT=char> Lexical<ByteT> operator+(Lexical<ByteT> &target);
template<typename ByteT=char> Lexical<ByteT> operator+(std::vector<Lexical<ByteT>>& target);

template<typename ByteT> std::vector<Lexical<ByteT>>&& operator+(Lexical<ByteT> &&src, Lexical<ByteT> &&target);
template<typename ByteT> std::vector<Lexical<ByteT>>&& operator+(Lexical<ByteT> &&src, Lexical<ByteT> &target);
template<typename ByteT> std::vector<Lexical<ByteT>>& operator+(Lexical<ByteT> &src, Lexical<ByteT> &&target);
template<typename ByteT> std::vector<Lexical<ByteT>>& operator+(Lexical<ByteT> &src, Lexical<ByteT> &&target);

// repetition
template<typename ByteT=char> Lexical<ByteT> operator*(Lexical<ByteT> &&target);
template<typename ByteT=char> Lexical<ByteT> operator*(std::vector<Lexical<ByteT>> &&target);

template<typename ByteT=char> Lexical<ByteT> operator*(Lexical<ByteT> &target);
template<typename ByteT=char> Lexical<ByteT> operator*(std::vector<Lexical<ByteT>> &target);

// exception
template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &&src, Lexical<ByteT> &&target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>>&& src, Lexical<ByteT> &&target);
template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &&target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &&target);

template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &&src, Lexical<ByteT> &target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>>&& src, Lexical<ByteT> &target);
template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &target);

template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &src, Lexical<ByteT> &&target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &&target);
template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &&target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &&target);

template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &src, Lexical<ByteT> &target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &target);
template<typename ByteT=char> Lexical<ByteT> operator-(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &target);
template<typename ByteT=char> Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &target);

#include "concatenation.hpp"
#include "alternation.hpp"
#include "optional.hpp"
#include "repetition.hpp"
#include "exception.hpp"
#endif // LIBBASE_GRAMMAR_OPERATORS_HPP_