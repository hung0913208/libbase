#include "property.hpp"

namespace base{
namespace grammar{
namespace properties{
static std::map<std::string, std::tuple<int, Handler>>& __lazyload_defproper__(){
  static std::map<std::string, std::tuple<int, Handler>> __def_proper__{};

  if (__def_proper__.size() == 0){
  }
  return __def_proper__;
}
} // namespace properties

using namespace properties;
Handler Property::find(std::string name){
  if (__lazyload_defproper__().find(name) == __lazyload_defproper__().end())
    return nullptr;
  return std::get<1>(__lazyload_defproper__()[name]);
}


int Property::level(std::string name){
  if (__lazyload_defproper__().find(name) == __lazyload_defproper__().end())
    throw NotFound;
  return std::get<0>(__lazyload_defproper__()[name]);
}

void Property::assign(std::string name, Handler handler, int level){
  __lazyload_defproper__()[name] = std::make_tuple(level, handler);
}
} // namespace grammar
} // namespace base