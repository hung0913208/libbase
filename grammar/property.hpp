#ifndef LIBBASE_GRAMMAR_PROPERTIES_HPP_
#define LIBBASE_GRAMMAR_PROPERTIES_HPP_
#include <functional>
#include <memory>
#include <vector>
#include <tuple>

#include "../logcat.hpp"
/* property' preferences */
#define USE_PATTERN_WITH_MULTITHREAD     0
#define USE_PATTERN_SINGLETON_DEFINITION 0

/* property's names */ 
#define LIBBASE_GRAMMAR_PATTERN_PROPERTY "pattern"

/* property's values */
#define LIBBASE_GRAMMAR_TRUE_VALUE "true"
#define LIBBASE_GRAMMAR_FALSE_VALUE "false"

namespace base{
namespace grammar{
struct Context;
struct Property;

using Handler  = std::function<Context*(Property*, std::shared_ptr<Context>)>;

struct Property{
 public:
  std::string name, value;
  Handler     handler;

 public:
  Property(std::string name, std::string value):
    name{name}, value{value}, handler{find(name)} {}

 public:
  static Handler find(std::string name);
  static int     level(std::string name);
  static void    assign(std::string name, Handler handler, int level = 0);
};

struct Context{
 private:
  using Properties = std::vector<Property*>;

 public:
  Context(){}

 public:
  explicit Context(Properties& properties):
    _properties{properties} {}
  virtual ~Context(){}

 public:
  void init(std::map<std::string, std::shared_ptr<Property>>& properties){
    if (_properties.size() == 0)
      for (auto item: properties)
        _properties.push_back(std::get<1>(item).get());
  }

  void reinit(std::map<std::string, std::shared_ptr<Property>>& properties){
    uninit();
    init(properties);
  }

  void uninit(){ _properties.clear(); }

 public:
  bool match(Property* property){
    for (auto& item: _properties)
      if (property == item) return true;
    return false;
  }

 protected:
  Properties _properties;
};
} // namespace grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_PROPERTIES_HPP_