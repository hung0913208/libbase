#ifndef LIBBASE_GRAMMAR_OPTIONAL_HPP_
#define LIBBASE_GRAMMAR_OPTIONAL_HPP_ 

#include <memory>
#include <vector>

#include "grammar.hpp"
#include "sequence.hpp"

template<typename ByteT> 
Lexical<ByteT> operator+(Lexical<ByteT> &&target){
  return Lexical<ByteT>::make_shared(target.type(),
    [target](base::Pipe<ByteT>&, std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto result = target->pull(idx, in, trial);

      return result == base::grammar::Fal? base::grammar::Inv: result;
    },
    [target](base::Pipe<ByteT>&, std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto result = target->push(idx, out, trial);

      return result == base::grammar::Fal? base::grammar::Inv: result;
    }, [target]() -> base::Error{ return target->reset(); });
}

template<typename ByteT> 
Lexical<ByteT> operator+(std::vector<Lexical<ByteT>> &&target){
  return base::grammar::Sequence<ByteT>(target, 
    [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result == base::grammar::Fal? base::grammar::Inv: result;
    },
    [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result == base::grammar::Fal? base::grammar::Inv: result;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator+(Lexical<ByteT> &target){
  return Lexical<ByteT>::make_shared(target.type(),
    [target](std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto result = target->pull(idx, in, trial);

      return result == base::grammar::Fal? base::grammar::Inv: result;
    },
    [target](std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto result = target->push(idx, out, trial);

      return result == base::grammar::Fal? base::grammar::Inv: result;
    }, [target]() -> base::Error{ return target.grammar()->reset(); });
}

template<typename ByteT> 
Lexical<ByteT> operator+(std::vector<Lexical<ByteT>> &target){
  return base::grammar::Sequence<ByteT>(target, 
    [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result == base::grammar::Fal? base::grammar::Inv: result;
    },
    [](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result == base::grammar::Fal? base::grammar::Inv: result;
    });
}

template<typename ByteT> 
std::vector<Lexical<ByteT>>&& operator+(Lexical<ByteT> &&src, Lexical<ByteT> &&target){  
  return src >> (+target);
}

template<typename ByteT> 
std::vector<Lexical<ByteT>>&& operator+(Lexical<ByteT> &&src, Lexical<ByteT> &target){  
  return src >> (+target);
}

template<typename ByteT> 
std::vector<Lexical<ByteT>>& operator+(Lexical<ByteT> &src, Lexical<ByteT> &&target){  
  return src >> (+target);
}

template<typename ByteT> 
std::vector<Lexical<ByteT>>& operator+(Lexical<ByteT> &src, Lexical<ByteT> &target){  
  return src >> (+target);
}
#endif // LIBBASE_GRAMMAR_OPTIONAL_HPP_