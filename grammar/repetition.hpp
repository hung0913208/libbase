#include <memory>
#include <vector>

#include "grammar.hpp"
#include "sequence.hpp"

namespace gram = base::grammar;

template<typename ByteT> 
Lexical<ByteT> operator*(Lexical<ByteT> &&target){
  return Lexical<ByteT>::make_shared(target.type(),
    [target](std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto result = target->pull(idx, in, trial);

      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    },
    [target](std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto result = target->push(idx, out, trial);
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    }, [target]() -> base::Error{ return target.grammar()->reset(); });
}

template<typename ByteT>
Lexical<ByteT> operator*(Lexical<ByteT> &target){
  return Lexical<ByteT>::make_shared(target.type(),
    [target](base::Pipe<ByteT>&, std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto result = target->pull(idx, in, trial);

      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    },
    [target](base::Pipe<ByteT>&, std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto result = target->push(idx, out, trial);
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    },
    [target]() -> base::Error{ return target->reset(); });
}

template<typename ByteT> 
Lexical<ByteT> operator*(std::vector<Lexical<ByteT>> &&targets){
  return std::make_shared<base::grammar::Sequence<ByteT>>(
    [targets](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    },
    [targets](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator*(std::vector<Lexical<ByteT>> &targets){
  return std::make_shared<base::grammar::Sequence<ByteT>>(
    [targets](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    },
    [targets](std::size_t, base::grammar::Status result) -> base::grammar::Status{
      return result != gram::Don? (result != gram::Inv? result: gram::Fal): gram::Pal;
    });
}