#if !defined(LIBBASE_GRAMMAR_HPP_) && __cplusplus
#define LIBBASE_GRAMMAR_HPP_

#include "../trace.hpp"
#include "property.hpp"

template <typename ByteT> struct Lexical;
namespace base {
namespace grammar {
enum Status { Pal = -2, Unf = -1, Fal = 0, Don = 1, Inv = 2 };
} // namespace grammar

template <typename ByteT = char>
struct Pipe : std::enable_shared_from_this<Pipe<ByteT>> {
 public:
  virtual ~Pipe() {}

 public:
  virtual std::shared_ptr<grammar::Context> prev(std::size_t step) = 0;
  virtual std::shared_ptr<grammar::Context> next(std::size_t step) = 0;

 public:
  virtual base::Error
  scatter(grammar::Status &result,
          std::function<bool(grammar::Status, grammar::Status &)> &&condition,
          std::function<grammar::Status()> &&task) = 0;
  virtual base::Error waiting() = 0;

 protected:
  virtual void process(ByteT &byte) = 0;
};

namespace grammar {
#if DEBUG || TRACE
inline std::string status(Status status) {
  switch (status) {
  case Pal:
    return "Pal";
  case Unf:
    return "Unf";
  case Fal:
    return "Fal";
  case Don:
    return "Don";
  case Inv:
    return "Inv";
  }
  return "";
}
#endif

template <typename ByteT = char>
struct Grammar : Context, std::enable_shared_from_this<Grammar<ByteT>> {
 public:
  struct Gate {
   public:
    virtual bool status() { throw NoSupport; }

   public:
    virtual bool parse(std::vector<ByteT> &) { throw NoSupport; }
    virtual bool generate(std::vector<ByteT> &) { throw NoSupport; }
  };

 public:
  using Pull = std::function<Status(Pipe<ByteT> &, std::size_t, ByteT, bool)>;
  using Push = std::function<Status(Pipe<ByteT> &, std::size_t, ByteT &, bool)>;
  using Rset = std::function<base::Error()>;

 public:
  template <typename Type> Grammar<ByteT> &operator[](Type &variable);

  template <typename Type>
  Grammar<ByteT> &operator()(std::function<Type &()> bridge);

 public:
  template <typename Type> Grammar<ByteT> &bridge(std::shared_ptr<Gate> &gate);

  template <typename Type> Grammar<ByteT> &bridge(std::shared_ptr<Gate> &&gate);

 public:
  explicit Grammar(Pull pull, Push push, Rset reset = nullptr)
      : Context{}, _gate{nullptr}, _pipe{nullptr}, _result{Inv}, _pull{pull},
        _push{push}, _reset{reset} {}

  virtual ~Grammar() {}

 public:
  inline base::grammar::Status pull(std::size_t idx, ByteT in, bool trial = false) {
    auto result = Unf;

    if (_result != Fal && _result != Don) {
      result = _pull ? _pull(*_pipe, idx, in, trial) : Fal;

      if (!trial || (result == Don && trial))
        _cache.push_back(in);

      if (!trial && result != Don)
        _result = result;
      try {
        if (result != Pal && result != Unf &&
            (_gate && !_gate->parse(_cache)))
          return (result = Fal);
        else
          return result;
      } catch (base::Error &err) {
        if (!(err == base::error::ENoSupport))
          return (_result = Fal);
      }
    }
    return result;
  }

  inline base::grammar::Status push(std::size_t idx, ByteT &out, bool trial = false) {
    auto result = Unf;

    if (result != Fal && result != Don) {
      try {
        if (_gate && !_cache.size() && !_gate->generate(_cache))
          _cache.clear();
        if (_cache.size())
          out = pop();
      } catch (base::Error &err) {
        if (!(err == error::ENoSupport))
          return (_result = Fal);
      }

      result = _push ? _push(*_pipe, idx, out, trial) : Fal;

      if (!trial && result != Don)
        _result = result;
      return result;
    } else
      return _result;
  }

  inline base::Error reset() {
    auto error = _reset ? _reset() : NoError;

    if (error)
      return error;
    _result = Unf;
    return reset_();
  }

  inline std::shared_ptr<Grammar<ByteT>::Gate> &gate() { return _gate; }

  inline void pipe(Pipe<ByteT> *pipe) {
    if (_pipe == nullptr)
      _pipe = pipe;
  }

 public:
  std::vector<Lexical<ByteT>> split() { return split_(); }

  Grammar<ByteT> *clone() { return clone_(); }

  inline bool match(Property *property) { return Context::match(property); }

  inline std::vector<ByteT> &cache() { return _cache; }

 protected:
  virtual std::vector<Lexical<ByteT>> split_() {
    return std::vector<Lexical<ByteT>>{};
  }

  virtual Grammar<ByteT> *clone_() {
    auto ret = new Grammar<ByteT>(_pull, _push, _reset);

    ret->_gate = _gate;
    _gate = nullptr;
    return ret;
  }

  virtual base::Error reset_() { return NoError; }

  virtual bool support_(const std::type_info &) { return false; };

 private:
  ByteT pop() {
    if (_cache.size()) {
      ByteT result = _cache.front();

      _cache.erase(_cache.begin());
      return result;
    } else
      throw OutOfRange;
  }

  protected:
  std::shared_ptr<Gate> _gate;
  std::vector<ByteT> _cache;
  Pipe<ByteT> *_pipe;

 private:
  base::grammar::Status _result;
 private:
  Pull _pull;
  Push _push;
  Rset _reset;
};

template <typename Type, typename ByteT = char>
struct Attribute : public Grammar<ByteT>::Gate {
 protected:
  using Iterator = typename std::vector<ByteT>::iterator;

  using Convert =
      std::function<base::Error(bool, std::vector<ByteT> &, Type &)>;
  using Bridge = std::function<Type &()>;

 public:
  explicit Attribute(Bridge bridge) : _bridge{bridge} {}
  virtual ~Attribute(){}

 public:
  bool status() { return _status; }

  Bridge &bridge() { return _bridge; }

 protected:
  virtual bool parse(std::vector<ByteT> &value) {
    Error error;

    if (!_convert || !_bridge)
      return true;

    _status = true;
    do
      error = _convert(_status, value, _bridge());
    while (error == error::EKeepContinue);

    return error == 0;
  }

  virtual bool generate(std::vector<ByteT> &result) {
    Error error;

    if (!_convert || !_bridge)
      return true;

    _status = false;
    do
      error = _convert(_status, result, _bridge());
    while (error == error::EKeepContinue);
    return error == 0;
  }

 protected:
  bool _status;

 protected:
  Convert _convert;
  Bridge _bridge;
};

template <typename Type, typename ByteT = char>
struct Bridge : public Attribute<Type, ByteT> {
 public:
  using Decode = std::function<Type(std::vector<ByteT> &)>;
  using Encode = std::function<std::vector<ByteT>(Type &)>;

 public:
  explicit Bridge(std::shared_ptr<Attribute<Type, ByteT>> gate, Decode decode,
                  Encode encode)
      : Attribute<Type, ByteT>(gate ? gate->bridge() : nullptr),
        _decode{decode}, _encode{encode} {
    this->_convert = [&](bool left2right, std::vector<ByteT> &bytes,
                         Type &flatten) -> base::Error {
      try {
        if (left2right)
          flatten = _decode(bytes);
        else
          bytes = _encode(flatten);

        return NoError;
      } catch (base::Error &error) {
        return error;
      }
    };
  }

 private:
  Decode _decode;
  Encode _encode;
};

template <typename ByteT> struct Template {
 private:
  using Comm = std::function<Status(Pipe<ByteT> &, Template<ByteT> &, bool)>;
  using Reset = std::function<base::Error()>;
  using Event = std::function<bool(Pipe<ByteT> &)>;

 public:
  struct Implement : std::enable_shared_from_this<Implement> {
   public:
    virtual ~Implement() {}

   public:
    Template<ByteT> &self(Template<ByteT> *owner = nullptr) {
      if (_self)
        _self = owner;
      return *_self;
    }

   public:
    virtual operator bool() = 0;

   public:
    virtual bool operator++() = 0;
    virtual bool operator--() = 0;
    // virtual Status operator()() = 0;

  private:
    Template<ByteT> *_self;
  };

 public:
  explicit Template(std::shared_ptr<Implement> &&implement, Comm pull,
                    Comm push, Reset reset = nullptr)
      : _impl{implement}, _pull{pull}, _push{push}, _reset{reset} {}

  explicit Template(std::shared_ptr<Implement> &&implement, Comm communicate,
                    Reset reset = nullptr)
      : _impl{implement}, _pull{communicate}, _push{communicate}, _reset{
                                                                      reset} {}

  explicit Template(Comm pull, Comm push, Reset reset = nullptr)
      : _impl{nullptr}, _pull{pull}, _push{push}, _reset{reset} {}

  explicit Template(Comm communicate, Reset reset = nullptr)
      : _impl{nullptr}, _pull{communicate}, _push{communicate}, _reset{reset} {}

 public:
  typename Grammar<ByteT>::Pull pulling() {
    return [&](Pipe<ByteT> &pipe, std::size_t index, ByteT in, bool trial) {
      auto result = Fal;

      this->_index = index;
      this->_io = &in;

      if (_events.find(result = _pull(pipe, *this, trial)) != _events.end())
        if (_events[result](pipe))
          return Fal;
      return result;
    };
  }

  typename Grammar<ByteT>::Push pushing() {
    return [&](Pipe<ByteT> &pipe, std::size_t index, ByteT &out, bool trial) {
      auto result = Fal;

      this->_index = index;
      this->_io = &out;

      if (_events.find(result = _push(pipe, *this, trial)) != _events.end())
        if (_events[result](pipe))
          return Fal;
      return result;
    };
  }

  typename Grammar<ByteT>::Rset &reseting() { return _reset; }

 public:
  std::size_t index() { return _index; }

  ByteT *io() { return io; }

 public:
  Template<ByteT> &operator()(Status status, Event event) {
    _events[status] = event;
    return *this;
  }

  Event &operator[](Status status) { return _events[status]; }

  Template<ByteT> &operator[](std::vector<std::tuple<Status, Event>> &&events) {
    for (auto event : events)
      _events[std::get<0>(event)] = std::get<1>(event);
    return *this;
  }

  Template<ByteT> &operator<<(std::tuple<Status, Event> &&event) {
    _events[std::get<0>(event)] = std::get<1>(event);
    return *this;
  }

  bool operator++() { return (_impl) ? (*_impl)++ : false; }

  bool operator--() { return (_impl) ? (*_impl)-- : false; }

  operator bool() { return *_impl; }

 private:
  std::shared_ptr<Implement> _impl;
  std::map<Status, Event> _events;

 private:
  size_t _index;
  ByteT *_io;

 private:
  Comm _pull, _push;
  Reset _reset;
};

template <typename ByteT>
template <typename Type>
inline Grammar<ByteT> &Grammar<ByteT>::
operator()(std::function<Type &()> bridge) {
  auto attr = std::dynamic_pointer_cast<Attribute<Type, ByteT>>(_gate);

  if (support_(typeid(Type)))
    throw NoSupport;
  else if (attr)
    attr->bridge() = bridge;
  else
    _gate = std::make_shared<Attribute<Type, ByteT>>(bridge);
  return *this;
}

template <typename ByteT>
template <typename Type>
Grammar<ByteT> &Grammar<ByteT>::operator[](Type &variable) {
  auto attr = std::dynamic_pointer_cast<Attribute<Type, ByteT>>(_gate);
  auto bridge = std::function<Type &()>{[&]() -> Type & { return variable; }};

  if (support_(typeid(Type)))
    throw NoSupport;
  else if (attr)
    attr->bridge() = bridge;
  else
    _gate = std::make_shared<Attribute<Type, ByteT>>(bridge);
  return *this;
}

template <typename ByteT>
template <typename Type>
Grammar<ByteT> &
Grammar<ByteT>::bridge(std::shared_ptr<Grammar<ByteT>::Gate> &gate) {
  _gate = gate;
  return *this;
}

template <typename ByteT>
template <typename Type>
inline Grammar<ByteT> &
Grammar<ByteT>::bridge(std::shared_ptr<Grammar<ByteT>::Gate> &&gate) {
  _gate = gate;
  return *this;
}
} // namespace grammar
} // namespace base
#endif