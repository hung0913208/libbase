#if !defined(LIBBASE_GRAMMAR_TEMPLATES_HPP_) && __cplusplus
#define LIBBASE_GRAMMAR_TEMPLATES_HPP_
#include "grammar.hpp"

namespace base{
namespace grammar{
namespace templates{

template<typename ByteT=char> Template<ByteT> consecutive(std::size_t size){
  return Template<ByteT>{
    [size](Pipe<ByteT>&, Template<ByteT>& context, bool) -> Status {
      return context.index() < size? Don: Unf;
    }};
}

template<typename ByteT=char> Template<ByteT> match(std::basic_string<ByteT>&& sample){
  return Template<ByteT>{
    [sample](Pipe<ByteT>&, Template<ByteT>& context, bool) -> Status {
      if (sample[context.index()] != context.io())
        return Fal;
      return context.index() < sample.size()? Unf: Don;
    },
    [sample](Pipe<ByteT>&, Template<ByteT>& context, bool) -> Status {
      context.io() = sample[context.index()];
      return context.index() < sample.size()? Unf: Don;
    }};
}
} // namespace templates
} // namespace grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_TEMPLATES_HPP_