#ifndef LIBBASE_GRAMMAR_CONCATENATION_HPP_
#define LIBBASE_GRAMMAR_CONCATENATION_HPP_
#include <memory>
#include <vector>

#include "grammar.hpp"

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &&src, Lexical<ByteT> &&target){
  return std::vector<Lexical<ByteT>>{src, target};
}

template<typename ByteT>
std::vector<Lexical<ByteT>>&& operator>>(std::vector<Lexical<ByteT>> &&src, Lexical<ByteT> &&target){
  src.push_back(target);
  return std::move(src);
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &&target){
  std::vector<Lexical<ByteT>> result;

  result.push_back(src);
  result.insert(result.end - 1, target.begin(), target.end());
  return result;
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &&target){  
  std::vector<Lexical<ByteT>> result;

  result.insert(result.end() - 1, src.begin(), src.end());
  result.insert(result.end() - 1, target.begin(), target.end());
  return result;
}


template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &&src, Lexical<ByteT> &target){
  return std::vector<Lexical<ByteT>>{src, target};
}

template<typename ByteT>
std::vector<Lexical<ByteT>>&& operator>>(std::vector<Lexical<ByteT>> &&src, Lexical<ByteT> &target){
  src.push_back(target);
  return std::move(src);
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &target){
  std::vector<Lexical<ByteT>> result;

  result.push_back(src);
  result.insert(result.end - 1, target.begin(), target.end());
  return result;
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &target){
  std::vector<Lexical<ByteT>> result;

  result.insert(result.end() - 1, src.begin(), src.end());
  result.insert(result.end() - 1, target.begin(), target.end());
  return result;
}


template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &src, Lexical<ByteT> &target){
  return std::vector<Lexical<ByteT>>{src, target};
}

template<typename ByteT>
std::vector<Lexical<ByteT>>& operator>>(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &target){  
  src.push_back(target);
  return src;
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &target){  
  std::vector<Lexical<ByteT>> result;

  result.push_back(src);
  result.insert(result.end - 1, target.begin(), target.end());
  return result;
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &target){  
  std::vector<Lexical<ByteT>> result;

  result.insert(result.end() - 1, src.begin(), src.end());
  result.insert(result.end() - 1, target.begin(), target.end());
  return result;
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &src, Lexical<ByteT> &&target){
  return std::vector<Lexical<ByteT>>{src, target};
}

template<typename ByteT>
std::vector<Lexical<ByteT>>& operator>>(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &&target){
  src.push_back(target);
  return src;
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &&target){
  std::vector<Lexical<ByteT>> result;

  result.push_back(src);
  result.insert(result.end - 1, target.begin(), target.end());
  return result;
}

template<typename ByteT>
std::vector<Lexical<ByteT>> operator>>(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &&target){
  std::vector<Lexical<ByteT>> result;

  result.insert(result.end() - 1, src.begin(), src.end());
  result.insert(result.end() - 1, target.begin(), target.end());
  return result;
}
#endif