#ifndef LIBBASE_GRAMMAR_EXCEPTION_HPP_
#define LIBBASE_GRAMMAR_EXCEPTION_HPP_
#include <memory>
#include <vector>

#include "grammar.hpp"
#include "sequence.hpp"

namespace gram = base::grammar;

template<typename ByteT> 
Lexical<ByteT>operator-(Lexical<ByteT> &&src, Lexical<ByteT> &&target){
  return Lexical<ByteT>::make_shared(
    [src, target](std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto exclude = target->pull(idx, in, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->pull(idx, in, trial);
    },
    [src, target](std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto exclude = target->push(idx, out, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->push(idx, out, trial);
    }, [src, target]() -> base::Error{ 
      auto error = target.grammar()->reset(); 
      return error? error: src.grammar()->reset();
    });
}

template<typename ByteT> 
Lexical<ByteT>operator-(std::vector<Lexical<ByteT>> &&src, Lexical<ByteT> &&target){
  return base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(src),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    }) - target;
}

template<typename ByteT>
Lexical<ByteT>operator-(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &&targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &&targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}


template<typename ByteT> 
Lexical<ByteT> operator-(Lexical<ByteT> &src, Lexical<ByteT> &&target){
  return Lexical<ByteT>::make_shared(
    [src, target](std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto exclude = target->pull(idx, in, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->pull(idx, in, trial);
    },
    [src, target](std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto exclude = target->push(idx, out, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->push(idx, out, trial);
    }, [src, target]() -> base::Error{ 
      auto error = target.grammar()->reset(); 
      return error? error: src.grammar()->reset();
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &&target){
  return base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(src),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    }) - target;
}

template<typename ByteT>
Lexical<ByteT> operator-(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &&targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &&targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(Lexical<ByteT> &&src, Lexical<ByteT> &target){
  return Lexical<ByteT>::make_shared(
    [src, target](std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto exclude = target->pull(idx, in, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->pull(idx, in, trial);
    },
    [src, target](std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto exclude = target->push(idx, out, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->push(idx, out, trial);
    }, [src, target]() -> base::Error{ 
      auto error = target.grammar()->reset(); 
      return error? error: src.grammar()->reset();
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &&src, Lexical<ByteT> &target){
  return base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(src),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    }) - target;
}

template<typename ByteT>
Lexical<ByteT> operator-(Lexical<ByteT> &&src, std::vector<Lexical<ByteT>> &targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &&src, std::vector<Lexical<ByteT>> &targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(Lexical<ByteT> &src, Lexical<ByteT> &target){
  return Lexical<ByteT>::make_shared(
    [src, target](std::size_t idx, ByteT in, bool trial) -> base::grammar::Status{
      auto exclude = target->pull(idx, in, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->pull(idx, in, trial);
    },
    [src, target](std::size_t idx, ByteT& out, bool trial) -> base::grammar::Status{
      auto exclude = target->push(idx, out, trial);

      if (exclude == gram::Don || exclude == gram::Unf)
        return gram::Unf;
      else return src->push(idx, out, trial);
    }, [src, target]() -> base::Error{ 
      auto error = target.grammar()->reset(); 
      return error? error: src.grammar()->reset();
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, Lexical<ByteT> &target){
  return base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(src),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    }) - target;
}

template<typename ByteT>
Lexical<ByteT> operator-(Lexical<ByteT> &src, std::vector<Lexical<ByteT>> &targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}

template<typename ByteT> 
Lexical<ByteT> operator-(std::vector<Lexical<ByteT>> &src, std::vector<Lexical<ByteT>> &targets){
  return src - base::grammar::Sequence<ByteT>(std::forward<std::vector<Lexical<ByteT>>&&>(targets),
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    },
    [](std::size_t, base::grammar::Status status) -> base::grammar::Status{
      return status;
    });
}
#endif