#ifndef LIBBASE_GRAMMAR_SEQUENCE_HPP_
#define LIBBASE_GRAMMAR_SEQUENCE_HPP_

#include "pipe.hpp"

#include <functional>
#include <vector>

template<typename ByteT> struct Lexical;

namespace base{
namespace grammar{
template<typename ByteT=char>
struct Sequence: Grammar<ByteT>{ 
 public:
  using Pull = std::function<base::grammar::Status(std::size_t, base::grammar::Status)>;
  using Push = std::function<base::grammar::Status(std::size_t, base::grammar::Status)>;

 public:
  explicit Sequence(Sequence<ByteT>* src): Grammar<ByteT>{
    [=](Pipe<ByteT>&, std::size_t, ByteT in, bool)  -> Status{ 
      return _in(_deserialize.step(), (_deserialize << in).status());
    }, 
    [=](Pipe<ByteT>&, std::size_t, ByteT& out, bool) -> Status{
      return _out(_serialize.step(), (_serialize >> out).status());
    }}, _target{clone(src->_target)}, _serialize{_target}, _deserialize{_target}, _in(src->_in), _out{src->_out} {}

  explicit Sequence(std::vector<Lexical<ByteT>>&& target, Pull in_, Push out_): Grammar<ByteT>{
    [=](Pipe<ByteT>&, std::size_t, ByteT in, bool)  -> Status{ 
      return _in(_deserialize.step(), (_deserialize << in).status());
    }, 
    [=](Pipe<ByteT>&, std::size_t, ByteT& out, bool) -> Status{
      return _out(_serialize.step(), (_serialize >> out).status());
    }}, _target{target}, _serialize{target}, _deserialize{target}, _in(in_), _out{out_} {}

  explicit Sequence(std::vector<Lexical<ByteT>>& target, Pull in_, Push out_): Grammar<ByteT>{
    [=](Pipe<ByteT>&, std::size_t, ByteT in, bool) -> Status{ 
      return _in(_deserialize.step(), (_deserialize << in).status());
    }, 
    [=](Pipe<ByteT>&, std::size_t, ByteT& out, bool) -> Status{
      return _out(_serialize.step(), (_serialize >> out).status());
    }}, _target{target},_serialize{target}, _deserialize{target}, _in{in_}, _out{out_} {}

 protected:
  std::vector<Lexical<ByteT>> split_(){ return _target; }

  Grammar<ByteT>* clone_(){ return new Sequence<ByteT>(this); }

  base::Error reset_(){
    auto lerror = _serialize.reset();
    auto rerror = _deserialize.reset(); 

    if (!lerror) return lerror;
    if (!rerror) return rerror;
    
    return lerror;
  }

  std::vector<Lexical<ByteT>> clone(std::vector<Lexical<ByteT>>& src){
    std::vector<Lexical<ByteT>> result;

    for (auto& item: src)
      result.push_back(Lexical<ByteT>{item.type(), std::shared_ptr<Grammar<ByteT>>{item->clone()}});
    return result;
  }

 protected:
  std::vector<Lexical<ByteT>> _target;

 private:
  Serialize<ByteT>   _serialize;
  Deserialize<ByteT> _deserialize;

 private:
  Pull _in;
  Push _out;
};
} // grammar
} // namespace base
#endif // LIBBASE_GRAMMAR_SEQUENCE_HPP_