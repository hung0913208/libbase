#include <functional>
#include <map>
#include <mutex>
#include <thread>
#include <vector>

#include "logcat.hpp"
#include "trace.hpp"
#include "config.hpp"
#include "utils.hpp"

namespace base {
namespace config{
std::vector<Slot>& slots();
} // namespace config

namespace debug {
using Catch  = std::function<void(const char *, int, int, const char *)>;
using Syslog = std::function<void(std::string &&)>;
using TID    = std::size_t;

static std::map<TID, Catch> catchers{};
static std::vector<Syslog>  syslogs{};

static std::string *__watchdog__{nullptr};
static std::mutex dbglock, catlock;

#ifndef DEBUG
static bool dbgflag{false};
#else
static bool dbgflag = DEBUG;
#endif

static bool exclude = false;
} // namespace debug

namespace internal{
std::mutex   lglock;

namespace syslog{
std::string  path;
std::fstream console;
bool inited{false}, added{false};
} // namespace syslog

base::Error startlog(char*[], int){ 
  if (!syslog::added){
    syslog::added = true;

    config::slots().push_back(
      [](std::string& name, base::Auto&){
      if (!syslog::inited && name == "console"){
        auto syslog = [](std::string&& message){
          auto console = base::config::get("console");

          if (console != nullptr){
            try{
              auto path = console.get<std::string>();

              if (path != syslog::path || !syslog::console.is_open()){
                syslog::console.open(path, std::ios_base::out);

                if (!syslog::console.is_open())
                  syslog::console.open(syslog::path, std::ios_base::app);
                else
                  syslog::path = path;
              } 

              if (syslog::console.is_open()) {
                if (message.size() == 1 && message[0] == '\n') {
                  syslog::console << std::endl << std::flush;
                } else if (message.size() > 0)
                  syslog::console << message;
              }
            } catch(base::Error&){}
          }
        };

        debug::syslogs.push_back(syslog);
        syslog::inited = true;
      }
    });
  }
  return NoError;
}

base::Error clearlog(){
  debug::catchers.clear();

  debug::syslogs.clear();
  debug::syslogs.shrink_to_fit();

  syslog::console.close();
  if (debug::__watchdog__)
    delete debug::__watchdog__;
  return NoError;
}
} // namespace internal

namespace error {
void exclude(bool flag) { base::debug::exclude = flag; }
} // namespace error
} // namespace base

const std::string lvname(int code) {
  if (code < 0)
    return "INFO";
  else if (code == 0)
    return "ERROR";
  else
    return "WARN";
}

namespace base {
#if READABLE
Error::Error(std::string &&debug, int code, int level_, std::string &&message,
             bool ref)
    : message{message},
      debug(debug), code{code}, reference{ref}, _printed{nullptr},
      _reference{nullptr} {

  critical = (level = level_) == 0;
  debug::TID threadid{};

#if DEBUG || TRACE || PROFILING
  if (code || message.size() > 0){
    _printed = (bool *)calloc(sizeof(bool), 1);
    _reference = (int *)calloc(sizeof(int), 1);
  }
#endif

  if (debug::__watchdog__)
    (*debug::__watchdog__) = "[" + lvname(level_) + ": " + codename() +
                             "]: " + message + " at " + debug;
  base::debug::catlock.lock();

  threadid = std::hash<std::thread::id>{}(std::this_thread::get_id());
  if (base::debug::catchers.find(threadid) !=
      base::debug::catchers.end())
    base::debug::catchers[threadid](nullptr, code, level_, nullptr);

  // print();
  base::debug::catlock.unlock();
}
#else
Error::Error(std::string &&, int code, int level_, std::string &&, bool ref)
    : code{code}, level{level_}, critical{level_ == 0}, reference{ref},
      _printed{nullptr}, _reference{nullptr} {
  debug::TID threadid{};

#if DEBUG || TRACE
  if (code){
    _printed = (bool *)calloc(sizeof(bool), 1);
    _reference = (int *)calloc(sizeof(int), 1);
  }
#endif

  if (debug::__watchdog__)
    (*debug::__watchdog__) = lvname(level_) + ": " + codename();

  base::debug::catlock.lock();
  threadid = std::hash<std::thread::id>{}(std::this_thread::get_id());

  if (base::debug::catchers.find(threadid) !=
      base::debug::catchers.end())
    base::debug::catchers[threadid](nullptr, code, level_, nullptr);

  // print();
  base::debug::catlock.unlock();
}

Error::Error(int code, int level_, bool ref)
    : code{code}, level{level_}, critical{level_ == 0}, reference{ref},
      _printed{nullptr}, _reference{nullptr} {
  debug::TID threadid{};

#if DEBUG || TRACE
  if (code){
    _printed = (bool *)calloc(sizeof(bool), 1);
    _reference = (int *)calloc(sizeof(int), 1);
  }
#endif

  if (debug::__watchdog__)
    (*debug::__watchdog__) = lvname(level_) + ": " + codename();

  base::debug::catlock.lock();
  threadid = std::hash<std::thread::id>{}(std::this_thread::get_id());

  if (base::debug::catchers.find(threadid) !=
      base::debug::catchers.end())
    base::debug::catchers[threadid](nullptr, code, level_, nullptr);

  // print();
  base::debug::catlock.unlock();
}
#endif

#if READABLE
Error::Error()
    : code{0}, level{0}, critical{false}, reference{false}, _printed{nullptr},
      _reference{nullptr} {
#if DEBUG || TRACE || PROFILING
#endif
}
#endif

Error::~Error() {
  print();
#if DEBUG || TRACE || (PROFILING && READABLE)
  internal::lglock.lock();

  if (_reference) {
    if (*_reference > 0)
      (*_reference)--;
    else {
      free(_reference);

      if (_printed)
        free(_printed);

      _reference = nullptr;
      _printed = nullptr;
    }
  }

  internal::lglock.unlock();
#endif
}

void Error::print() const{
  if (debug::exclude)
    return;

#if READABLE
  Boundary bound{[](){ internal::lglock.lock(); },
                 [](){ internal::lglock.unlock(); }};
#if TRACE
  if ((code == 0 && level >= 0) || reference || (_printed && *_printed))
    return;
  else {
   #if !APPLE
    time_t now = time(0);
    char buffer[80];

    /* @NOTE: OSX have different implementation and this cause memory leak    */
    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now)); // <-- @OSX: memory leak
   #endif

    if (code == 0 && message.size() == 0)
      return;

    if (level == 0)
      trace::console << (Color{Color::Red} << "["
     #if !APPLE
                                           << buffer << ": "
     #endif
                                           << lvname(level) << ": "
                                           << codename() << "]: ")
                     << message << " at " << debug << trace::eol;
    else if (level < 0){
      trace::console << (Color{Color::Blue} << "["
     #if !APPLE
                                           << buffer << ": "
     #endif
                                            << lvname(level));
      if (code > 0){
        trace::console << (Color{Color::Blue} << ": " << codename() << "]: ")
                       << message << " at " << debug << trace::eol;
      } else {
        trace::console << (Color{Color::Blue} << "]: ") 
                       << message << trace::eol;
      }
    } else if (level > 0)
      trace::console << (Color{Color::Yellow} << "["
     #if !APPLE
                                              << buffer << ": "
     #endif
                                              << lvname(level) << ": "
                                              << codename() << "]: ")
                     << message << " at " << debug << trace::eol;
    if (_printed)
      *_printed = true;
  }
#elif DEBUG
#if PROFILING
  if (level_ < 0 && !reference && _printed && !*_printed) {
   #if !APPLE
    time_t now = time(0);
    char buffer[80];

    /* @NOTE: OSX have different implementation and this cause memory leak    */
    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now)); // <-- @OSX: memory leak
   #endif

    if (level == 0)
      trace::console << (Color{Color::Red} << "[" 
     #if !APPLE
                                           << buffer << ": "
     #endif
                                           << lvname(level) << ": "
                                           << codename() << "]: ")
                     << message << " at " << debug << trace::eol;
    else if (level < 0){
      trace::console << (Color{Color::Blue} << "[" 
     #if !APPLE
                                            << buffer << ": "
     #endif
                                            << lvname(level));
      if (code > 0){
        trace::console << (Color{Color::Blue} << ": " << codename() << "]: ")
                       << message << " at " << debug << trace::eol;
      } else {
        trace::console << (Color{Color::Blue} << "]: ") 
                       << message << trace::eol;
      }
    } else if (level > 0)
      trace::console << (Color{Color::Yellow} << "["
     #if !APPLE
                                           << buffer << ": "
     #endif
                                              << lvname(level) << ": "
                                              << codename() << "]: ")
                     << message << " at " << debug << trace::eol;
    if (_printed)
      *_printed = true;
  }
#endif

  if (code == 0 || !debug::dbgflag || level < 0 || reference || 
      (_printed && *_printed))
    return;
  else {
   #if !APPLE
    time_t now = time(0);
    char buffer[80];

    /* @NOTE: OSX have different implementation and this cause memory leak    */
    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now)); // <-- @OSX: memory leak
   #endif

    if (level == 0)
      trace::console << (Color{Color::Red} << "["
     #if !APPLE
                                           << buffer << ": "
     #endif
                                           << lvname(level) << ": "
                                           << codename() << "]: ")
                     << message << " at " << debug << trace::eol;
    else if (level < 0)
      trace::console << (Color{Color::Blue} << "["
     #if !APPLE
                                            << buffer << ": "
     #endif
                                            << lvname(level) << ": "
                                            << codename() << "]: ")
                     << message << " at " << debug << trace::eol;
    else if (level > 0)
      trace::console << (Color{Color::Yellow} << "["
     #if !APPLE
                                              << buffer << ": "
     #endif
                                              << lvname(level) << ": "
                                              << codename() << "]: ")
                     << message << " at " << debug << trace::eol;
    if (_printed)
      *_printed = true;
  }
#endif
  internal::lglock.unlock();
#else
#if DEBUG
  if (code == 0 || reference || (_printed && *_printed))
    return;
  else {
    time_t now = time(0);
    char buffer[80];

    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now));
    this->reference = reference;

    if (level == 0)
      trace::console << (Color{Color::Red} << "[ " << buffer << " ]: " 
                                           << lvname(level) << " : "
                                           << codename())
                     << trace::eol;
    else if (level < 0)
      trace::console << (Color{Color::Blue} << "[ " << buffer << " ]: " 
                                            << lvname(level) << " : "
                                            << codename())
                     << trace::eol;
    else if (level > 0)
      trace::console << (Color{Color::Yellow}
                         << "[ " << buffer << " ]: " << lvname(level) << " : "
                         << codename())
                     << trace::eol;
    if (_printed)
      *_printed = true;
  }
#else
#if PROFILING
  if (level_ < 0 && !reference) {
    time_t now = time(0);
    char buffer[80];

    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now));
    if (level == 0)
      trace::console << (Color{Color::Red} << "[ " << buffer << " ]: " 
                                           << lvname(level) << " : "
                                           << codename())
                     << trace::eol;
    else if (level < 0)
      trace::console << (Color{Color::Blue} << "[ " << buffer << " ]: " 
                                            << lvname(level) << " : "
                                            << codename())
                     << trace::eol;
    else if (level > 0)
      trace::console << (Color{Color::Yellow} << "[ " << buffer << " ]: " 
                                              << lvname(level) << " : "
                                              << codename())
                     << trace::eol;
  }
#endif
  if (code == 0 || !debug::dbgflag || level < 0 || reference)
    return;
  else {
    time_t now = time(0);
    char buffer[80];

    strftime(buffer, 80, "%m-%d-%Y %X", gmtime(&now));
    if (level == 0)
      trace::console << (Color{Color::Red} << "[ " << buffer << " ]: " 
                                           << lvname(level) << " : "
                                           << codename())
                     << trace::eol;
    else if (level < 0)
      trace::console << (Color{Color::Blue} << "[ " << buffer << " ]: " 
                                            << lvname(level) << " : "
                                            << codename())
                     << trace::eol;
    else if (level > 0)
      trace::console << (Color{Color::Yellow} << "[ " << buffer << " ]: " 
                                              << lvname(level) << " : "
                                              << codename())
                     << trace::eol;
  }
#endif
#endif
}

Error::Error(const Error &error):
    code{error.code}, level{error.level}, critical{error.critical},
    reference{true}, _printed{error._printed}, _reference{error._reference} {
  print();
#if READABLE
  internal::lglock.lock();

  message = error.message;
  debug = error.debug;

  internal::lglock.unlock();
#endif
#if DEBUG || TRACE || (PROFILING && READABLE)
  if (_reference)
    (*_reference)++;
#endif
}

Error::Error(const Error &&error):
    code{error.code}, level{error.level}, critical{error.critical},
    reference{true}, _printed{error._printed}, _reference{error._reference} {
  error.print();
#if READABLE
  internal::lglock.lock();

  message = error.message;
  debug = error.debug;

  internal::lglock.unlock();
#endif
#if DEBUG || TRACE || (PROFILING && READABLE)
  if (_reference)
    (*_reference)++;
#endif
}

Error &Error::reason(std::string &&_message) {
#if READABLE
  internal::lglock.lock();
  message = _message;
  internal::lglock.unlock();

  print();
#else
  (void)_message;
#endif
  return *this;
}

Error &Error::change(int level_) {
  critical = (level = level_) == 0;
  print();
  return *this;
}

Error &Error::ref(Error &error) {  
  error.print();
#if READABLE
  internal::lglock.lock();

  message = error.message;
  debug = error.debug;

  internal::lglock.unlock();
#endif
  code = error.code;
  level = error.level;
  critical = error.critical;
  reference = true;

  internal::lglock.lock();

  if (_reference) {
    if (*_reference > 0)
      (*_reference)--;
    else {
      free(_reference);

      if (_printed)
        free(_printed);

      _reference = nullptr;
      _printed = nullptr;
    }
  }

  internal::lglock.unlock();

  _printed = error._printed;
  _reference = error._reference;

#if DEBUG || TRACE || (PROFILING && READABLE)
  (*_reference)++;
#endif
  return *this;
}

std::string Error::codename() const{
  switch (code) {
  case base::error::ENoError:
    return "NoError";
  case base::error::EKeepContinue:
    return "KeepContinue";
  case base::error::ENoSupport:
    return "NoSupport";
  case base::error::EBadLogic:
    return "BadLogic";
  case base::error::EBadAccess:
    return "BadAccess";
  case base::error::EOutOfRange:
    return "OutOfRange";
  case base::error::ENotFound:
    return "NotFound";
  case base::error::EDrainMem:
    return "DrainMem";
  case base::error::EWatchErrno:
    return "WatchErrno";
  case base::error::EInterrupted:
    return "Interrupted";
  case base::error::EDoNothing:
    return "DoNothing";
  case base::error::EDoAgain:
    return "DoAgain";
  default:
    return std::to_string(code);
  }
}

bool Error::operator==(Error &&dst) { return dst.code == code; }

bool Error::operator==(int value) { return code == value; }

bool Error::operator==(bool value) { return (code != 0 && critical) == value; }

Error& Error::operator<<(const std::string& reason_){
 #if READABLE
  internal::lglock.lock();

  if (reason_[0] != ' '  && reason_[0] != '\'' && reason_[0] != '[' && 
      reason_[0] != '\"' && reason_[0] != '('  && reason_[0] != ']' &&
      reason_[0] != ')')
    message += (" " + reason_);
  else
    message += reason_;

  internal::lglock.unlock();
 #else
  (void) reason_;
 #endif
  return *this;
}

Error& Error::operator<<(std::string&& reason_){
 #if READABLE
  internal::lglock.lock();

  if (reason_[0] != ' '  && reason_[0] != '\'' && reason_[0] != '[' && 
      reason_[0] != '\"' && reason_[0] != '('  && reason_[0] != ']' &&
      reason_[0] != ')')
    message += (" " + reason_);
  else
    message += reason_;

  internal::lglock.unlock();
 #else
  (void) reason_;
 #endif
  return *this;
}

Error::operator bool() { return code != 0 && critical; }

Error::operator int() { return code; }

Error &Error::operator=(const Error &error) {
  if (&error == this)
    return *this;

  error.print();
#if READABLE
  internal::lglock.lock();

  message = error.message;
  debug = error.debug;

  internal::lglock.unlock();
#endif
  code = error.code;
  level = error.level;
  critical = error.critical;
  reference = true;

  internal::lglock.lock();

  if (_reference) {
    if (*_reference > 0)
      (*_reference)--;
    else {
      free(_reference);

      if (_printed)
        free(_printed);

      _reference = nullptr;
      _printed = nullptr;
    }
  }

  internal::lglock.unlock();

  _printed = error._printed;
  _reference = error._reference;
#if DEBUG || TRACE || (PROFILING && READABLE)
  if (_reference)
    (*_reference)++;
#endif
  return *this;
}

Error &Error::operator=(Error &&error) {
  if (&error == this)
    return *this;

  error.print();
#if READABLE
  internal::lglock.lock();

  message = error.message;
  debug = error.debug;

  internal::lglock.unlock();
#endif
  code = error.code;
  level = error.level;
  critical = error.critical;
  reference = true;

  internal::lglock.lock();

  if (_reference) {
    if (*_reference > 0)
      (*_reference)--;
    else {
      free(_reference);

      if (_printed)
        free(_printed);

      _reference = nullptr;
      _printed = nullptr;
    }
  }

  internal::lglock.unlock();

  _printed = error._printed;
  _reference = error._reference;
#if DEBUG || TRACE || (PROFILING && READABLE)
  if (_reference)
    (*_reference)++;
#endif
  return *this;
}

std::string Error::position(const char *file, int line, const char *proc) {
  std::string result{};

  result.append(std::string{file})
      .append(":")
      .append(std::to_string(line))
      .append(" ")
      .append(proc, strlen(proc));
  return result;
}

Debug::Debug(bool mode) {
  debug::dbglock.lock();
  _rescue = debug::dbgflag;
  debug::dbgflag = mode;
  debug::dbglock.unlock();
}

Debug::~Debug() {
  debug::dbglock.lock();
  debug::dbgflag = _rescue;
  debug::dbglock.unlock();
}

Watch::Watch() : _saved{debug::__watchdog__} {
  debug::__watchdog__ = &this->msg;
}

Watch::~Watch() { debug::__watchdog__ = _saved; }

#if !SINGLE_COLOR
Color::Color(std::size_t code)
    : _code{code},
#else
Color::Color(std::size_t)
    :
#endif
      _printed{false} {
}

Color &Color::operator<<(std::string message) {
  _message.append(message);
  return *this;
}

Color &Color::operator()() {
  if (!_printed && _message.size() > 0) {
#if !SINGLE_COLOR
    printf("\x1B[%lum%s\x1B[0m", _code, _message.c_str());
#else
    printf("%s", _message.c_str());
#endif
  }
  return *this;
}

std::string &&Color::message() { return rvalue(_message); }

namespace trace {
Console::Console(char keyword) : _keyword{keyword} {}

Console &Console::operator<<(Color &color) {
  if (_keyword == '\0') {
    for (auto log : debug::syslogs)
      log(rvalue(color.message()));
    if (config::get("console") == nullptr || config::get("monitor") != nullptr)
      color();
  }
  return *this;
}

Console &Console::operator<<(Color &&color) {
  if (_keyword == '\0') {
    for (auto log : debug::syslogs)
      log(rvalue(color.message()));
    if (config::get("console") == nullptr || config::get("monitor") != nullptr)
      color();
  }
  return *this;
}

Console &Console::operator<<(std::string &&message) {
  if (_keyword == '\0') {
    for (auto log : debug::syslogs)
      log(rvalue(message));
    if (config::get("console") == nullptr || config::get("monitor") != nullptr)
      print(message);
  }
  return *this;
}

Console &Console::operator<<(const std::string &message) {
  if (_keyword == '\0') {
    auto clone = std::string{message};

    for (auto log : debug::syslogs)
      log(rvalue(clone));
    if (config::get("console") == nullptr || config::get("monitor") != nullptr)
      print(message);
  }
  return *this;
}

Console &Console::operator<<(Console &console) {
  if (_keyword == '\0') {
    auto keyword = std::string{console.keyword()};

    for (auto log : debug::syslogs)
      log(rvalue(keyword));
    if (config::get("console") == nullptr || config::get("monitor") != nullptr)
      print(keyword);
  }
  return *this;
}

Console &Console::operator<<(int value) {
  if (_keyword == '\0') {
    auto svalue = std::to_string(value);

    for (auto log : debug::syslogs)
      log(rvalue(svalue));
    if (config::get("console") == nullptr || config::get("monitor") != nullptr)
      print(svalue);
  }
  return *this;
}
} // namespace trace
} // namespace base

std::ostream &operator<<(std::ostream &stream, base::Error error) {
#if READABLE
  stream << "[" << error.codename() << "]: " << error.message << " at "
         << error.debug << std::flush;
#else
  stream << "got an error: [" << error.codename() << "]: " << std::flush;
#endif
  return stream;
}

std::ostream &operator<<(std::ostream &ostream, base::Color &color) {
  color();
  return ostream;
}

void writeLogW(const char *format, ...) {
  va_list vlist;

  va_start(vlist, format);
  vprintf(format, vlist);
  va_end(vlist);
}

void catchLogW(unsigned int threadid,
               void (*callback)(const char *line, int code, int level,
                                const char *message)) {
  base::debug::catchers[threadid] = callback;
}

extern "C" {
typedef struct Debug {
  char *file, *proc;
  int line;
} Debug;

Debug cdebug(const char *file, const char *proc, int line) {
  Debug result;

  result.line = line;
  result.file = (char *)file;
  result.proc = (char *)proc;

  return result;
}

int cerror(int code, int level, Debug pos, const char *message) {
  return base::Error(base::Error::position(pos.file, pos.line, pos.proc), code,
                     level, std::string{message});
}
}