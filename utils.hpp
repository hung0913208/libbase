#if !defined(LIBBASE_UTILS_HPP_) && __cplusplus
#define LIBBASE_UTILS_HPP_

#include <string>
#include <sstream>
#include <iomanip>
#include <memory>
#include <functional>

#include "macros.hpp"

#define rvalue RVALUE
#define squeeze SQUEEZE
#define cast_ CAST_

namespace base{
template<typename T> T* duplicate(T* sample){ return new T{sample}; }

template<typename T> T reverse(T value){
  uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);

  for (auto i = 0; i < cast_(i, sizeof(value)) / 2; ++i)
    std::swap(bytes[i], bytes[sizeof(value) - i - 1]);

  return *(reinterpret_cast<T*>(bytes));
}

template<typename T> std::string i2hex(T value){
  std::stringstream stream;

  stream << "0x" << std::setfill ('0') << std::setw(sizeof(T)*2) 
         << std::hex << value;
  return stream.str();
}

struct Boundary{
 /* @WARNING: 
  * this struct will go wrong if you define like this: base::Boundary boundary{[](){}, [](){}};
  * please you always need to define fully variables.
  */
 public:
  explicit Boundary(std::function<void()> init,
                          std::function<void()> destroy):
    _sdestroy{destroy}, _status{true} { init(); }

  explicit Boundary(std::function<void(Boundary&)> init,
                          std::function<void(Boundary&)> destroy):
    _wdestroy{destroy}, _status{true} { init(*this); }

  virtual ~Boundary(){ release(); }

  inline void* &context(){ return _context; }

  inline void release(){
    if (!_status) return;
    else if (_sdestroy) _sdestroy();
    else
      _wdestroy(*this);
    _status = false;
  }
 protected:
  Boundary(Boundary&) = delete;
  Boundary() = delete;
 
 private:
  std::function<void(Boundary&)> _wdestroy;
  std::function<void()> _sdestroy;
  void* _context;
  bool  _status;
};

template<typename Type>
struct Property: std::enable_shared_from_this<Property<Type>>{
 private:
  using PropertyPtr = std::shared_ptr<Property<Type>>;

 public:
  using Setter = std::function<void(Type)>;
  using Getter = std::function<Type()>;

 public:
  explicit Property(Getter getter, Setter setter):
    _getter{getter}, _setter{setter}{}

  explicit Property(Getter getter): _getter{getter}{
    _setter = [&](Type value){ _value = value; };
  }

  explicit Property(Setter setter): _setter{setter}{
    _getter = [&]() -> Type { return _value; };
  }

  virtual ~Property(){}
  Property(){
    _getter = [&]() -> Type { return _value; };
    _setter = [&](Type value) { _value = value; };
  }

 public:
  void operator=(const Type&& value){ _setter(const_cast<Type&&>(value)); }

  void operator=(const Type& value){ _setter(const_cast<Type&>(value)); }

  void operator=(Type&& value){ _setter(value); }

  void operator=(Type& value){ _setter(value); }

  Type operator()(){ return _getter(); }

 public:
  Property& getter(Getter callback){
    _getter = callback;
    return *this;
  }

  Property& setter(Setter callback){
    _setter = callback;
    return *this;
  }

 protected:
  std::function<void(Type)> _setter;
  std::function<Type()>    _getter;

 private:
  Type _value;
};
} // namespace base

#endif  // LIBBASE_UTILS_HPP_