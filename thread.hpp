#ifndef LIBBASE_THREAD_HPP_
#define LIBBASE_THREAD_HPP_
#include <pthread.h>
#include <memory.h>

#include "logcat.hpp"

#if __cplusplus
extern "C"{
#endif
typedef pthread_mutex_t pmutex_t;
typedef int   (*Checker)(void*);
typedef void* (*Runner)(void*);

pthread_t clbCreateThread(Runner runner);
CError    clbWaitThreadUntilEnd(pthread_t thread);

pmutex_t clbCreateLockWithTimeout(int timeout);
pmutex_t clbCreateLockWithChecker(int timeout,
                                  void* content, Checker check);
pmutex_t clbCreateLock();

CError   clbLockOn(pmutex_t lock);
CError   clbLockOff(pmutex_t lock);
CError   clbRemoveLock(pmutex_t lock);
#if __cplusplus
}
#endif

#if __cplusplus
#include <functional>
#include <memory>

#include "tuple.hpp"

namespace base{
struct Thread: std::enable_shared_from_this<Thread>{
 private:
  enum State{ Stop = 0, Run = 1, Pause = 2 };

 public:
  using Callback = std::function<Tuple(Thread&, Tuple&)>;
  using Yield    = std::function<void(Tuple)>;
  using PThread  = pthread_t;
  using PMutex   = pthread_mutex_t;

 public:
  explicit Thread(Callback callback, bool start_immediately = false);
  virtual ~Thread();

 public:
  Thread(Thread&) = delete;
  Thread() = delete;

 public:
  base::Error yield(Yield callback);
  bool kill(int signal);

  bool start(bool wait = false);
  bool start(Tuple args, bool wait = false);

 private:
  Callback _callback;
  Yield    _yield;
  PThread  _pthread;
  PMutex   _lock;

 private:
  State _status;
  Tuple _inputs;
};

struct Lock{
 public:
  using Callback = std::function<void()>;

  ~Lock();
  explicit Lock();

 public:
  void safe(Callback callback);
  void wait(Callback before, Callback after);
  void lock();
  void unlock();

  base::Property<bool> status;

 private:
  Thread::PMutex  _mutex;
  Thread::PThread _thread;

  bool _status;
};
}  // namespace base
#endif
#endif  // LIBBASE_THREAD_HPP_