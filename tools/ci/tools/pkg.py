import subprocess
import platform
import shutil
import sys
import os

class Pkg(object):
	def __init__(self, **kargs):
		super(Pkg, self).__init__()

		if platform.system() == 'Linux':
			self._commands = {
				'install': 'install -y', 
				'remove': 'remove',
				'update': 'update',
				'upgrade': 'upgrade',
				'search': 'search'
			}

			if len(shutil.which('apt-get')) > 0:
				self._pkg = 'apt-get'
			elif len(shutil.which('yum')) > 0:
				self._pkg = 'yum'
			elif len(shutil.which('zypper')) > 0:
				self._pkg = 'zypper'
        	elif len(shutil.which('pkg')) > 0:
        		self._pkg = 'pkg'
		elif platform.system() == 'Mac':
			if len(shutil.which('brew')) == 0:
				sys.exit(-1)
			else:
				self._pkg = 'brew'
