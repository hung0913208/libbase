require 'net/http/post/multipart'
require 'json/ext'
require 'net/http'
require 'time'
require 'json'

class FSend
	UP_KEYS = "http://fsend.vn/v2/up-keys"
	CONTENT_JSON = {"Content-Type" => "application/json"}

	def initialize
		@instance_ = Net::HTTP.new("fsend.vn", 80)
		@session_  = nil
	end

	def session
		if @session_.nil? or @session_[:expire] >= Time.now then
			resp = @instance_.send_request("POST", FSend::UP_KEYS, data={}.to_json, header=CONTENT_JSON)
			code = resp.code.to_i

			@session_ = nil
			if code == 200 or code == 201 then
				context = JSON.parse(resp.body)

				if context.key?("key") then
					@session_ = {key: context["key"], expire: Time.parse(context["expire_in"])}
				end
			end
		end

		return @session_
	end

	def upload(file)
		File.open(file) do |fd|
			url = "#{UP_KEYS}/#{self.session()[:key]}/upload"
			puts File.size(file)
			req = {"file_name": File.basename(file), "file_size": File.size(file)}
			res = @instance_.send_request("PUT", url, data=req.to_json, header=CONTENT_JSON)

			if res.code.to_i == 200 then
				files = {"file" => UploadIO.new(fd, "text/plain", File.basename(file))}
				context = JSON.parse(res.body)
				
				puts context	
				location = context['location']
				service  = location.split('/')[2]
				protocol = location.split('://').first
				hostname = service.split(':').first
				port     = service.split(':')[1]

				if port == nil then
					port = 
						if protocol == 'http' then 80 
						elsif protocol == 'https' then 443
						end
				end
				req = Net::HTTP::Post::Multipart.new(location, files)
				res = Net::HTTP.start(hostname, port){ |http| http.request(req) }
				puts res.body
				return res.code.to_i == 200
			end

			return false
		end
	end
end
send = FSend.new
puts ARGV.first
send.upload(ARGV.first)
