import subprocess

class Exec(object):
	def __init__(self, **kwags):
		super(ExecP, self).__init__()

	def perform(self):
		pass

class Sequence(object):
	def __init__(self, *args):
		pass

class Compiler(object):
	def __init__(self):
		super(Compiler, self).__init__()

	def check(self):
		raise AssertionError("This is an abstract class, please implement it")

class Git(object):
	def __init__(self):
		super(Git, self).__init__()

class Generator(object):
	def __init__(self, type, **kargs):
		super(Generator, self).__init__()

		if 'project' in kargs:
			self._project = kargs['project']
		else:
			self._project = ''

	def generate(self):
		if len(self._project) > 0:
			# this project is intended to be a big-project not a sub-project

	def build(self):
		pass
