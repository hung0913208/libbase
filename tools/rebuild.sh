set -e

if [ "$1" = "Coverage" ]; then
    unameOut="$(uname -s)"
    case "${unameOut}" in
        Linux*)     machine=Linux;;
        Darwin*)    machine=Mac;;
        CYGWIN*)    machine=Cygwin;;
        MINGW*)     machine=MinGw;;
        *)          machine="UNKNOWN:${unameOut}"
    esac
    if [[ "$CC" =~ "clang" ]] || [[ "$CXX" =~ "clang++" ]] || [[ $machine == "Mac" ]]; then
        exit 0
    fi
fi

mkdir -p ./$1
cd ./$1
BUILD_TYPE=$1
shift
if [ $# -ne 0 ]; then
    cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_CXX_FLAGS="'$*'" ../..
else
    cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE ../..
fi

if [ $? -ne 0 ]; then
        code=-1
fi

make VERBOSE=1
if [ $? -ne 0 ]; then
    code=-1
fi

cd ..
exit $code
