unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac
BRANCH=$(git rev-parse --abbrev-ref HEAD)
if [[ $CC =~ '*clang*' ]] && [[ $CXX =~ '*clang++*' ]] && [ $machine != "Mac" ]; then
    echo "we didn't support install gtest in this environment"
else
    if [ $BRANCH == $1 ] || [ $# == 0 ]; then
        echo "gtest is being installed into branch $branch"
        rm -fr googletest
        git clone https://github.com/google/googletest.git

        cd googletest
        cmake -DBUILD_SHARED_LIBS=ON .
        make && make install

        if [ $machine == 'Linux' ]; then
            ldconfig -v | grep gtest
        fi

        if [ $# == 2 ]; then
            cd $2
        fi
    else
        echo "gtest can't be installed in this branch: $branch"
    fi
fi
