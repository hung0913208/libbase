if [ ! $# -ne 4 ]; then
	export NGROK_AUTHTOKEN=$4

	rm -fr ngrok.zip
	rm -fr ngrok

	unameOut="$(uname -s)"
	case "${unameOut}" in
		Linux*)     wget -q -O ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip;;
		Darwin*)    wget -q -O ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-darwin-amd64.zip;;
		MINGW*)     wget -q -O ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-windows-amd64.zip;;
		FreeBSD*)   wget -q -O ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-freebsd-amd64.zip;;
		*)          exit 1
		esac
	unzip ngrok.zip
fi

if [ -z $NGROK_AUTHTOKEN ]; then
	cd ./$2 || exit -1
	if [ $1 == "gdbserver" ]; then
		gdbserver localhost:1234 ./$3 &>/dev/null
	elif [ $1 == "valgrind" ]; then
		(valgrind --tool=memcheck --leak-check=full --leak-resolution=high --track-origins=yes --show-leak-kinds=all --vgdb=full --vgdb-error=0 ./$3 &>/dev/null) & (sleep 5 && cgdb -ex 'target remote | vgdb')
		pkill --signal 9 -f /usr/bin/valgrind.bin
	else
		echo "don\'t support $1"
	fi
elif [ ! -f ./$2/$3 ]; then
	echo "Your build with config $2 is fail at the compiling step"
else
	./ngrok authtoken ${NGROK_AUTHTOKEN}
	pkill --signal 9 -f ./ngrok
	if [ $1 == "gdbserver" ]; then
		((gdbserver localhost:5678 ./$3 &>/dev/null) && pkill --signal 9 -f ./ngrok) & (./ngrok tcp 5678)
	elif [ $1 == "valgrind" ]; then
		((valgrind --tool=memcheck --leak-check=full --leak-resolution=high --track-origins=yes --show-leak-kinds=all --vgdb=full --vgdb-error=0 ./$3) && pkill --signal 9 -f ./ngrok)& (sleep 5 && (vgdb --port=1234 &>/dev/null)) & (./ngrok tcp 1234 &>/dev/null)
	else
		echo "don\'t support $1"
	fi
	pkill --signal 9 -f /usr/bin/valgrind.bin
	pkill --signal 9 -f gdbserver
	cd ../ || exit -1
fi
