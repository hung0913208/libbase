set -e
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ -d "./$1" ]; then
  cd "./$1"
  if [ "$1" == "Debug" ]; then
    ulimit -c unlimited
    if ! ctest --verbose; then
      COREFILE=$(find . -maxdepth 1 -name "core*" | head -n 1)
      if [[ -f "$COREFILE" ]]; then
        gdb -c "$COREFILE" "$2" -ex "thread apply all bt" -ex "set pagination 0" -batch;
      fi
      exit -1
    fi
    if [ $# -gt 1 ]; then
      if [ $machine == 'Linux' ]; then
        echo
        echo
        echo "Run with ASAN_SYMBOLIZER_PATH"
        echo

        ASAN_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer $2
        if [ "$?" -ne 0 ]; then
          exit -1
        fi

        echo
        echo
        echo "Run with ASAN_OPTIONS: detect_leaks=1"
        echo

        ASAN_OPTIONS=detect_leaks=1 "$2"
        if [ "$?" -ne 0 ]; then
          exit -1
        fi
      fi
    fi
  else
    if [ "$(ctest --verbose)" -ne 0 ]; then
      exit -1
    fi
    cd ..
  fi
  cd ..
else
  echo "Please run \"build.sh $1\" first"
  exit -1
fi