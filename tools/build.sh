set -e

if [ "$1" = "Coverage" ]; then
    unameOut="$(uname -s)"
    case "${unameOut}" in
        Linux*)     machine=Linux;;
        Darwin*)    machine=Mac;;
        CYGWIN*)    machine=Cygwin;;
        MINGW*)     machine=MinGw;;
        FreeBSD*)   machine=FreeBSD;;
        *)          machine="UNKNOWN:${unameOut}"
    esac
fi

if [ -d ./$1 ]; then
    echo "Found old version build --> remove it now"
    rm -fr ./$1/*
fi

mkdir -p ./$1
cd ./$1
BUILD_TYPE=$1
shift
if [ -f ../../CMakeLists.txt ]; then
    if [ $# -ne 0 ]; then
        cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE ${CONFIGURE} -DCMAKE_CXX_FLAGS="'$*'" ../..
    else
        cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE ${CONFIGURE} ../..
    fi
elif [ -f ../CMakeLists.txt ]; then
    if [ $# -ne 0 ]; then
        cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE ${CONFIGURE} -DCMAKE_CXX_FLAGS="'$*'" ../.
    else
        cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE ${CONFIGURE} ../.
    fi
else
    CMAKELIST=$2
    shift
    if [ $# -ne 0 ]; then
        cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE ${CONFIGURE} -DCMAKE_CXX_FLAGS="'$*'" ${CMAKELIST}
    else
        cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=$BUILD_TYPE ${CONFIGURE} ${CMAKELIST}
    fi
fi

if [ $? -ne 0 ]; then
    code=-1
fi

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     make -j $(($(grep -c ^processor /proc/cpuinfo)*2)) VERBOSE=1;;
    Darwin*)    make -j $(($(sysctl hw.ncpu | awk '{print $2}')*2)) VERBOSE=1;;
    CYGWIN*)    make VERBOSE=1;;
    MINGW*)     make VERBOSE=1;;
    FreeBSD*)   make -j $(($(sysctl hw.ncpu | awk '{print $2}')*2)) VERBOSE=1;;
    *)          make VERBOSE=1;;
esac

if [ $? -ne 0 ]; then
    code=-1
fi

cd ..
exit $code
