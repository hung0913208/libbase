#!/usr/bin/env bash

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    FreeBSD*)   machine=FreeBSD;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ "$machine" == 'Linux' ] || [ "$machine" == 'FreeBSD' ]; then
    SU=''
    INSTALL=''
    REQUIRED=''

    # check and set INSTALL tool
    if [ $(which apt-get) ]; then
        apt update
        apt upgrade -y

        INSTALL='apt-get install -y'
        REQUIRED=("ssh" "python")
    elif [ $(which yum) ]; then
        yum update
        yum upgrade -y

        INSTALL='yum install -y'
        REQUIRED=("ssh" "python")
    elif [ $(which zypper) ]; then
        zypper update
        zypper upgrade -y

        INSTALL='zypper install -y'
        REQUIRED='ssh python'
    elif [ $(which pkg) ]; then
        pkg update

        INSTALL='pkg'
        REQUIRED=("ssh" "python")
    else
        echo ''
        echo 'I dont known your distribution, please inform me if you need to support'
        echo 'You can send me an email to hung0913208@gmail.com'
        echo 'Thanks'
        echo 'Hung'
        echo ''
        exit -1
    fi

    # check root if we didn't have root permission
    if [ $(whoami) != 'root' ]; then
        if [ ! $(which sudo) ]; then
            echo "Sudo is needed but you aren't on root and can't access to root"
            exit -1
        fi

        if sudo -S -p '' echo -n < /dev/null 2> /dev/null ; then
            SU='sudo'
        else
            echo 'Sudo is not enabled'
            exit -1
        fi
    fi
fi

if [ "$machine" == 'Mac' ] then
    SU=''
    REQUIRED=("ssh" "python")
    INSTALL='brew install'

    # check and set INSTALL tool
    if [ ! $(which brew) ]; then
        echo 'Please install brew before run anything on you OSX system'
        exit -1
    fi
fi

# okey, install basic packages which are defined above
for PACKAGE in $REQUIRED; do
    $SU $INSTALL $REQUIRED
done

ROOT=$(pwd)
if [ $(which python) ]; then
    if [ ! -f "$ROOT/tools/ci/fetch.py" ]; then
        echo "it seems your ci can't fetch libbase successfully"
        exit -1
    fi

    # now redeploy ci platform of libbase to this dir
    cp -a "$ROOT/tools/ci/*" "$ROOT"

    # okey now, run ci tools to fetch, build and test your project, base on your 
    # file yaml
    if [! $(python fetch.py --project $1 --branch $2 --instruct $3) ]; then
        echo "Project $1 on branch $2 are failing recently"
        exit -1
    elif [! $(python prepare.py --project $1 --branch $2 --instruct $3) ]; then
        echo "Project $1 on branch $2 are failing recently"
        exit -1
    elif [! $(python build.py --project $1 --branch $2 --instruct $3) ]; then
        echo "Project $1 on branch $2 are failing recently"
        exit -1
    elif [! $(python test.py --project $1 --branch $2 --instruct $3) ]; then
        echo "Project $1 on branch $2 are failing recently"
        exit -1
    else
        echo "Congratulation, Project $1 is pass everything now"
        exit 0
    fi
else
    echo "It seems your system can't install python as i expected"
    exit -1
fi
