set -e
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [[ -d ./Coverage ]]; then
        cd ./Coverage
        if [ "$(make "$1")" -ne 0 ]; then
                exit -1
        fi
        lcov --directory . --capture --output-file coverage.info
        lcov --remove coverage.info '/usr/*' --output-file coverage.info # filter out system
        lcov --list coverage.info #debug info
        export CODECOV_TOKEN=$2
        bash <(curl -s https://codecov.io/bash) || echo "Codecov did not collect coverage reports"
        cd ..
# elif [[ "$CC" =~ "clang" ]] || [[ "$CXX" =~ "clang++" ]] || [[ "$machine" == "Mac" ]]; then
#         echo "Warning: Please run \"build.sh Coverage\" first"
#         exit 0
fi