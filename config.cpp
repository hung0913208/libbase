#include "all.hpp"

namespace base{
namespace internal{
base::Error startlog(char* argv[], int argc);
base::Error clearlog();

base::Error starttest(char* argv[], int argc);
base::Error cleartest();

base::Error startdlock(char* argv[], int argc);
base::Error cleardlock();
} // namespace internal

namespace config{
namespace internal{
bool started = false;

std::map<std::string, base::Auto> configs;
std::map<std::string, int> exceptions;
std::vector<Slot> slots;
} // namespace internal

std::vector<Slot>& slots(){ return base::config::internal::slots; }

base::Auto get(std::string name){
  using namespace base::config::internal;

  if (started && configs.find(name) != configs.end())
    return configs[name];
  return base::Auto{};
}

base::Error set(std::string name, base::Auto value){
  using namespace base::config::internal;

  if (started){
    base::Error error;

    if ( exceptions.find(name) != exceptions.end())
      return BadAccess;
    else
      configs[name] = value;

    for (auto& slot: internal::slots) slot(name, value);
  }
  return NoError;
}

void start(char* argv[], int argc){
  using namespace base::config::internal;
  using namespace base::internal;

  if (!started){
    startlog(argv, argc);
    starttest(argv, argc);
    startdlock(argv, argc);
    started = true;
  }
}

int _finish(int code){
  using namespace base::config::internal;
  using namespace base::internal;

  if (started){
    auto error = NoError;
    auto result = NoError;

    result = (error = cleartest());

    error = clearlog();
    if (!result)
      result = error;

    error = cleardlock();
    if (!result)
      result = error;

    started = false;
    return (result)? -1: code;
  }
  return code;
}

int finish(int code, std::function<void()> before, std::function<void()> after){
  int result = code;

  if (before) before();
  result = _finish(code);
  if (after) after();
  return result;
}
} // namespace config
} // namespace