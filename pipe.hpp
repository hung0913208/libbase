#if  !defined(LIBBASE_GRAMMAR_ALL_HPP_) && __cplusplus
#define LIBBASE_GRAMMAR_ALL_HPP_
#include "grammar/operators.hpp"
#include "grammar/grammar.hpp"
#include "grammar/parallel.hpp"
#include "grammar/content.hpp"
#include "grammar/sequence.hpp"
#include "grammar/templates.hpp"
#endif