#if !defined(LIBBASE_STREAM_HPP_)
#define LIBBASE_STREAM_HPP_
#if !__cplusplus
#include <stdint.h>
#else
#include <cstdint>
#endif

typedef struct CBuffer{
  int     *_begin, *_end;
  uint8_t *_buffer, *_left;
  int     *_size;
} CBuffer;

typedef struct CRack{
  /* @NOTE: io methods */
  int (*read)(struct CRack* cache, uint8_t* block, int* size);
  int (*write)(struct CRack* cache, uint8_t* block, int* size);
  int (*seek)(struct CRack* cache, long offset, int whence);
  int (*tell)(struct CRack* cache);

  /* @NOTE: memory methods */
  uint8_t* (*mmap)(struct CRack* cache);
} CRack;

#if __cplusplus
#include <memory>
#include <functional>
#if !DISABLE_LIBGRAMMAR
#include "./pipe.hpp"
#endif

#include "./logcat.hpp"
#include "./utils.hpp"
#include "./trace.hpp"

namespace base {
struct Cache: std::enable_shared_from_this<Cache>{
 public:
  using byte = uint8_t;

 public:
  virtual ~Cache(){}

 public:
  virtual bool write(uint8_t byte) = 0;
  virtual byte read() = 0;

 public:
  virtual bool revert(int count) = 0;
  virtual void reset() = 0;

 public:
  virtual std::size_t size() = 0;
  virtual std::size_t max() = 0;
};

struct Buffer: Cache{
 private:
  CBuffer _cbuff;
  bool    _autodel;
  bool    _inited;
  int    *_cref, _max;

 public:
  using byte = uint8_t;

 public:
  Buffer(const Buffer& src);
  Buffer();

 public:
  ~Buffer() override;

 public:
  template<typename Type> base::Error init(Type* buffer, std::size_t count, bool autoremove = true){
    _cbuff._buffer  = reinterpret_cast<uint8_t*>(buffer);
    _cbuff._size    = new int{_max = sizeof(Type) * count};
    _cbuff._begin   = new int(0);
    _cbuff._end     = new int(static_cast<int>(_max) - 1);
    _cbuff._left    = new uint8_t{0};

    _autodel = autoremove;
    _inited  = true;
    _cref    = new int{0};
    return NoError;
  }

  template<typename Type> base::Error init(Type* buffer, int &begin, int &end, std::size_t count, bool autoremove = true){
    _cbuff._buffer  = reinterpret_cast<uint8_t*>(buffer);
    _cbuff._size    = new int{_max = sizeof(Type) * count};
    _cbuff._begin   = &begin;
    _cbuff._end     = &end;
    _cbuff._left    = new uint8_t{begin < end? true: false};

    _autodel = autoremove;
    _inited  = false;
    _cref    = new int{0};
    return NoError;
  }

  template<typename Type> Type pull(){
    Type result;

    for (auto i = 0; i < cast_(i, sizeof(Type)); ++i)
      reinterpret_cast<uint8_t*>(&result)[i] = read();
    return result;
  }

  template<typename Type> base::Error push(Type value){
    auto error = NoError;

    for (auto i = 0; !error && i < cast_(i, sizeof(Type)); ++i)
      error = write(reinterpret_cast<uint8_t*>(&value)[i])? OutOfRange: error;
    return error;
  }

 public:
  Buffer& range(std::size_t max);

 public:
  bool full();
  bool empty();
  int  cref();

 public:
  bool write(uint8_t byte) override;
  bool revert(int count) override;
  byte read() override;
  void reset() override;

 public:
  std::size_t size() override;
  std::size_t max() override;
};

struct Rack: Cache{
 private:
  CRack  UNUSED(_cache);
  Buffer _buffer;
  FILE  *_fd;

 public:
  explicit Rack(std::string&& path, std::size_t bsize = 0);
  explicit Rack(std::string&& path, Buffer&& buffer);
  explicit Rack(std::string&& path, Buffer&  buffer);

  explicit Rack(std::string& path, std::size_t bsize = 0);
  explicit Rack(std::string& path, Buffer&& buffer);
  explicit Rack(std::string& path, Buffer&  buffer);

 public:
  FILE* fdesc();

 public:
  ~Rack() override;

 public:
  bool write(uint8_t byte) override;
  byte read() override;

 public:
  bool revert(int count) override;
  void reset() override;

 public:
  std::size_t size() override;
  std::size_t max() override;

 private:
  void init(std::string&& path);
  void init(std::string&  path);
};
} // namespace base

namespace base {
struct Stream : std::enable_shared_from_this<Stream> {
 public:
  using ComF = std::function<base::Error(uint8_t*, std::size_t&)>;

 public:
  Stream(ComF&& read, ComF&& write):
  _read{read}, _write{write}, _manual{0}{}

  virtual ~Stream(){}

 public:
  Stream() = delete;

 public:
  uint8_t  rByte(bool reverse = false);
  uint16_t rWord(bool reverse = false);
  uint32_t rDWord(bool reverse = false);
  uint64_t rLWord(bool reverse = false);

 public:
  base::Error rByte(uint8_t& byte, bool reverse = false);
  base::Error rWord(uint16_t& word, bool reverse = false);
  base::Error rDWord(uint32_t& dword, bool reverse = false);
  base::Error rLWord(uint64_t& lword, bool reverse = false);

 protected:
  uint8_t  rByte(base::Error& error, bool reverse = false);
  uint16_t rWord(base::Error& error, bool reverse = false);
  uint32_t rDWord(base::Error& error, bool reverse = false);
  uint64_t rLWord(base::Error& error, bool reverse = false);

 public:
 #if !__APPLE__ && !DISABLE_LIBGRAMMAR
  template<typename ByteT>
  std::shared_ptr<Stream> operator[](Lexical<ByteT>&&){
    return std::make_shared<Stream>(
      [&](uint8_t*, std::size_t) -> base::Error{
        return NoSupport;
      },
      [&](uint8_t*, std::size_t) -> base::Error{
        return NoSupport;
      });
  }
 #endif

  template<typename Type> std::size_t copyTo(Type* buffer, std::size_t size){
    auto raw   = reinterpret_cast<uint8_t*>(buffer);
    auto error = NoError;
    auto count = 0;

    for (; count < cast_(count, size * sizeof(Type)); ++count){
      raw[count] = rByte(error);

      if (error.code != base::error::EKeepContinue && error.code != base::error::EOutOfRange)
        throw error;
      else if (error.code == base::error::EKeepContinue && _input){
        auto size = _input->size();

        for (; count < cast_(count, size); ++count)
          raw[count] = rByte();
        _keep = true;
        break;
      } else return 0;
    }
    return count/sizeof(Type);
  }

  template<typename Type> std::size_t copyFrom(Type* buffer, std::size_t size){
    auto raw   = reinterpret_cast<uint8_t*>(buffer);
    auto error = NoError;
    auto count = size;

    _keep = false;
    if ((error = write(raw, count)))
      throw error;
    return count/sizeof(Type);
  }

  inline std::shared_ptr<Cache>& input(){ return _input; }

  inline std::shared_ptr<Cache>& output(){ return _output; }

 public:
  base::Error wByte (uint8_t  byte, bool reverse = false);
  base::Error wWord (uint16_t  word, bool reverse = false);
  base::Error wDWord(uint32_t dword, bool reverse = false);
  base::Error wLWord(uint64_t lword, bool reverse = false);

  inline base::Error revert(bool input, int count) {
    bool okey = false;

    if (input) {
      if (_input)
        okey = _input->revert(count);
      else
        return NoSupport;
    } else if (_output)
      okey = _output->revert(count);
    else
      return NoSupport;

    return okey ? NoError : OutOfRange;
  }

  inline base::Error flush() {
    auto error = NoError;

    if (_output) {
      auto tmp = (uint8_t*)malloc(_output->size());

      if (tmp) {
        auto size = _output->size();
        auto written = size;

        for (auto i = 0; i < cast_(i, size); ++i) tmp[i] = _output->read();
        if ((error = write(tmp, written))) _output->revert(size - written);
        free(tmp);
      }
    }

    return error;
  }

 public:
  bool keep(){ return _keep; }

 protected:
  explicit Stream(Stream& stream):
      std::enable_shared_from_this<base::Stream>{stream},
      _read{stream._read}, _write{stream._write}{
    _keep   = stream._keep;
    _manual = stream._manual;
  }

 protected:
#define STREAM_BUFFER_SCALE 1024
  inline base::Error read(uint8_t* data, std::size_t& size) {
    if (_input && _input->max() > 0 && (_manual & 1)) {
      uint8_t buffer[STREAM_BUFFER_SCALE];
      base::Error error{NoError};
      std::size_t saved{0}, total{0};

      for (auto p = 0; p < cast_(p, size) && !error;) {
        if (_input->size() < size) {
          auto needed = _input->max() - _input->size();

          while (needed > 0) {
            auto consume =
                needed < STREAM_BUFFER_SCALE ? needed : STREAM_BUFFER_SCALE;
            auto recved = consume;

            error = _read(buffer, recved);
            for (auto i = 0; i < cast_(i, recved); ++i)
              _input->write(buffer[i]);
            needed -= recved;

            if (error)
              return error;
            else if (recved < consume)
              break;
          }
        }

        saved = _input->size();
        for (auto i = 0; !error && p < cast_(p, size) && i < cast_(i, saved);
             ++i, ++p, ++total)
          data[p] = _input->read();
      }
      size = total;
      return NoError;
    } else
      return _read(data, size);
  }

  inline base::Error write(uint8_t* data, std::size_t& size) {
    if (_output && _output->max() > 0 && (_manual & 2)) {
      uint8_t buffer[STREAM_BUFFER_SCALE];
      base::Error error{NoError};

      for (auto p = 0; p < cast_(p, size);) {
        if (_output->size() == _output->max()) {
          while (_output->size() > 0) {
            auto needed = _output->size() < STREAM_BUFFER_SCALE
                              ? _output->size()
                              : STREAM_BUFFER_SCALE;
            auto consume = needed;

            for (auto i = 0; i < cast_(i, needed); ++i)
              buffer[i] = _output->read();

            if ((error = _write(buffer, consume))) {
              _output->revert(consume - needed);
              size = p + consume - needed;
              return error;
            }
          }

          p += _output->max();
        }
        for (; _output->size() < _output->max() && p < cast_(p, size); ++p)
          _output->write(data[p]);
      }
      size = 0;
      return NoError;
    } else
      return _write(data, size);
  }

  inline base::Error read(uint8_t* buffer, std::size_t&& size) {
    std::size_t recved = size;
    return read(buffer, recved);
  }

  inline base::Error write(uint8_t* buffer, std::size_t&& size) {
    std::size_t sent = size;
    return write(buffer, sent);
  }

 protected:
  std::shared_ptr<Cache> _input, _output;
  ComF _read, _write;
 
 protected:
  bool    _keep;
  uint8_t _manual;
};
} // namespace base

template<typename ByteT>
std::shared_ptr<base::Stream>& operator<<(std::shared_ptr<base::Stream>& dst, ByteT value){
  for (auto i = sizeof(ByteT); i > 0; i--){
    auto error = dst->wByte(reinterpret_cast<uint8_t*>(&value)[i - 1]);

    if (error) throw error;
  }
  return dst;
}

template<typename ByteT>
std::shared_ptr<base::Stream>& operator>>(std::shared_ptr<base::Stream>& src, ByteT& value){
  for (auto i = sizeof(ByteT); i > 0; i--){
    auto error = src->rByte(reinterpret_cast<uint8_t*>(&value)[i - 1]);

    if (error) throw error;
  }
  return src;
}

std::shared_ptr<base::Stream>& operator<<(std::shared_ptr<base::Stream>& dst, base::Buffer& buffer);
std::shared_ptr<base::Stream>& operator>>(std::shared_ptr<base::Stream>& src, base::Buffer& buffer);
#endif

#if !__cplusplus
typedef struct Query {
  void* buffer;
  int   n, nsize;
} Query;

CBuffer buffer(void* buf, int size, int* margin[2]);
Query  query(void* buf, int size, int* n);

int read4buff(CBuffer buffer, Query query);
int write2buff(Query query, CBuffer buffer);
int revert2buff(CBuffer buffer, int size);
#endif
#endif // LIBBASE_STREAM_HPP_
