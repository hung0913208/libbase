#if !defined(LIBBASE_PRINT_HPP_) && __cplusplus
#define LIBBASE_PRINT_HPP_

#include "logcat.hpp"

#include <cassert>
#include <functional>
#include <string>
#include <tuple>
#include <vector>

enum{ BOOLEAN = 0, NUMBER = 1, STRING = 2, PRINTER = 3 };

#define fwmap(type, key, value) \
  std::forward<std::tuple<std::string, type>>(std::make_tuple(key, value))
#define fwblock(type, value) std::forward<type>(value)

namespace base{
enum TypePrinter{ Json, Xml };

namespace print{
TypePrinter getTypePrinter(std::size_t type);

struct PrinterImpl{
 public:
  struct Object{
    explicit Object(std::size_t type);

    std::string value;
    std::size_t type;
  };

  using Element = std::tuple<std::string, Object>;

 public:
  std::function<bool(std::string&, Element&&)> _append_edict;
  std::function<bool(std::string&, Object&&)> _append_earray;
  std::function<void()> _release_printer;

 public:
  explicit PrinterImpl(TypePrinter&& type = Json);
  ~PrinterImpl();

 protected:
  TypePrinter _printer;
  int         _count;
};
}  // namespace print

struct Printer{
 private:
  print::PrinterImpl _printer;
  std::ostream* _ostream;
  std::string _result, _name;
  bool _status;

 public:
  Printer();
  ~Printer();

  explicit Printer(TypePrinter&& type);
  explicit Printer(std::ostream& output);
  explicit Printer(std::ostream& output, TypePrinter&& type);

 public:
  Printer& operator<<(std::tuple<std::string, int>&& element);
  Printer& operator<<(std::tuple<std::string, long>&& element);
  Printer& operator<<(std::tuple<std::string, float>&& element);
  Printer& operator<<(std::tuple<std::string, double>&& element);
  Printer& operator<<(std::tuple<std::string, bool>&& element);
  Printer& operator<<(std::tuple<std::string, std::string>&& element);
  Printer& operator<<(std::tuple<std::string, const char*>&& element);

  Printer& operator<<(int element);
  Printer& operator<<(long element);
  Printer& operator<<(float element);
  Printer& operator<<(double element);
  Printer& operator<<(bool element);
  Printer& operator<<(std::string&& element);
  Printer& operator<<(const char* element);

  Printer& operator<<(Printer&& printer);
  Printer& operator<<(std::tuple<std::string, Printer&&>&& element);

 public:
  std::string&& toString();
  void flush();
};
}  // namespace base
#endif  // LIBBASE_PRINT_HPP_
