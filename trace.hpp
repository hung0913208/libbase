#if !defined(LIBBASE_TRACE_HPP_) && __cplusplus
#define LIBBASE_TRACE_HPP_
/* @VERSION: trace.hpp verion 2: codename pascal */
/* @NOTE: redefine my trace.hpp again with new UI, it still use trace.hpp like
 * a unify core but the UI must change to improve performance and the way of
 * implementation */

#include <functional>
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <thread>
#include <chrono>
#include <ctime>
#include <mutex>

#include "./logcat.hpp"
#include "./utils.hpp"
#include "./config.hpp"

#define RUN_LABEL      "[ RUN      ] "
#define PASSED_LABEL   "[       OK ] "
#define FAILED_LABEL   "[   FAILED ] "
#define IGNORED_LABEL  "[  IGNORED ] "
#define CHECKED_LABEL  "[  CHECKED ] "
#define SNAPSHOT_LABEL "[ SNAPSHOT ] "

namespace std{ std::string to_string(std::string s); }
namespace base{
namespace trace{
struct Console{
 public:
   Console(char keyword='\0');

 public:
  Console& operator<<(Console& console);
  Console& operator<<(Color& color);
  Console& operator<<(Color&& color);
  Console& operator<<(std::string&& message);
  Console& operator<<(const std::string& message);
  Console& operator<<(int value);

 public:
  inline char keyword(){ return _keyword; }
  inline void lock(){ _lock = true; }
  inline void unlock(){ _lock = false; }

 private:
  inline void print(std::string&& message){
    if (message.size() == 1 && message[0] == '\n'){
      std::cout << std::endl << std::flush;
    } else {
      
      std::cout << message;
    }
  }

  inline void print(const std::string& message){
    if (message.size() == 1 && message[0] == '\n') {
      std::cout << std::endl << std::flush;
    } else {
      std::cout << message;
    }
  }

 private:
  char  _keyword;
  bool  _lock;
};

static std::mutex     glmutex{};
static trace::Console console{};
static trace::Console eol{'\n'};

void backtrace(std::size_t max);

struct Base: std::enable_shared_from_this<Base>{ 
 public:
  std::string key;

 public:
  inline void test(std::string line, std::function<void()>&& passing){
    using namespace std::chrono;

    auto now = system_clock::to_time_t(system_clock::now());

    std::stringstream id;
    id << std::this_thread::get_id();
   #if DEBUG
    trace::glmutex.lock();
    Watch{};
   #if SINGLE_COLOR
    trace::console << CHECKED_LABEL
   #if USE_MULTITHREAD
                   << "<" << id.str() << ">"
   #endif
                   << key << ": line " << line << ": ";
   #else
    trace::console << (Color{Color::Cyan} << CHECKED_LABEL) 
   #if USE_MULTITHREAD
                   << (Color{Color::Yellow} << "<" << id.str() << ">")
   #endif
                   << key << ": line " << line << " ";
   #endif
    trace::glmutex.unlock();
   #endif

    passing();

   #if DEBUG
    trace::glmutex.lock();
   #if SINGLE_COLOR
    trace::console << "   -->   passed" << "<" << id.str() << ">"
   #else
    trace::console << "   -->  " << (Color{Color::Green} << " passed ")
   #endif
                   << trace::eol;
    trace::glmutex.unlock();
   #endif
    wlog(now, rvalue(line), typeid(""), "passed");
  }

  inline int& backtrace(){ return _backtrace; }

 protected:
  virtual void wlog(std::time_t timer, std::string&& name,
                    const std::type_info& type, std::string&& value){
    Transparent(timer);
    Transparent(name);
    Transparent(type);
    Transparent(value);
  }

  virtual ~Base(){}

 private:
  int _backtrace = 0;
};
} // namespace trace

std::shared_ptr<trace::Base>& initialize(std::string&& key, 
    std::function<std::shared_ptr<trace::Base>()> generator, 
    bool force=false);

void release(std::shared_ptr<trace::Base>&& context);

struct Snapshot: public virtual trace::Base{
 public:
  explicit Snapshot(std::string&& file, std::string&& function);
  virtual ~Snapshot();

 public:
  template<typename Type> void snap(std::string&& name, Type variable){
    using namespace std::chrono;

    auto now = system_clock::to_time_t(system_clock::now());
    auto val = std::to_string(variable);

    std::stringstream id;
    id << std::this_thread::get_id();
    wlog(now, rvalue(name), typeid(Type), rvalue(val));

   #if DEBUG
    trace::glmutex.lock();
    Watch{};

   #if SINGLE_COLOR
    trace::console << SNAPSHOT_LABEL << name << " = " << val << " "
   #if USE_MULTITHREAD
                   << "<" << id.str() << ">"
   #endif
                   << trace::eol;
   #else
    trace::console << (Color{Color::Cyan} << SNAPSHOT_LABEL)
   #if USE_MULTITHREAD
                   << (Color{Color::Yellow}  << "<" << id.str() << ">")
   #endif
                   << (Color{Color::Magneto} << name) << " = " 
                   << (Color{Color::Yellow} << val) << " "
                   << trace::eol;
   #endif

    trace::glmutex.unlock();
   #endif
  }

  template<typename Type> void watch(std::string&& name,
                                     std::function<Type()>&& formular){
    using namespace std::chrono;

    auto now = system_clock::to_time_t(system_clock::now());
    auto val = std::to_string(formular());

    std::stringstream id;
    id << std::this_thread::get_id();
    wlog(now, rvalue(name), typeid(Type), rvalue(val));

   #if DEBUG
    trace::glmutex.lock();
    Watch{};

   #if SINGLE_COLOR
    trace::console << SNAPSHOT_LABEL << name << " = " << val    
   #if USE_MULTITHREAD
                   << "<" << id.str() << ">"
   #endif
                   << trace::eol;
   #else
    trace::console << (Color{Color::Cyan}    << SNAPSHOT_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                   << (Color{Color::Magneto} << name) << " = "
                   << (Color{Color::Yellow}  << val)  << " "
                   << trace::eol;
   #endif

    trace::glmutex.unlock();
   #endif
  }

 public:
  static std::shared_ptr<Snapshot> initialize(std::string file,
                                              std::string function){
    auto result = std::dynamic_pointer_cast<Snapshot>(
      base::initialize(
        rvalue(file + "::" + function), 
        [&]() -> std::shared_ptr<Base>{
          return std::make_shared<Snapshot>(rvalue(function), rvalue(file));
        }));

    return result;
  }

 protected:
  std::string _file, _function;
};
} // namespace base

namespace base{
template<typename Result>
struct LTrace: public Snapshot{
 private:
  using Target = std::function<Result&()>;

 public:
  explicit LTrace(std::string function, std::string file):
    Snapshot{rvalue(file), rvalue(function)}
  { key = file + "::" + function; }

  virtual ~LTrace(){ }

 public:
  Target& define(){ return _target; }

  Result  operator()(){
    using namespace std::chrono;

    std::stringstream id;
    clock_t start, end;

   #if DEBUG
    Debug debug{true};
   #endif

    id << std::this_thread::get_id();
    try{
     #if TRACE
      trace::glmutex.lock();
      trace::console << (Color{Color::Yellow} << RUN_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << " " 
                     << trace::eol;
      trace::glmutex.unlock();
     #endif
      start = clock();

      auto& result = _target();

      end = clock();
      trace::glmutex.lock();

     #if TRACE
      trace::console << (Color{Color::Green} << PASSED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us) " 
                     << trace::eol;
     #elif PROFILING
      trace::console << _function << "(" << (end - start) << " us)" 
                     << trace::eol;
     #endif

      if (backtrace() > 0) trace::backtrace(backtrace());
      trace::glmutex.unlock();

      return result;
    } catch (std::exception& except){
      end = system_clock::to_time_t(system_clock::now());
      trace::glmutex.lock();
      trace::console << (Color{Color::Red}  << FAILED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us): " 
                     << except.what() << trace::eol;

      if (backtrace() > 0) trace::backtrace(backtrace());
      trace::glmutex.unlock();
      throw WatchErrno.reason(except.what()); 
    } catch (base::Error& error){
      end = system_clock::to_time_t(system_clock::now());

      trace::glmutex.lock();
      trace::console << (Color{Color::Red} << FAILED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us)" 
     #if READABLE
                     << ": "<< error.message << " "
     #else
                     << " "
     #endif
                     << trace::eol;

      trace::glmutex.unlock();
      throw base::Error{error}; 
    }
  }

 public:
  static std::shared_ptr<LTrace<Result>> initialize(std::string&& function,
                                                    std::string&& file){
    auto result = std::dynamic_pointer_cast<LTrace<Result>>(
      base::initialize(
        rvalue(file + "::" + function), 
        [&]() -> std::shared_ptr<Base>{
          return std::make_shared<LTrace<Result>>(rvalue(function), rvalue(file));
        }
      ));
    return result;
  }

 private:
  Target _target;
};

template<typename Result>
struct RTrace: public Snapshot{
 private:
  using Target = std::function<Result()>;

 public:
  explicit RTrace(std::string function, std::string file):
    Snapshot{rvalue(file), rvalue(function)}
  { key = file + "::" + function; }

  virtual ~RTrace(){ }

 public:
  Target& define(){ return _target; }
  Result  operator()(){
    using namespace std::chrono;

   #if DEBUG
    Debug debug{true};
   #endif

    clock_t start, end;
    std::stringstream id;
    
    id << std::this_thread::get_id();
    try{
     #if TRACE
      trace::glmutex.lock();
      trace::console << (Color{Color::Yellow} << RUN_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << " <" << id.str() << ">")
     #endif
                     << _function << " " 
                     << trace::eol;
      trace::glmutex.unlock();
     #endif

      start = clock();
      auto result = _target();
      end = clock();

      trace::glmutex.lock();
     #if TRACE
      trace::console << (Color{Color::Green} << PASSED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << " <" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us) " 
                     << trace::eol;
     #elif PROFILING
      trace::console << _function << "(" << (end - start) << " us)"
                     << trace::eol;
     #endif

      if (backtrace() > 0) trace::backtrace(backtrace());
      trace::glmutex.unlock();

      return result;
    } catch (std::exception& except){
      end = system_clock::to_time_t(system_clock::now());
      trace::glmutex.lock();

      trace::console << (Color{Color::Red}  << FAILED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us): " 
                     << except.what() << trace::eol;

      if (backtrace() > 0) trace::backtrace(backtrace());
      trace::glmutex.unlock();
      throw WatchErrno.reason(except.what()); 
    } catch (base::Error& error){
      end = system_clock::to_time_t(system_clock::now());
      trace::glmutex.lock();

      trace::console << (Color{Color::Red} << FAILED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us)" 
     #if READABLE
                     << ": "<< error.message << " "
     #else
                     << " "
     #endif
                     << trace::eol;

      trace::glmutex.unlock();
      throw base::Error{error}; 
    }
  }

 public:
  static std::shared_ptr<RTrace<Result>> initialize(std::string&& function,
                                                    std::string&& file){
    auto result = std::dynamic_pointer_cast<RTrace<Result>>(
      base::initialize(
        rvalue(file + "::" + function), 
        [&]() -> std::shared_ptr<Base>{
          return std::make_shared<RTrace<Result>>(rvalue(function), rvalue(file));
        }
      ));
    return result;
  }

 private:
  Target _target;
};

template<> struct RTrace<void>: public Snapshot{
 private:
  using Target = std::function<void()>;

 public:
  explicit RTrace(std::string function, std::string file):
    Snapshot{rvalue(file), rvalue(function)}
  { key = file + "::" + function; }

  virtual ~RTrace(){ }

 public:
  Target& define(){ return _target; }

  void operator()(){
    using namespace std::chrono;

   #if DEBUG
    Debug debug{true};
   #endif

    clock_t start, end;
    std::stringstream id;
    
    id << std::this_thread::get_id();
    try{
     #if TRACE
      trace::glmutex.lock();
      trace::console << (Color{Color::Yellow} << "[ RUN      ] ") 
                     << _function << trace::eol;

      trace::glmutex.unlock();
     #endif

      start = clock();
      _target();
      end = clock();

      trace::glmutex.lock();
     #if TRACE
      trace::console << (Color{Color::Green} <<  "[       OK ] ")
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us)"
                     << trace::eol;

     #elif PROFILING
      trace::console << _function << "(" << (end - start) << " us)"
                     << trace::eol;
     #endif

      if (backtrace() > 0) trace::backtrace(backtrace());
      trace::glmutex.unlock();
    } catch (std::exception& except){
      end = system_clock::to_time_t(system_clock::now());
      trace::glmutex.lock();

      trace::console << (Color{Color::Red}  << FAILED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us): " 
                     << except.what() << trace::eol;

      if (backtrace() > 0) trace::backtrace(backtrace());
      trace::glmutex.unlock();
      throw WatchErrno.reason(except.what()); 
    } catch (base::Error& error){
      end = system_clock::to_time_t(system_clock::now());
      trace::glmutex.lock();

      trace::console << (Color{Color::Red} << FAILED_LABEL)
     #if USE_MULTITHREAD
                     << (Color{Color::Yellow} << "<" << id.str() << ">")
     #endif
                     << _function << "(" << (end - start) << " us)" 
     #if READABLE
                     << ": "<< error.message << " "
     #else
                     << " "
     #endif
                     << trace::eol;
      trace::glmutex.unlock();
      throw base::Error{error}; 
    }
  }

 public:
  static std::shared_ptr<RTrace<void>> initialize(std::string&& function,
                                                  std::string&& file){
    auto result = std::dynamic_pointer_cast<RTrace<void>>(
      base::initialize(
        rvalue(file + "::" + function), 
        [&]() -> std::shared_ptr<Base>{
          return std::make_shared<RTrace<void>>(rvalue(function), rvalue(file));
        }
      ));
    return result;
  }

 private:
  Target _target;
};
} // namespace base

/* @NOTE: trace only support when macro TRACE is on, because of the affect of 
 * removing trace.hpp now, i think the best way is keeping trace.hpp and define
 * a upper level of this library. Everything still same at the core but the UI
 * must change to the better application */

/* @NOTE: define a 'tracking' function must be something like this
 + void someFunction(Arg0 arg0, Arg1 arg1, ...){
 +   BEGIN_PTRACKING()
 +
 +   END_PTRACKING;
 + }

 + Type& someFunction(Arg0 arg0, Arg1 arg1, ...){
 +   BEGIN_LTRACKING(void)
 +
 +   END_FTRACKING;
 + }

 + Type someFunction(Arg0 arg0, Arg1 arg1, ...){
 +   BEGIN_RTRACKING(void)
 +
 +   END_FTRACKING;
 + }

 *
 * this design would be easier to use and more effective for tracing code from
 * afar, beside of that, this would require more.
 */
#if TRACE == 2
#ifndef BEGIN_PTRACKING
#define BEGIN_PTRACKING()                                                      \
  using namespace base;                                                        \
  auto __tracking__ = RTrace<void>::initialize(__PRETTY_FUNCTION__, __FILE__); \
  __tracking__->define() = std::function<void()>([&](){
#endif

#ifndef BEGIN_LTRACKING
#define BEGIN_LTRACKING(Ret)                                                   \
  using namespace base;                                                        \
  auto __tracking__ = LTrace<Ret>::initialize(__PRETTY_FUNCTION__, __FILE__);  \
  __tracking__->define() = std::function<Ret&()>([&]() -> Ret&{
#endif

#ifndef BEGIN_RTRACKING
#define BEGIN_RTRACKING(Ret)                                                   \
  using namespace base;                                                        \
  auto __tracking__ = RTrace<Ret>::initialize(__PRETTY_FUNCTION__, __FILE__);  \
  __tracking__->define() = std::function<Ret()>([&]() -> Ret{
#endif

#ifndef END_FTRACKING
#define END_FTRACKING                                                          \
  });                                                                          \
  return (*__tracking__)()
#endif

#ifndef END_PTRACKING
#define END_PTRACKING                                                          \
  });                                                                          \
  (*__tracking__)()
#endif
/* @NOTE: UI-toolbox of trace
 * - SNAPSHOT(Variable): capture variable at the time and print it on screen 
 * - SUMMARY(Block): catch a summary of many information when codeflow jump into
 * this line
 * - PASS(Message): print message when it pass out this line
 * - PRESENT(Name, Struct): present a block of value on screen
 */
#ifndef SNAPSHOT
#define SNAPSHOT(Variable) __tracking__->snap(#Variable, Variable)
#endif

#ifndef SNAPSHOT_WITH
#define SNAPSHOT_WITH(Convert, Variable)                                       \
  __tracking__->watch(#Variable, std::function<std::string()>{                 \
    [&]() -> std::string{ return Convert(Variable); }                          \
  });
#endif

#ifndef SUMMARY
#define SUMMARY(Block) __tracking__->test(std::to_string(__LINE__), [&]()##Block)
#endif

#ifndef PASS
#define PASS(Message)                                                          \
  __tracking__->test(std::to_string(__LINE__), [&](){                         \
    base::trace::console << Message;                                           \
  })
#endif

#ifndef CHECK
#define CHECK(Line)                                                            \
  __tracking__->test(std::to_string(__LINE__), [&](){                         \
    Line; base::trace::console << #Line;                                       \
  })

#endif
// #define PRESENT(Name, Struct) __tracking__
#elif TRACE == 1
/* @NOTE: backport to version 'leriril', as I said above, I still using the old
 * UI but change the core, branch leriril is just used to save my previous 
 * working */

namespace base{
template<typename Type>
struct TraceWarp{
  std::shared_ptr<LTrace<Type>> ltrace;
  std::shared_ptr<RTrace<Type>> rtrace;

  TraceWarp(){}

 public:
  Type  rresult(){ return (*rtrace)(); }

  Type& lresult(){ return (*ltrace)(); }

  void test(std::size_t line, std::function<void()> callback){
    if (rtrace != nullptr)
      rtrace->test(std::to_string(line), rvalue(callback));
    else if (ltrace != nullptr)
      ltrace->test(std::to_string(line), rvalue(callback));
  }

  template<typename Input> void snap(std::string&& name, Input variable){
    if (rtrace != nullptr)
      rtrace->snap(rvalue(name), variable);
    else if (ltrace != nullptr)
      ltrace->snap(rvalue(name), variable);
  }

  template<typename Input> void watch(std::string&& name,
                                     std::function<Input()>&& formular){
    if (rtrace != nullptr)
      rtrace->watch(rvalue(name), rvalue(formular));
    else if (ltrace != nullptr)
      ltrace->watch(rvalue(name), rvalue(formular));
  }
};

template<>
struct TraceWarp<void>{
  std::shared_ptr<RTrace<void>> trace;

  TraceWarp(){}
 public:
  void operator()(){ (*trace)(); }

 public:
  void test(std::size_t line, std::function<void()> callback){
    trace->test(std::to_string(line), rvalue(callback));
  }

  template<typename Type> void snap(std::string&& name, Type variable){
    trace->snap(rvalue(name), variable);
  }

  template<typename Type> void watch(std::string&& name,
                                     std::function<Type()>&& formular){
    trace->watch(rvalue(name), rvalue(formular));
  }
};
} // namespace base

#ifndef RETURNING_BUFFER
#define RETURNING_BUFFER __result__
#endif

#ifndef INIT_TRACKING
#define INIT_TRACKING(Type)                                                    \
  base::TraceWarp<Type> __tracking__{};
#endif

#ifndef REGISTER_RETURNING_BUFFER
#define REGISTER_RETURNING_BUFFER(TResult)                                     \
  INIT_TRACKING(TResult); TResult __result__{}
#endif

#ifndef REGISTER_REFF_RETURNING_BUFFER
#define REGISTER_REFF_RETURNING_BUFFER(TResult, value)                         \
  INIT_TRACKING(TResult); TResult& __result__ = value
#endif

#ifndef REGISTER_RETURNING_BUFFER_WITH_INIT
#define REGISTER_RETURNING_BUFFER_WITH_INIT(TResult)                           \
  INIT_TRACKING(TResult); TResult __result__
#endif

#ifndef BUILD_PTRACKING
#define BUILD_PTRACKING(perform)                                               \
  __tracking__.trace = base::RTrace<void>::initialize(                         \
    __PRETTY_FUNCTION__, __FILE__);                                            \
  __tracking__.trace->define() = perform;                                      \
  __tracking__()
#endif

#ifndef BUILD_FTRACKING
#define BUILD_FTRACKING(perform)                                               \
  __tracking__.rtrace = base::RTrace<decltype(__result__)>::initialize(        \
      __PRETTY_FUNCTION__, __FILE__);                                          \
  __tracking__.rtrace->define() = perform
#endif

#ifndef BUILD_REFF_TRACKING
#define BUILD_REFF_TRACKING(perform)                                           \
  __tracking__.ltrace = base::LTrace<decltype(__result__)>::initialize(        \
      __PRETTY_FUNCTION__, __FILE__);                                          \
  __tracking__.ltrace->define() = perform
#endif

#ifndef PRINT_PASSING_LINE
#define PRINT_PASSING_LINE(message)                                            \
  __tracking__.test(                                                          \
      __LINE__, []() { base::trace::console << message << base::trace::eol; })
#endif

#ifndef CHECK_PASSING_LINE
#define CHECK_PASSING_LINE(line)                                               \
  __tracking__.test(__LINE__, std::function<void()>{[](){ line }})
#endif

#ifndef CHECK_PASSING_BLOCK
#define CHECK_PASSING_BLOCK(block)                                             \
  __tracking__.test(__LINE__, std::function<void()>(block))
#endif

#ifndef TAKE_VSNAPSHOT
#define TAKE_VSNAPSHOT(name, value) __tracking__.snap(name, value)
#endif

#ifndef TAKE_FSNAPSHOT
#define TAKE_FSNAPSHOT(name, value) __tracking__.watch(name, value)
#endif

#ifndef RETURN_FROM_TRACKING
#define RETURN_FROM_TRACKING (*__tracking__.rtrace)()
#endif

#ifndef RETURN_REFF_FROM_TRACKING
#define RETURN_REFF_FROM_TRACKING (*__tracking__.ltrace)();
#endif

#ifndef BEGIN_PTRACKING
#define BEGIN_PTRACKING() auto __result__ = [&](){
#endif

#ifndef BEGIN_LTRACKING
#define BEGIN_LTRACKING(Result) auto __result__ = [&]() -> Result&{
#endif

#ifndef BEGIN_RTRACKING
#define BEGIN_RTRACKING(Result) auto __result__ = [&]() -> Result{
#endif

#ifndef END_FTRACKING
#define END_FTRACKING                                                          \
  };                                                                           \
  return __result__()
#endif

#ifndef END_PTRACKING
#define END_PTRACKING                                                          \
  };                                                                           \
  __result__()
#endif

#ifndef SNAPSHOT
#define SNAPSHOT(Variable) (0)
#endif

#ifndef SNAPSHOT_WITH
#define SNAPSHOT_WITH(Variable, Convert) (0)
#endif

#ifndef SUMMARY
#define SUMMARY(Block) (0)
#endif

#ifndef PASS
#define PASS(Message) (0)
#endif

#ifndef CHECK
#define CHECK(Line) (0)
#endif
#else
#ifndef BEGIN_PTRACKING
#define BEGIN_PTRACKING() auto __result__ = [&](){
#endif

#ifndef BEGIN_LTRACKING
#define BEGIN_LTRACKING(Result) auto __result__ = [&]() -> Result&{
#endif

#ifndef BEGIN_RTRACKING
#define BEGIN_RTRACKING(Result) auto __result__ = [&]() -> Result{
#endif

#ifndef END_FTRACKING
#define END_FTRACKING                                                          \
  };                                                                           \
  return __result__()
#endif

#ifndef END_PTRACKING
#define END_PTRACKING                                                          \
  };                                                                           \
  __result__()
#endif

#ifndef SNAPSHOT
#define SNAPSHOT(Variable) (0)
#endif

#ifndef SNAPSHOT_WITH
#define SNAPSHOT_WITH(Variable, Convert) (0)
#endif

#ifndef SUMMARY
#define SUMMARY(Block) (0)
#endif

#ifndef PASS
#define PASS(Message) (0)
#endif

#ifndef CHECK
#define CHECK(Line) (0)
#endif

/* @NOTE: backport to version 'leriril', as I said above, I still using the old
 * UI but change the core, branch leriril is just used to save my previous 
 * working */
#ifndef RETURNING_BUFFER
#define RETURNING_BUFFER __result__
#endif

#ifndef INIT_TRACKING
#define INIT_TRACKING(type) (void)(0);
#endif

#ifndef REGISTER_RETURNING_BUFFER
#define REGISTER_RETURNING_BUFFER(TResult) TResult __result__
#endif

#ifndef REGISTER_REFF_RETURNING_BUFFER
#define REGISTER_REFF_RETURNING_BUFFER(TResult, value)                         \
  TResult __result__ = value
#endif

#ifndef REGISTER_RETURNING_BUFFER_WITH_INIT
#define REGISTER_RETURNING_BUFFER_WITH_INIT(TResult) TResult __result__
#endif

#ifndef BUILD_FTRACKING
#define BUILD_FTRACKING(perform) return perform()
#endif

#ifndef BUILD_PTRACKING
#define BUILD_PTRACKING(perform) perform()
#endif

#ifndef BUILD_REFF_TRACKING
#define BUILD_REFF_TRACKING(perform) return perform()
#endif

#ifndef PRINT_PASSING_LINE
#define PRINT_PASSING_LINE(message) (void)(0)
#endif

#ifndef CHECK_PASSING_LINE
#define CHECK_PASSING_LINE(line) line
#endif

#ifndef CHECK_PASSING_BLOCK
#define CHECK_PASSING_BLOCK(block) block
#endif

#ifndef TAKE_VSNAPSHOT
#define TAKE_VSNAPSHOT(name, value) (void)(0)
#endif

#ifndef TAKE_FSNAPSHOT
#define TAKE_FSNAPSHOT(name, value) (void)(0)
#endif

#ifndef RETURN_FROM_TRACKING
#define RETURN_FROM_TRACKING RETURNING_BUFFER
#endif

#ifndef RETURN_REFF_FROM_TRACKING
#define RETURN_REFF_FROM_TRACKING RETURNING_BUFFER
#endif
#endif
#endif // LIBBASE_TRACE_HPP_

/* @TODO: 
 * - Bug duoc bao tai file libbase/grammar/lexica.hpp, viec khoi tao bat thanh, std::map<> khong the load duoc
 * session Trace vao bang anh xa(co the la loi cua std::map)
 */