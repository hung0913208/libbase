#include "print.hpp"

#include <string>
#include <tuple>
#include <utility>
#include <vector>

using Impl = base::print::PrinterImpl;

namespace base{
namespace print{
TypePrinter getTypePrinter(std::size_t type){
  return std::forward<TypePrinter>((type == 0)? Json: Xml);
}

PrinterImpl::Object::Object(std::size_t type) : type{type} {}

PrinterImpl::PrinterImpl(TypePrinter&& type) : _printer{type}, _count{0} {
  switch (type){
    case Json:
      _append_edict = [](std::string& buffer, Element&& elm) -> bool{
        if (buffer.size() == 0) buffer.append("{");
        else
          buffer.back() = ',';

        buffer.append("\"").append(std::get<0>(elm)).append("\":");

        if (std::get<1>(elm).type == STRING)
          buffer.append("\"").append(std::get<1>(elm).value).append("\"");
        else
          buffer.append(std::get<1>(elm).value);

        buffer.append("}");
        return false;
      };

      _append_earray = [](std::string& buffer, PrinterImpl::Object&& elm) -> bool{
        if (buffer.size() == 0) buffer.append("[");
        else
          buffer.back() = ',';

        if (elm.type == STRING)
          buffer.append("\"").append(elm.value).append("\"");
        else
          buffer.append(elm.value);

        buffer.append("]");
        return true;
      };
      break;

    case Xml:
      _append_edict = [&](std::string& buffer, Element&& elm) -> bool{
        buffer.append("<");

        if (std::get<1>(elm).type != PRINTER)
          buffer.append("tag key=\"").append(std::get<0>(elm)).append("\"")
                .append(" value=\"").append(std::get<1>(elm).value)
                .append("\"/>");
        else
          buffer.append(std::get<0>(elm)).append(">")
                .append(std::get<1>(elm).value)
                .append("</").append(std::get<0>(elm)).append(">");

        _count = 0;
        return false;
      };

      _append_earray = [&](std::string& buffer, PrinterImpl::Object&& elm) -> bool{
        buffer.append("<item index=\"").append(std::to_string(_count))
              .append("\"").append("\"")
              .append(elm.value).append("\"");

        switch(elm.type){
        case PRINTER:
          break;

        case STRING:
          buffer.append(" type=\"string\"");
          break;

        case NUMBER:
          buffer.append(" type=\"number\"");
          break;

        case BOOLEAN:
          buffer.append(" type=\"boolean\"");
          break;

        default:
          throw NoSupport;
        }

        buffer.append(">").append(elm.value).append("</item>");
        _count += 1;
        return true;
      };
      break;

    default:
      break;
  }
}

PrinterImpl::~PrinterImpl(){
  if (_release_printer) _release_printer();
}
}  // namespace print

Printer::Printer(std::ostream& output, TypePrinter&& type)
    : _printer{std::forward<TypePrinter>(type)},
      _ostream{&output},
      _status{false} {}

Printer::Printer(TypePrinter&& type)
    : _printer{std::forward<TypePrinter>(type)},
      _ostream{nullptr},
      _status{false} {}

Printer::Printer(std::ostream& output)
    : _printer{Json}, _ostream{&output}, _status{false} {}

Printer::Printer() : _printer{Json}, _ostream{nullptr}, _status{false} {}

Printer::~Printer(){}

Printer& Printer::operator<<(std::tuple<std::string, int>&& element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(std::get<1>(element)));
    if (!_printer._append_edict) throw NoSupport;
    if (!_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::tuple<std::string, long>&& element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(std::get<1>(element)));
    if (!_printer._append_edict) throw NoSupport;
    if (!_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::tuple<std::string, float>&& element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(std::get<1>(element)));
    if (!_printer._append_edict) throw NoSupport;
    if (!_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::tuple<std::string, double>&& element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(std::get<1>(element)));
    if (!_printer._append_edict) throw NoSupport;
    if (!_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::tuple<std::string, std::string>&& element){
  try{
    Impl::Object object{STRING};

    object.value.assign(std::get<1>(element));
    if (!_printer._append_edict) throw NoSupport;
    if (!_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::tuple<std::string, const char*>&& element){
  try{
    Impl::Object object{STRING};

    object.value.assign(std::string{std::get<1>(element)});

    if (!_printer._append_edict) throw NoSupport;
    if (!_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::tuple<std::string, bool>&& element){
  try{
    Impl::Object object{STRING};

    object.value.assign(std::get<1>(element) ? "True" : "False");

    if (!_printer._append_edict) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(int element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(element));

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(long element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(element));

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(float element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(element));

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(double element){
  try{
    Impl::Object object{NUMBER};

    object.value.assign(std::to_string(element));

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::string&& element){
  try{
    Impl::Object object{STRING};

    object.value.assign(element);

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(const char* element){
  try{
    Impl::Object object{STRING};

    object.value.assign(element);

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(bool element){
  try{
    Impl::Object object{BOOLEAN};

    object.value.assign(element ? "True" : "False");

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

std::string&& Printer::toString(){
  return std::forward<std::string>(_result);
}

Printer& Printer::operator<<(Printer&& printer){
  try{
    Impl::Object object{PRINTER};

    object.value.assign(printer.toString());

    if (!_printer._append_earray) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_earray(_result, std::forward<Impl::Object>(object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}

Printer& Printer::operator<<(std::tuple<std::string, Printer&&>&& element){
  try{
    Impl::Object object{PRINTER};

    object.value.assign(std::get<1>(element).toString());

    if (!_printer._append_edict) throw NoSupport;
    if (_status){
      if (_ostream != nullptr && _ostream->good())
        _ostream->write(_result.c_str(), _result.size());
      _status = !_status;
      _result.erase();
    }
    _printer._append_edict(_result, std::make_tuple(std::get<0>(element), object));
    return *this;
  } catch (base::Error& error){
    throw Error{error};
  }
}


void Printer::flush(){
  if (_ostream){
    _ostream->write(_result.c_str(), _result.size());
    _result.clear();
  }
}
}  // namespace base
