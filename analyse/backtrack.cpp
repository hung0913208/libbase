#include "../trace.hpp"
#include "../utils.hpp"
#include "../macros.hpp"

#if LINUX || APPLE
#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <vector>
#include <string>

namespace base{
namespace internal{
#define ADDR2LINE "eu-addr2line '%p' --pid=%d > /dev/stderr\n"

std::pair<char**, int> symbols(void** array, int max){
  auto size    = backtrace (array, max);
  auto symbols = backtrace_symbols (array, size);

  return std::make_pair(symbols, size);
}
std::string fixname(std::string&& symbol){ return symbol; }

std::string addpos(std::string&& symbol){
  char syscom[1024];

  snprintf(syscom, 1024, ADDR2LINE, symbol.c_str(), getpid());
  if (system(syscom) != 0)
    return "";
  return "";
}
} // namespace internal

namespace trace{
void backtrace(std::size_t max){
  void* buffer[max];

  auto symbols = internal::symbols(buffer, max);
  for (auto i = 1; i < std::get<1>(symbols); ++i)
    console << internal::fixname(std::get<0>(symbols)[i]) << eol;

  free (std::get<0>(symbols));
}
} // namespace trace
} // namespace base

#else
namespace base{
namespace trace{
void backtrace(std::size_t max){
  warn(DoNothing.reason("backtrace() don't support you OS"));
}
} // namespace trace
} // namespace base
#endif