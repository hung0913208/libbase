#include "trace.hpp"
#include "logcat.hpp"
#include "thread.hpp"

#include <vector>

namespace std { std::string to_string(std::string s){ return s; } }

using Entry     = std::shared_ptr<base::trace::Base>;
using Channel   = std::map<std::string, std::vector<Entry>>;
using Generator = std::function<Entry()>;

namespace base {
namespace internal{
std::map<std::thread::id, Channel> stacktraces{};
std::map<std::thread::id, int> threadmanager{};

base::Error startdlock(char* argv[], int argc){
  return NoError;
}

base::Error cleardlock(){
  return NoError;
}
} // namespace internal

Entry& initialize(std::string&& key, Generator generator, bool force){
  using namespace internal;

  base::Boundary bound{[](){ trace::glmutex.lock(); },
                       [](){ trace::glmutex.unlock(); }};
  auto thread_id = std::this_thread::get_id();

  /* @RESOLVE:
   * - co ve nhu std::map co gioi han ve do dai key 
   * - khong, no dang co gang truy cap vao mot vung nho nullptr
   * - co ve khong sua duoc, bug nam ngoai code, tat ca deu nham 
   * vao cung 1 ham bi loi. 
   */
  if (stacktraces.find(thread_id) == stacktraces.end())
    stacktraces[thread_id] = Channel{};

  if (stacktraces[thread_id].find(key) == stacktraces[thread_id].end())
    stacktraces[thread_id][key] = std::vector<std::shared_ptr<base::trace::Base>>{};

  if (force || stacktraces[thread_id][key].size() == 0)
    stacktraces[thread_id][key].push_back(generator());
  return stacktraces[thread_id][key].back();
}

void release(std::shared_ptr<trace::Base>&& context){ 
  using namespace internal;

  base::Boundary bound{[](){ trace::glmutex.lock(); },
                       [](){ trace::glmutex.unlock(); }};
  auto thread_id = std::this_thread::get_id();

  if (stacktraces[thread_id].find(context->key) == stacktraces[thread_id].end())
    throw NotFound;
  else if (stacktraces[thread_id][context->key].size() == 1)
    stacktraces[thread_id].erase(context->key);
  else stacktraces[thread_id][context->key].pop_back();
}

Snapshot::Snapshot(std::string&& file, std::string&& function):
  _file {file}, _function {function} { }

Snapshot::~Snapshot(){}
} // namespace base
