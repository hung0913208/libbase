#include "hash.hpp"

uint32_t hashArray32b(const char* s, size_t len){
}

uint64_t hashArray64b(const char* s, size_t len){
}

uint128_t hashArray128b(const char* s, size_t len){
}

uint32_t hashArray32bWithSeed(const char* s, size_t len, size_t seed){
}

uint64_t hashArray64bWithSeed(const char* s, size_t len, size_t seed){
}

uint128_t hashArray128bWithSeed(const char* s, size_t len, size_t seed){
}

uint32_t hashArray32WithSeed(const char* s, size_t len, size_t seed1, size_t seed2){
}

uint64_t hashArray64bWithSeed(const char* s, size_t len, size_t seed1, size_t seed2){
}

uint128_t hashArray128bWithSeed(const char* s, size_t len, size_t seed1, size_t seed2){
}