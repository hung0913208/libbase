#if !defined(LIBBASE_AUTO_HPP_)
#define LIBBASE_AUTO_HPP_

#if __cplusplus
#include <functional>
#include <cxxabi.h>
#include <typeinfo>

#include "logcat.hpp"

namespace base{
struct Auto{
 private:
  using Type = std::reference_wrapper<const std::type_info>;
  using Ref = int*;
  using Any = void*;

  typedef void (*Del)(Any);
  typedef Any (*Copy)(Any);

 public:
   Auto(): _context{nullptr}, _clone{nullptr}, _del{nullptr},
           _ref{nullptr}, _type{typeid(nullptr)}{ }
  ~Auto(){
    if (!_ref) return;
    else if (*_ref > 0) (*_ref)--;
    else{
      free(_ref);
      if (_del) _del(_context);
      else
        free(_context);
    }
  }

  explicit Auto(std::nullptr_t):
      _context{nullptr},
      _clone{nullptr}, 
      _del{nullptr},
      _ref{nullptr},
      _type{typeid(nullptr)}{ }

  Auto(const Auto& src): 
      _context{src._context},
      _clone{src._clone}, 
      _del{src._del},
      _ref{src._ref},
      _type{src._type}{ 
    if (_ref) (*_ref)++;
  }

  Auto(Auto&& src):
      _context{src._context},
      _clone{src._clone},
      _del{src._del},
      _ref{src._ref},
      _type{src._type}{
    if (_ref) (*_ref)++;
  }

 public:
  inline Auto& operator=(std::nullptr_t){
    if (_ref){
      if (*_ref > 0) (*_ref)--;
      else{
        free(_ref);

        if (_del) _del(_context);
        else
          free(_context);
      }
    }

    _type    = typeid(std::nullptr_t);
    _clone   = nullptr;
    _del     = nullptr;
    _ref     = nullptr;
    _context = nullptr;
    return *this;
  }

  inline Auto& operator=(const Auto& src){
    if (&src == this) return *this;
    if (_ref){
      if (*_ref > 0) (*_ref)--;
      else{
        free(_ref);
        if (_del) _del(_context);
        else
          free(_context);
      }
    }

    _del     = src._del;
    _clone   = src._clone;
    _ref     = src._ref;
    _context = src._context;
    _type    = src._type.get();

    if (_ref) (*_ref)++;
    return *this;
  }

  inline bool operator!=(std::nullptr_t){
    return _context != nullptr;
  }

  inline bool operator<(const Auto& src) const{ return _context < src._context; }

  inline operator bool(){ return _context != nullptr; }

 public:
  template<typename ValueT> Auto& operator=(ValueT value){
    if (_type.get() == typeid(ValueT)){
      *(reinterpret_cast<ValueT*>(_context)) = value;
      return *this;
    } else if (_ref){
      if (*_ref > 0) (*_ref)--;
      else{
        free(_ref);

        if (_del) _del(_context);
        else
          free(_context);
      }
    }

    _del     = [](Any context){ delete reinterpret_cast<ValueT*>(context); };
    _clone   = [](Any context) -> Any {
      auto value = reinterpret_cast<ValueT*>(context);
      return reinterpret_cast<Any>(new ValueT{*value});
    };
    _ref     = reinterpret_cast<int*>(calloc(sizeof(int), 1));
    _context = reinterpret_cast<Any>(new ValueT{value});
    _type    = typeid(ValueT);

    return *this;
  }

  template<typename ValueT> bool operator==(const ValueT& value) const{
    if (_type.get() != typeid(ValueT))
      return false;
    if (_context)
      return !memcmp(&value, _context, sizeof(ValueT));
    return false;
  }

  template<typename ValueT> bool operator !=(const ValueT& value) const{
    if (_type.get() != typeid(ValueT))
      return false;
    if (_context)
      return memcmp(&value, _context, sizeof(ValueT));
    else  return false;
  }

  template<typename ValueT> bool operator==(const ValueT&& value) const{
    if (_type.get() != typeid(ValueT))
      return false;
    if (_context)
      return !memcmp(&value, _context, sizeof(ValueT));
    else return false;
  }

  template<typename ValueT> bool operator!=(const ValueT&& value) const{ 
    if (_type.get() != typeid(ValueT))
      return false;
    if (_context)
      return !memcmp(&value, _context, sizeof(ValueT));
    else return false;
  }

  bool operator==(std::nullptr_t&& UNUSED(value)) const{
    return _context == nullptr;
  }

  bool operator!=(std::nullptr_t&& UNUSED(value)) const{ 
    return _context != nullptr;
  }

  bool operator==(base::Auto& UNUSED(value)) const{
    throw BadLogic.reason("can't compare auto with auto");
  }

  bool operator!=(base::Auto& UNUSED(value)) const{ 
    throw BadLogic.reason("can't compare auto with auto");
  }

  bool operator==(base::Auto&& UNUSED(value)) const{
    throw BadLogic.reason("can't compare auto with auto");
  }

  bool operator!=(base::Auto&& UNUSED(value)) const{ 
    throw BadLogic.reason("can't compare auto with auto");
  }

 public:
  template<typename ValueT> base::Error move(const ValueT& value){
    if (!_context) return BadAccess.reason("can\'t move value to nulltype");
    else if (_type.get() != typeid(ValueT))
      return BadLogic.reason("\'ValueT\' must be " + nametype());

    *(reinterpret_cast<ValueT*>(_context)) = value;
    return NoError;
  }

  template<typename ValueT> base::Error move(ValueT&& value){
    if (!_context) return BadAccess.reason("can\'t move value to nulltype");
    else if (_type.get() != typeid(ValueT))
      return BadLogic.reason("\'ValueT\' must be " + nametype());

    *(reinterpret_cast<ValueT*>(_context)) = value;
    return NoError;
  }

  template<typename ValueT> base::Error pull(const ValueT& value){
    if (_ref){
      if (*_ref > 0) (*_ref)--;
      else{
        free(_ref);
        if (_del) _del(_context);
        else
          free(_context);
      }
    }

    _type    = typeid(ValueT);
    _clone   = nullptr;
    _del     = nullptr;
    _ref     = nullptr;
    _context = reinterpret_cast<Any>(const_cast<ValueT*>(&value));
    return NoError;
  }

  template<typename ValueT> ValueT get(){
    if (typeid(ValueT) != _type.get())
      throw BadAccess.reason("ValueT must be " + nametype());

    return *((ValueT*)_context);
  }

  template<typename ValueT> Auto& set(const ValueT& value){
    base::Error error{};

    if ((error = pull(value)))
      throw error;
    return *this;
  }

  template<typename ValueT> Auto& set(ValueT&& value){
    return ((*this) = value);
  }

  template<typename ValueT> base::Error push(ValueT& value){
    if (typeid(ValueT) != _type.get())
      return BadAccess.reason("ValueT must be " + nametype());
    else
      value = *(reinterpret_cast<ValueT*>(_context));
    if (_ref){
      if (*_ref > 0) (*_ref)--;
      else{
        free(_ref);
        if (_del) _del(_context);
        else
          free(_context);
      }
    }
    _type    = typeid(std::nullptr_t);
    _context = nullptr;
    _ref     = nullptr;
    _del     = nullptr;
    _clone   = nullptr;
    return NoError;
  }

  inline std::pair<Any, Del> strip(){
    auto result = std::make_pair(_context, _del);

    if (_ref){
      if (_del){
        std::get<0>(result) = _clone(_context);

        if (*_ref > 0)
          (*_ref)--;
        else {
          free(_ref);

          if (_del) _del(_context);
          else
            free(_context);
        }
      } else {
        if (*_ref > 0)
          (*_ref)--;
        else {
          free(_ref);
        }
      }
    }

    _type    = typeid(std::nullptr_t);
    _context = nullptr;
    _ref     = nullptr;
    _del     = nullptr;
    _clone   = nullptr;
    return result;
  }
 public:
  inline Type type(){ return _type; }
  inline Del& delete_(){ return _del; }

  inline std::string nametype(){
    int status;
    std::unique_ptr<char[], void (*)(void*)> result(
      abi::__cxa_demangle(_type.get().name(), 0, 0, &status), std::free);

    return result.get() ? std::string(result.get()) : "error occurred";
  }

  inline bool is(const std::type_info& type){
    return this->type().get() == type; 
  }

 private:
  Any  _context;
  Copy _clone;
  Del  _del;
  Ref  _ref;
  Type _type;
};
} // namespace base
#endif

#if __cplusplus
extern "C"{
#endif

#if __cplusplus
}
#endif
#endif // LIBBASE_AUTO_HPP_